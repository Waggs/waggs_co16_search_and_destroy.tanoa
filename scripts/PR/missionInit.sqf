
waitUntil { (postInitComplete) };
if !(pr_deBug == 0) then { diag_log format ["*PR* missionInit start"]; };

if (hasInterface) then { waitUntil { local player; player == player }; };  //--- Check to wait until player is ready

enableSaving [false, false];

//--- Briefing and trigger setup *** check if need to move to fn_postInit.sqf
if (useBriefing) then { execVM "scripts\PR\scripts\init\briefingInit.sqf"; };  //--- Sets mission breifing
execVM "scripts\PR\scripts\init\triggerInit.sqf";
if (useTasks) then { call compile preprocessFileLineNumbers "special\tasks.sqf"; };  //--- Sets mission tasks

[] execVM "scripts\PR\scripts\init\NVG.sqf";  //--- PR Night Vision Goggles
[] execVM "scripts\PR\scripts\init\fn_optics.sqf";  //--- PR Optics
[] execVM "scripts\PR\scripts\init\3rdView.sqf";  //--- PR 3rd Person View
[] execVM "scripts\PR\scripts\init\pilotCheck.sqf";  //--- PR Pilot Check

if !(aceOn) then { call pr_fnc_diaryHud; };

{ _x setVariable ["BIS_noCoreConversations", true] } forEach allUnits; enableSentences false;  //--- Silences AI from speaking and on screen chats 

//if (["IntroMovie", 0] call BIS_fnc_getParamValue == 1) then { if (!isNil {player}) then { execVM "special\titles.sqf"; }; };  //---Intro Movie
if (["IntroMovie", 0] call BIS_fnc_getParamValue == 1) then { if (!isNil {player}) then { _nil = [] execVM "special\intro.sqf"; }; };  //---Intro Movie

#include "scripts\init\taskForceRadio_Init.sqf"  //--- TaskForceRadio - "scripts\PR\functions\fn_taskForceRadio.sqf"
#include "scripts\init\skull_init.sqf"  //--- Skulls custom scripts

[] execVM "scripts\PR\weather\weatherACE.sqf";  //--- PR Custom weather, world dependant
[] execVM "scripts\PR\weather\stormyWeather.sqf";  //--- Adds foggy breath, wind sounds and snow effects

[] execVM "scripts\PR\scripts\misc\globalChatTools.sqf";  //--- Sets sideChat globally
[] execVM "scripts\PR\scripts\misc\drag_push\pr_push.sqf"; //--- sets objects pushable, with fatigue

[] execVM "scripts\PR\scripts\tools\module_cleanup\init.sqf";  //--- Module Performance Clean-up script

execVM "scripts\IgiLoad\IgiLoadInit.sqf";  //--- IgiLoad script - logistical support

if (isNil "playCustomMusic") then { if (["IntroMusic",0] call BIS_fnc_getParamValue == 1) then { null = [["special\music\music.sqf"],"BIS_fnc_execVM",nil,true] call BIS_fnc_MP; }; };  //--- Adds music at mission start

if (useBriefing) then { execVM "special\briefing.sqf"; };  //--- Sets mission breifing
if (useTasks) then { execVM "scripts\PR\scripts\tasks\taskInit.sqf"; };  //--- Sets mission tasks

SLP_init = [] execVM "scripts\SLP\SLP_init.sqf";  //--- SLP Spawning Script

call compile preprocessFileLineNumbers "scripts\UPSMON\Init_UPSMON.sqf";  //--- Init UPSMON script (must be run on all clients)

if (aceOn) then { [player] spawn pr_fnc_medHolder; };

if (useClient) then {
    [] call compile preprocessFileLineNumbers "scripts\PR\loadouts\fn_loadoutInit.sqf"; //--- Initilize Custom Loadout scripts
    [] execVM "special\fn_Client.sqf";
};

if (useDrag_koInit) then { [] execVM "special\drag_koInit.sqf"; }; //--- Allows drag and knock out on units
if (useMoveInVehicleTimer) then { [] call compile preprocessFileLineNumbers "special\fn_moveInVehTimer.sqf"; }; //--- Moves players into vehicle at mission start, set on a timer
if (useMissionTimer) then { [] call compile preprocessFileLineNumbers "special\fn_missionTimer.sqf"; }; //--- Allows putting an ending on mission by timer
if (useHideMapObjects) then { [] call compile preprocessFileLineNumbers "special\hideMapObjects.sqf"; }; //--- Function to hide map objects at mission start
if (useRandomMove) then { [] call compile preprocessFileLineNumbers "special\randomMoveInit.sqf"; }; //--- Random object start positions
if (useIntel_Destruction) then { execVM "special\intel_destructionInit.sqf"; }; //--- Data Down/Up load, Bomb defuse, Nuke & Fire scripts
if (useIgiPreLoad) then { execVM "special\IgiPreLoad.sqf"; };  //--- IgiLoad Pre-Load items in vehicles before mission start

if (usePR_Init) then {  //--- Reaps custom init for all things not init.sqf
    #include "..\..\special\PR_Init.sqf"
};

if !(pr_deBug == 0) then { diag_log format ["*PR* missionInit complete"]; };
