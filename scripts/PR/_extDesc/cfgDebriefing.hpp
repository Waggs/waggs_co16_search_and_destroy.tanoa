
//--- End Mission #1
class End1 {
    title = "Mission Completed";
    subtitle = "Well done men";
    description = "You have successfully completed your mission, you have served your country well.";
    pictureBackground = "scripts\CUSTOM\pictures\end3.jpg";
    picture = "b_inf";
    pictureColor[] = {0.0,0.3,0.6,1};
};

//--- End Mission #2
class End2 {
    title = "Mission Ended";
    subtitle = "Complete failure men";
    description = "You failed to complete your mission, work on your teamwork before you attempt this again.";
    pictureBackground = "scripts\CUSTOM\pictures\end2.jpg";
    picture = "b_inf";
    pictureColor[] = {0.0,0.3,0.6,1};
};
