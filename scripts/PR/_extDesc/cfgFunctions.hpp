
//--- Pre init scritps
#include "..\config.hpp"

//--- AIS Revive
#include "..\..\ais_injury\cfgFunctionsAIS.hpp"

//--- Adds Blue Hud features
#include "..\scripts\hud_tags\BlueHud\functions\cfgFunctions.hpp"

//--- TRT_AASS Artillery Computer script
#include "..\commander\artillery\functions\cfgFunctions.hpp"

//--- FHQ Tasktracker
#include "..\scripts\tasks\functions\fhq_tasktracker.hpp"