/*
med_drop.sqf
ver 1.0 - 2015-04-17 complete ACE medical system rework in progress

"medHeliSpawn" marker required in mission for spawn location // side  1 = Blufor, 2 = Opfor, 3 = Indedpendent // gunner crew  0 = off, 1 = ON
["spawn marker", "drop marker", side, gunner crew, "type of crate", custom crate, grid location of caller]

["medHeliSpawn", "medDrop", 1, 1, Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR\HealthCare\misc\AiLift\med_drop.sqf";

From a trigger use:
if !(heliDrop_med) then {["medHeliSpawn", "medDrop", 1, 1, "AGM_Box_Medical", 0, 0] execVM "scripts\PR\HealthCare\misc\AiLift\med_drop.sqf"} else {hint format["Only run one instance of AiLift at a time"];};

if (true) then {["medHeliSpawn", "medDrop", 1, 1, "Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR\HealthCare\misc\AiLift\med_drop.sqf"};
*/ //diag_log format ["***test***"];
private ["_spawn","_drop","_side","_crew","_crate","_custom","_gridPos"]; 

if (isNil "heliDrop_med") then {heliDrop_med = false}; 
heliDrop_med = true; publicVariable "heliDrop_med"; 

_spawn      = _this select 0; 
_drop       = _this select 1; 
_side       = _this select 2; 
_crew       = _this select 3; 
_crate      = _this select 4; 
_custom     = _this select 5; 
_gridPos    = _this select 6; 

_dropbox    = _crate createVehicle (getMarkerPos _spawn); 
_rtb        = "Land_HelipadEmpty_F" createVehicle (getMarkerPos _spawn); 
_dropland1  = "Land_HelipadEmpty_F" createVehicle (getMarkerPos _drop); 
gridPos     = _gridPos; publicVariable "gridPos"; 
dropbox     = _dropbox; publicVariable "dropbox"; 
Heli_med    = objNull; 
sleep 1; 

if (_custom == 1) then { 
    //[{[dropbox] call PR_fnc_customBox;},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
    //[{[dropbox] call fnc_aceMedBox;},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
    [[dropbox], "fnc_aceMedBox"] call BIS_fnc_MP; 
}; 

sleep 10; 

Heli_med = "b_heli_Transport_01_camo_F" createVehicle (getMarkerPos _spawn); 
Heli_med allowDamage false; 
if (true) then {Heli_med animateDoor ['door_R', 1]; Heli_med animateDoor ['door_L', 1];}; 
_grouppilots = createGroup West; 
"b_helipilot_F" createUnit [getMarkerPos _spawn, _grouppilots, "this moveInDriver Heli_med", 0.6, "corporal"]; 
"b_helipilot_F" createUnit [getMarkerPos _spawn, _grouppilots, "this moveInTurret [Heli_med, [0]]", 0.6, "corporal"]; 
_grouppilots allowFleeing 0; 
{_x allowDamage false} forEach units _grouppilots; 

if (_crew == 1) then { 
    grpMedCrew = createGroup West; 
    "b_helicrew_F" createUnit [getMarkerPos _spawn, grpMedCrew, "this moveInTurret [Heli_med, [1]]", 0.6, "corporal"]; 
    "b_helicrew_F" createUnit [getMarkerPos _spawn, grpMedCrew, "this moveInTurret [Heli_med, [2]]", 0.6, "corporal"]; 
    grpMedCrew allowFleeing 0; 
    grpMedCrew setBehaviour "COMBAT"; 
}; 

Heli_med setGroupID ["Carrier Pigeon"]; 
publicVariable "Heli_med"; 
showChat True; 

[{FIREFLY = [West,"airbase"];  FIREFLY sideChat format ["Carrier Pigeon, Supply Drop Requested at Grid %1",gridPos];},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
_dropbox allowDamage false; 
_dropbox attachTo [Heli_med,[0,2,-5]]; 
sleep 1; 
_dropbox allowDamage true; 
Heli_med flyInHeight 330; 
_waypoint1 = _grouppilots addWaypoint [getMarkerPos _Drop, 5]; 
_waypoint1 setWayPointBehaviour "CARELESS"; 
_waypoint1 setWayPointSpeed "NORMAL"; 
_waypoint1 setWayPointType "MOVE"; 
_waypoint1 setWayPointCombatMode "WHITE"; 
sleep 5; 

[{Heli_med setGroupID ["Carrier Pigeon"]; Heli_med sideChat "Copy Base Firefly, Supply Drop in Route";},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
_waypoint2 = _grouppilots addWaypoint [getMarkerPos _Drop, 2]; 
Heli_med flyInHeight 250; 
_dropbox allowDamage false; 

waitUntil {Heli_med distance _dropland1 < 400};
Heli_med allowDamage true; 
{_x allowDamage true} forEach units _grouppilots; 

waitUntil {Heli_med distance _dropland1 < 300}; 
detach _dropbox; 
sleep 4.5; 
_pilot = _dropbox; 
_chute = createVehicle ["B_Parachute_02_F", [100, 100, 200], [], 0, 'FLY']; 
_chute setPos [position _pilot select 0, position _pilot select 1, (position _pilot select 2) - 4]; 
_dropbox attachTo [_chute, [0, 0, -0.6]]; 

[{Heli_med setGroupID ["Carrier Pigeon"]; Heli_med sideChat format ["Base Firefly, Package Dropped at Grid %1",gridPos];},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
sleep 5; 
[{FIREFLY = [West,"airbase"]; FIREFLY sideChat "Copy Carrier Pigeon, RTB";},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
_waypoint3 = _grouppilots addWaypoint [getPos _rtb, 5]; 
_waypoint3 setWayPointBehaviour "CARELESS"; 
_waypoint3 setWayPointSpeed "FULL"; 
_waypoint3 setWayPointType "MOVE"; 

_waypoint3 setWayPointCombatMode "WHITE"; 

if (((getPos _dropbox) select 2) > 3) then { 0 = [_dropbox] execVM "scripts\PR\HealthCare\misc\AiLift\find_medBox_air.sqf";}; 

waitUntil {position _dropbox select 2 < 0.5 || isNull _chute;}; 
detach _dropbox; 
_dropbox setPos [position _dropbox select 0, position _dropbox select 1, 0]; 
_dropbox allowDamage true; 

if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 0 = [_dropbox] execVM "scripts\PR\HealthCare\misc\AiLift\find_medBox_land.sqf";}; 

waitUntil {Heli_med distance _rtb < 400;}; 
Heli_med land "Land"; 
waitUntil {((getPos Heli_med) select 2) < 0.5;}; 

sleep 120; 
[{ Heli_med setGroupID ["Carrier Pigeon"]; Heli_med sideChat "Base Firefly, Carrier Pigeon has Returned to Base";},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
sleep 5; 
[{ FIREFLY = [West,"airbase"]; FIREFLY sideChat "Copy Carrier Pigeon, Standby";},"BIS_fnc_spawn",true,false] call BIS_fnc_MP; 
deleteVehicle _dropland1; 
deleteVehicle _rtb; 
deleteVehicle Heli_med; 
{deleteVehicle _x} forEach units _grouppilots; 
if !(isNil "grpMedCrew") then { 
    {deleteVehicle _x} forEach units grpMedCrew; 
}; 
	
if (true) exitWith { heliDrop_med = false; publicVariable "heliDrop_med";}; 
