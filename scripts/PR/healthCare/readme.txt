/* PR HealthCare System 
*  Author: PapaReap 
*  ver 1.1 - 2016-01-03 major script system rework in progress 
*  ver 1.0 - 2014-12-26 complete medical system rework in progress
* 
*  todo - fix multible cargo mash drops actions, sort action name 
*         add to these instructions the parms needed 
*         add med box to cargo of field mash 
*         make med box mash specific 
*         possible long range radio 
* 
*  Place "healthCare" folder in your "scripts\PR" directory, adjust paths as necessary 
*  Usage: Add to your "init.sqf" lines below, remove "*". If using TaskForce Radio, best to place after it. 
*  
*  fnc_local_healthCare = compile preprocessFileLineNumbers "scripts\PR\healthCare\medFunctions\fn_local_healthCare.sqf";          //--- Local healthcare before server healthcare 
*  [server] call compile preprocessFileLineNumbers "scripts\PR\healthCare\medFunctions\fn_healthCare_init.sqf";                    //--- Server  healthcare 
*  [player] call compile preprocessFileLineNumbers "scripts\PR\healthCare\medFunctions\fn_playerState.sqf";                        //--- Player health & crawling 
*  [player] call compile preprocessFileLineNumbers "scripts\PR\healthCare\medFunctions\fn_MediCare.sqf";                           //--- Medic duties, must name player "LeadMedic" or "p1" 
*  [] call compile preprocessFileLineNumbers "scripts\PR\healthCare\mash\mashCommon\fn_mashTreatment.sqf";                         //--- Allows receiving blood by mobile mash, if allowed 
*  fnc_mashTreatmentClient = compile preprocessFileLineNumbers "scripts\PR\healthCare\mash\mashCommon\fn_mashTreatmentClient.sqf"; //--- Monitors blood received by mobile mash, if allowed 
*  fnc_randomBodyDamage = compile preprocessFileLineNumbers "scripts\PR\healthCare\misc\fn_randomBodyDamage.sqf";                  //--- Script to give random body damage for a specific period 
*/ 