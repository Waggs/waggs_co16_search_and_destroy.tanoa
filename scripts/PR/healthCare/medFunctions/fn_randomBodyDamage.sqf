/*

man setHitPointDamage [HitRightLeg, .8]; 
 * Arguments:
 * 0: The Unit <OBJECT>
 * 1: Damage to Add <NUMBER>
 * 2: Selection ("head", "body", "hand_l", "hand_r", "leg_l", "leg_r") <STRING>
 * 3: Projectile Type <STRING>
 
 * Example:
 * [player, 0.8, "leg_r", "bullet"] call ace_medical_fnc_addDamageToUnit
 * [cursorTarget, 1, "body", "stab"] call ace_medical_fnc_addDamageToUnit
 
 * Author: Glowbal
 * Sets a unit in the unconscious state.
 *
 * Arguments:
 * 0: The unit that will be put in an unconscious state <OBJECT>
 * 1: Set unconsciouns <BOOL> (default: true)
 * 2: Minimum unconscious time <NUMBER> (default: (round(random(10)+5)))
 * 3: Force AI Unconscious (skip random death chance) <BOOL> (default: false)
 *
 * ReturnValue:
 * nil
 *
 * Example:
 * [bob, true] call ace_medical_fnc_setUnconscious;  // if we have unconsciousness for AI disabled, we will kill the unit instead
 *
 * Public: yes

*/

//_nul = [pilot] execVM "scripts\PR\healthCare\misc\randomBodyDamage.sqf";


/*
_unit getVariable ["ace_medical_bloodVolume", 30, true];

    _unit = _this select 0;
	_bodyPart = _this select 1;
	_bodyParts = _this select 2;
	_damage = _this select 3;
	_pain = _this select 4;

    _bodyPart = ["HitRightLeg", "HitLeftLeg", "HitRightArm", "HitLeftArm", "HitBody", "HitHead"] call BIS_fnc_selectRandom;
    _bodyPart2Chance = 1;
    _bodyPart2Chance = [1, 2] call BIS_fnc_selectRandom;

    _damage = ((0.2 + random 0.8) min 0.8);
    _pain = ((0.2 + random 0.4) min 0.6);
    _unit allowDamage true;
    _unit setHitPointDamage [_bodyPart, _damage];

    if (_bodyPart2Chance == 2) then {
        _bodyPart2 = ["HitRightLeg", "HitLeftLeg", "HitRightArm", "HitLeftArm", "HitBody", "HitHead"] call BIS_fnc_selectRandom;
        _unit setHitPointDamage [_bodyPart2, _damage];
    }; 
    _unit setVariable ["ace_medical_pain", _pain, true];
    _unit allowDamage false;
*/

// _nul = [pilot1, [true, 30, true], [false, "body", "bullet", 0.5]] execVM "scripts\PR\healthCare\misc\fn_randomBodyDamage.sqf";
// [pilot1, [true, 30, true], [false, "body", "bullet", 0.5],["DNR", false]] spawn pr_fnc_randomBodyDamage;
// _nul = [pilot1, [true, 300, true], [false, "body", "bullet", 0.5]] spawn pr_fnc_randomBodyDamage;
// _nul = [pilot1, [true, 30, true], [true]] execVM "scripts\PR\healthCare\misc\randomBodyDamage.sqf";
// _nul = [pilot1] execVM "scripts\PR\healthCare\misc\randomBodyDamage.sqf";
// 0 = [pilot1, [true, 60, true], [false, "body", "bullet", 0.5],[true, ""]] spawn pr_fnc_randomBodyDamage;
// 0 = [pilot1, [true, 60, true], [true],[true]] spawn pr_fnc_randomBodyDamage;
// 0 = [pilot1, [true, 60, true], [true], [true]] spawn pr_fnc_randomBodyDamage;

//usage; // 0 = [pilot1, [true, 120, true], [true], [true]] spawn pr_fnc_randomBodyDamage;
//--- _nil = [this] execVM "scripts\PR\scripts\misc\fn_randomBodyDamage.sqf";

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_randomBodyDamage start"]; };

pr_fnc_blur = { //[_unit] call pr_fnc_blur;
    _unit = _this select 0;
    _blur = ppEffectCreate ["DynamicBlur", 472];
    _blur ppEffectEnable true;
    _blur ppEffectAdjust [(0.5 + random 1.5)]; //1.21
    _blur ppEffectCommit 2;
    _unit setVariable ["prBlur", _blur];
};

pr_fnc_blurRemove = {
    _unit = _this select 0;
    _blur = _unit getVariable "prBlur";
    ppeffectdestroy _blur;
};

if !(aceOn) then {
    //[pilot2, [true, 5000, true], [true], [true]] spawn pr_fnc_randomBodyDamage;
    _unit = _this select 0;
    _getUnconscious = false;
    _noRevive = false;

    if (isNil { _unit getVariable ["PR_randomUnconcious", Nil] }) then {
        _unit setVariable ["PR_randomUnconcious", false, false];
        _getUnconscious = _unit getVariable "PR_randomUnconcious";
    } else {
        _getUnconscious = _unit getVariable "PR_randomUnconcious";
    };

    _unconciousArray = [_this, 1, [], [[]]] call BIS_fnc_param; // _this select 1
    _unconcious = [_unconciousArray, 0, true] call BIS_fnc_paramIn; // Set unconsciouns - <BOOL>
    _time = [_unconciousArray, 1, 30] call BIS_fnc_paramIn; // Minimum unconscious time - <NUMBER>

    _damageArray = [_this, 2, [], [[]]] call BIS_fnc_param;
    _doDamage = _damageArray select 0;

    _initArray = [_this, 3, [], [[]]] call BIS_fnc_param; // _this select 3
    _noRevive = [_initArray, 0, false] call BIS_fnc_paramIn; // Unit will stay unconscious until unconcious time runs out

    if (_unconcious) then {
        _unit playActionNow "agonyStart";
        if (_unit == player) then {
            [_unit] call pr_fnc_blur;
        };
        //[_unit, "PRONE_INJURED_U2"] call BIS_fnc_ambientAnim;
        _unit setVariable ["PR_randomUnconcious", true, false];
        _getUnconscious = _unit getVariable "PR_randomUnconcious";
        _unit setUnconscious true;
        _unit setCaptive true;
    };

    if (_doDamage) then {
        _unit setHitPointDamage ["HitHead", (0.25 + (random 0.74) min 0.99)]; // 1 dead
        _unit setHitPointDamage ["HitChest", (0.25 + (random 0.75))];
        _unit setHitPointDamage ["HitBody", (0.25 + (random 0.74) min 0.99)]; // 1 dead
        _unit setHitPointDamage ["HitHands", (0.25 + (random 0.75))];
        _unit setHitPointDamage ["HitLegs", (0.25 + (random 0.75))];
    };

    _placeTime = floor (serverTime);
    _timeToComplete = _placeTime + _time;

    while { ((alive _unit) && (_getUnconscious) && (floor (serverTime) < _timeToComplete)) } do {
        if (_noRevive) then {
            if !(_unit getVariable "PR_randomUnconcious") then {
                _getUnconscious = _unit getVariable "PR_randomUnconcious";
            };
        } else {
            if (_unit getHitPointDamage "HitHead" < 0.1) then {
                _unit setVariable ["PR_randomUnconcious", false, false];
                _getUnconscious = _unit getVariable "PR_randomUnconcious";
            };
        };
        uisleep 2;
    };

    if ((alive _unit) && (!(_getUnconscious) || (_unit getHitPointDamage "HitHead" < 0.1))) then {
        _unit playActionNow "agonyStop";
        _unit setCaptive true;
        if (_unit == player) then {
            [_unit] call pr_fnc_blurRemove;
        };
    };
};

if !(aceOn) exitWith {};

_unit = _this select 0;
[_unit, 0.01, "leg_r", "bullet"] call ace_medical_fnc_addDamageToUnit;

_unconciousArray = [_this, 1, [], [[]]] call BIS_fnc_param;  // _this select 1
_unconcious = [_unconciousArray, 0, true] call BIS_fnc_paramIn; // Set unconsciouns - <BOOL>
_time = [_unconciousArray, 1, 30] call BIS_fnc_paramIn; // Minimum unconscious time - <NUMBER>
_force = [_unconciousArray, 2, true] call BIS_fnc_paramIn; // Force AI Unconscious (skip random death chance) - <BOOL>

_noRevive = false;
_isUnconscious = false;
_randomCount = 0;

[_unit, _unit] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;
//_blood = _unit getVariable "ace_medical_bloodVolume";
_isRandom = false;
_damageArray = [_this, 2, [], [[]]] call BIS_fnc_param;  // _this select 2
_random = [_damageArray, 0, true] call BIS_fnc_paramIn; // Random body part - <BOOL>
if (typeName _random == "ARRAY") then {
    _randomArray = _random;
    _isRandom = [_randomArray, 0, true] call BIS_fnc_paramIn;
    if (count _randomArray > 1) then {
        _randomCount = [_randomArray, 1, 0] call BIS_fnc_paramIn;
        _randomCount = (_randomCount) min 6;
    };
} else { _isRandom = _random };


pr_random = _random; pr_randomCount = _randomCount; pr_isRandom = _isRandom; // delete after testing 

_unit allowDamage false;
_damage = 0;
_rPain = 0; // randomPain
_bP = "";  // Body Part
_bPNs = [0, 1, 2, 3, 4, 5]; // _bodyPartNumbers ["head", "body", "hand_l", "hand_r", "leg_l", "leg_r"];
_rPN = []; // randomPartNumber
_PNA = [0, 0, 0, 0, 0, 0]; pr_newPartNumber = _PNA; // Part Number Array
_bloodLoss = 0;

if ((_isRandom) && (_randomCount > 0)) then { // 0 = [pilot1, [true, 60, true], [[true, 2]], [true]] spawn pr_fnc_randomBodyDamage;
    for [{ _loop = 0 }, { _loop < _randomCount }, { _loop = _loop + 1 }] do {
        _bPN = _bPNs call BIS_fnc_selectRandom; pr_bodyPartNumber = _bPN; // _bPN = bodyPartNumber
        _bPNs = _bPNs - [_bPN]; pr_bodyPartNumbers = _bPNs;
        _rPN = _rPN + [_bPN]; pr_randomBodyPartNumbers = _rPN;
    };
    //_PNA = [0, 0, 0, 0, 0, 0]; pr_newPartNumber = _PNA; // Part Number Array
    for [{ _loop = 0 }, { _loop < (count _rPN) }, { _loop = _loop + 1 }] do {
        if (_rPN select _loop == 0) then {
            _damage = ((random 1) Max 0.2) Min 1; _PNA set [0, _damage];
        } else {
            if (_rPN select _loop == 1) then {
                _damage = ((random 1) Max 0.2) Min 1; _PNA set [1, _damage];
            } else {
                if (_rPN select _loop == 2) then {
                    _damage = ((random 1) Max 0.2) Min 1; _PNA set [2, _damage];
                } else {
                    if (_rPN select _loop == 3) then {
                        _damage = ((random 1) Max 0.2) Min 1; _PNA set [3, _damage];
                    } else {
                        if (_rPN select _loop == 4) then {
                            _damage = ((random 1) Max 0.2) Min 1; _PNA set [4, _damage];
                        } else {
                            if (_rPN select _loop == 5) then {
                                _damage = ((random 1) Max 0.2) Min 1; _PNA set [5, _damage];
                            };
                        };
                    };
                };
            };
        };
    };
    _unit setVariable ["ace_medical_bodyPartStatus", _PNA, true];
    _bloodLoss = (100 / ((_PNA select 0) + (_PNA select 1) + (_PNA select 2) + (_PNA select 3) + (_PNA select 4) + (_PNA select 5))) min 100; pr_bloodLoss = _bloodLoss; // 100 / 5.4 = 18.52
    _unit setVariable ["ace_medical_bloodVolume", _bloodLoss, true];
    _rPain = ((random 0.9) Max 0.2) Min 0.9;
    _unit setVariable ["ace_medical_pain", _rPain, true];
} else {
    if (_isRandom) then {  // 0 = [pilot1, [true, 60, true], true, [true]] spawn pr_fnc_randomBodyDamage;
        _bPN = _bPNs call BIS_fnc_selectRandom; pr_bodyPartNumber = _bPN; // _bPN = bodyPartNumber
        _bPNs = _bPNs - [_bPN]; pr_bodyPartNumbers = _bPNs;
        _rPN = _rPN + [_bPN]; pr_randomBodyPartNumbers = _rPN;
        _damage = ((random 1) Max 0.2) Min 1;
        _PNA set [_bPN, _damage];
        _unit setVariable ["ace_medical_bodyPartStatus", _PNA, true];
        _bloodLoss = (75 / ((_PNA select 0) + (_PNA select 1) + (_PNA select 2) + (_PNA select 3) + (_PNA select 4) + (_PNA select 5))) min 100; pr_bloodLoss = _bloodLoss; // 100 / 5.4 = 18.52
        _unit setVariable ["ace_medical_bloodVolume", _bloodLoss, true];
        _rPain = ((random 0.9) Max 0.2) Min 0.9;
        _unit setVariable ["ace_medical_pain", _rPain, true]; // 0.9 * 100
    } else {  // 0 = [pilot1, [true, 60, true], [false, "body", 0.5], [true, ""]] spawn pr_fnc_randomBodyDamage;
        if (count _damageArray > 1) then { _bP = [_damageArray, 1, "body"] call BIS_fnc_paramIn; };  // "head", "body", "hand_l", "hand_r", "leg_l", "leg_r"
        if (count _damageArray > 2) then { _damage = [_damageArray, 2, 0.5] call BIS_fnc_paramIn; };  // Damage 0-1
        if (_bP == "head") then {
            _PNA set [0, _damage];
        } else {
            if (_bP == "body") then {
                _PNA set [1, _damage];
            } else {
                if (_bP == "hand_l") then {
                    _PNA set [2, _damage];
                } else {
                    if (_bP == "hand_r") then {
                        _PNA set [3, _damage];
                    } else {
                        if (_bP == "leg_l") then {
                            _PNA set [4, _damage];
                        } else {
                            if (_bP == "leg_r") then {
                                _PNA set [5, _damage];
                            };
                        };
                    };
                };
            };
        };
        _unit setVariable ["ace_medical_bodyPartStatus", _PNA, true];
        _bloodLoss = (75 / ((_PNA select 0) + (_PNA select 1) + (_PNA select 2) + (_PNA select 3) + (_PNA select 4) + (_PNA select 5))) min 100; pr_bloodLoss = _bloodLoss; // 100 / 5.4 = 18.52
        _unit setVariable ["ace_medical_bloodVolume", _bloodLoss, true];
        _rPain = ((random 0.9) Max 0.2) Min 0.9;
        _unit setVariable ["ace_medical_pain", _rPain, true]; // 0.9 * 100
    };
}; //_bodyPart = ["HitRightLeg", "HitLeftLeg", "HitRightArm", "HitLeftArm", "HitBody", "HitHead"] call BIS_fnc_selectRandom;

[_unit, _unconcious, _time, _force] call ace_medical_fnc_setUnconscious;

_placeTime = floor (serverTime);
_timeToComplete = _placeTime + _time;

_unit setVariable ["PR_randomUnconcious", true, false];
_getUnconscious = _unit getVariable "PR_randomUnconcious";

//special init
_initArray = [_this, 3, [], [[]]] call BIS_fnc_param;  // _this select 3
_noRevive = [_initArray, 0, false] call BIS_fnc_paramIn; // Unit will stay unconscious until unconcious time runs out
if (count _initArray > 1) then { _init = [_initArray, 1, ""] call BIS_fnc_paramIn; }; // Unit Init - <STRING>

while { ((alive _unit) && (_getUnconscious) && (floor (serverTime) < _timeToComplete)) } do {
    _getUnconscious = _unit getVariable "PR_randomUnconcious";
    _isUnconscious = _unit getVariable "ACE_isUnconscious";
    _blood = _unit getVariable "ace_medical_bloodVolume";
    if !(_isUnconscious) then {
        if (_noRevive) then {
            //get existing damage, pain blood, etc...
            _damageBodyParts = _unit getVariable "ace_medical_bodyPartStatus";
            [_unit, _unit] call ace_medical_fnc_treatmentAdvanced_fullHealLocal; //[CALLER, TARGET] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;
            [_unit, _unconcious, _time, _force] call ace_medical_fnc_setUnconscious; //reforce unconciousness
            _unit setVariable ["ace_medical_bodyPartStatus", _damageBodyParts, true];
        } else {
            _unit setVariable ["PR_randomUnconcious", false, false]; // to allow waking set: pilot1 setVariable ["PR_randomUnconcious", false, false];
            _getUnconscious = _unit getVariable "PR_randomUnconcious";
        };
    };
    if (_blood < 15) then { _unit setVariable ["ace_medical_bloodVolume", 15, true]; };
    if !(alive _unit) then { [_unit, _unit] call ace_medical_fnc_copyDeadBody; };
    uisleep 2;
};

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_randomBodyDamage complete"]; };
