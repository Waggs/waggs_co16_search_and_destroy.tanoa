/* 
*  Author: PapaReap 
*  Function names: pr_fnc_healthCareInit 
*  PR Healthcare init ran on server before mission start 
*  ver 1.2 - 2016-01-03 major script system rework in progress 
*  ver 1.1 - 2015-05-25 convert to function 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/ 
if !(aceOn) exitWith {}; 
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_healthCareInit start"]; }; 

T_INIT = false; T_Server = false; T_Client = false; T_JIP = false; GAME_OVER = false; 
T_MP = (if (playersNumber east + playersNumber west + playersNumber resistance + playersNumber civilian > 0) then { true } else { false }); 
if (isServer) then { 
    T_Server = true; if (!isNull player) then {T_Client = true}; T_INIT = true; 
} else { 
    T_Client = true; if (isNull player) then { T_JIP = true; [] spawn { waitUntil { !isNull player }; T_INIT = true }; } else { T_INIT = true }; 
}; 
waitUntil { T_INIT }; 

heliDrop_med   = false; publicVariable "heliDrop_med"; 
heliDrop_mash  = false; publicVariable "heliDrop_mash"; 
mash_man_tent  = objNull; 
medHolder = objNull; publicVariable "medHolder"; 

//if (isNil "mash_dropbox") then { mash_dropbox = "B_Slingload_01_Medevac_F" }; 

playable_units = playableUnits; publicVariable "playable_units"; 
if (!isNil "HC") then { playable_units = playable_units - [HC] }; 
if (!isNil "HC2") then { playable_units = playable_units - [HC2] }; 

// MASH SETTINGS
_end_mission    = ["EndMission", 1] call BIS_fnc_getParamValue;  //mash_Array 0  - ends mission when all players are unconscious
_action_color   = "#FBE0A9";  //mash_Array 1 - color for action menu items
_action_color2  = "#ffb600";  //mash_Array 2 - color2 for action menu items
_mash_deploy    = ["DeployMash", 1] call BIS_fnc_getParamValue;  //mash_Array 3 
_mash_type      = ["MashType", 1] call BIS_fnc_getParamValue;  //mash_Array 4 - set as 0 - for vehicle and 1 for man, 2 for man & field mash
_mash_man       = if ((isNil "LeadMedic") || (_mash_type == 0)) then { if !(isNil "p1") then { p1 } else { objNull } } else { LeadMedic };  //mash_Array 5 - name of unit here 
_all_medics     = 0;   //mash_Array 6 - reserved for future
_mash_vehicle   = 0;   //mash_Array 7 - reserved for future
_all_vehicles   = 0;   //mash_Array 8 - reserved for future
_stowDeploys    = 2;   //mash_Array 9 - Can Camo nets be stowed after deployment. 0/1/2 - no/yes/choice
_numDeploys     = 5;   //mash_Array 10 - number of time the mobile mash can deploy camo nets
_timeDeploy     = 13;  //mash_Array 11 - Time to deploy and stow mash tent
// =====================================================================================================================
mash_Array = [_end_mission,_action_color,_action_color2,_mash_deploy,_mash_type,_mash_man,_all_medics,_mash_vehicle,_all_vehicles,_stowDeploys,_numDeploys,_timeDeploy]; publicVariable "mash_Array"; 

fnc_missionEnd = { 
    if !(isServer) exitWith {}; 
    waitUntil { time > 0 }; 
    private ["_unconscious","_sleep"]; 
    _unconscious  = 0; 
    _sleep  = ["Unconscious", 0] call BIS_fnc_getParamValue; 
    sleep 20; 

    while { true } do { 
        _unconscious = 0; 
        if ({ alive _x } count playableUnits != { _x getVariable ["ACE_isUnconscious", false] } count playableUnits) then { _unconscious = 1 }; 
        if (_unconscious == 0) then { 
            sleep _sleep; 
            if ({ alive _x } count playableUnits != { _x getVariable ["ACE_isUnconscious", false] } count playableUnits) then { _unconscious = 1 }; 
            if (_unconscious == 0) exitWith { mission_Over = true; publicVariable "mission_Over" }; 
            sleep 5; 
        }; 
    }; 
}; 

if (isNil "pr_fnc_initRead") then { pr_fnc_initRead = false; }; 

_nul = [] spawn pr_fnc_localHealthCare; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_healthCareInit complete"]; }; 
