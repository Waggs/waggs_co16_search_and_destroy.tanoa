//--- fn_fireMission.sqf 
//--- ver 1.0 2015-07-04
//--- credits to tryteyker for the foundation of this script.
if (isNil "waitForComplete") then { waitForComplete = false; publicVariable "waitForComplete"; }; 

if (waitForComplete) exitWith { hint "Wait for Fire Mission to complete."; }; 
waitForComplete = true; publicVariable "waitForComplete"; 

fn_splash = { 
    private ["_splashRound", "_hitPos", "_artillery"]; 
    _splashRound = _this select 0; 
    _artillery = _this select 1; 
    _hitPos = missionNamespace getVariable "MarkerPositionUsed"; 
    sleep 6; 

    waitUntil {_splashRound distance _hitPos < 500}; 

    _splashHint = format ["%1:  Splash, Out", _artillery]; 
    logic_artillery sideChat _splashHint; 
    logic_artillery_SideChat = _splashHint; publicVariable "logic_artillery_SideChat"; 
}; 

//_artillery = objNull; 
_artillery = [_this, 0, missionNamespace getVariable "ArtilleryUsed", [objNull]] call BIS_fnc_param; // Artillery used. (OBJECT, UNIT) 
_amrounds = [_this, 1, missionNamespace getVariable "AmountRoundsUsed", [0]] call BIS_fnc_param; // Amount of rounds. (NUMBER, INTEGER) 
_delay = [_this, 2, missionNamespace getVariable "SetDelay", [0]] call BIS_fnc_param; // Delay in seconds between shots. (NUMBER, FLOAT) 
_round = [_this, 3,  missionNamespace getVariable "typeShellUsed", [[]]] call BIS_fnc_param; // Type of round used (ARRAY, CLASSNAMES) 
_target = [_this, 4, missionNamespace getVariable "MarkerPositionUsed", [[]], [2, 3]] call BIS_fnc_param; // Target. (POSITION, MARKER) 

_unit = player; 
sleep 2; 
[_artillery] joinSilent (group _unit); 

_gridPos = mapGridPosition _target; 
_roundCount = missionNamespace getVariable "AmountRoundsUsed"; 
_roundsName = _round select 0; 
_roundDisplayName = _roundsName call ISSE_Cfg_Magazine_GetName; //gets display name of rounds
sleep 2; 

_callSupportHint = format ["%1, Requesting [%2]",_artillery, _roundCount]; 
logic_Leader sideChat _callSupportHint; 
logic_Leader_SideChat = _callSupportHint; publicVariable "logic_Leader_SideChat"; 

_callSupportHint2 = format ["%1", _roundDisplayName]; 
logic_Leader sideChat _callSupportHint2; 
logic_Leader_SideChat = _callSupportHint2; publicVariable "logic_Leader_SideChat"; 

_callSupportHint3 = format ["Grid Location %1, over", _gridPos]; 
logic_Leader sideChat _callSupportHint3; 
logic_Leader_SideChat = _callSupportHint3; publicVariable "logic_Leader_SideChat"; 
sleep 2; 

_targetingHint = format ["%1:  Stand by, targeting FIRE MISSION", _artillery]; 
logic_artillery sideChat _targetingHint; 
logic_artillery_SideChat = _targetingHint; publicVariable "logic_artillery_SideChat"; 

// Marker used to mark fire position.
_marker = missionNamespace getVariable "MarkerUsed"; 

_shellType = "";
_shellType = _round select 0; 

//--- Create target
_dummy = createVehicle ["Land_HelipadEmpty_F", _target, [], 0, "FLY"]; 
_dummy setVehicleVarName "DUMMY"; 
_dummy setposATL [_target select 0, _target select 1, 0]; 
_artillery doWatch _dummy; 
sleep 5; 
_artillery doTarget _dummy; 
sleep 5; 


_minDist = 0; 
_maxDist = 0; 

if (_artillery iskindOf "staticMortar") then { 
    _minDist = 100; 
    _maxDist = 3200; 
    if (_shellType == "8Rnd_82mm_Mo_shells") then { 
		//_isInRange = getPos _target inRangeOfArtillery [[_artillery], "8Rnd_82mm_Mo_shells"]
        missionNameSpace setVariable ["myShellUsed", "mortar out"]; 
    } else { 
        if (_shellType == "8Rnd_82mm_Mo_Smoke_white") then { 
            missionNameSpace setVariable ["myShellUsed", "smoke round out"]; 
        } else { 
            if (_shellType == "8Rnd_82mm_Mo_Flare_white") then { 
                missionNameSpace setVariable ["myShellUsed", "flare round out"]; 
            }; 
        }; 
    }; 
} else { 
    if (_artillery iskindOf "MBT_01_arty_base_F") then { 
        _minDist = 900; 
        _maxDist = 30000; 
        if (_shellType == "32Rnd_155mm_Mo_shells") then { 
            missionNameSpace setVariable ["myShellUsed", "155mm HE shell out"]; 
        } else { 
            if (_shellType == "2Rnd_155mm_Mo_guided") then { 
                missionNameSpace setVariable ["myShellUsed", "guided missile out"]; 
            } else { 
                if (_shellType == "6Rnd_155mm_Mo_mine") then { 
                    missionNameSpace setVariable ["myShellUsed", "mine cluster out"]; 
                } else { 
                    if (_shellType == "2Rnd_155mm_Mo_Cluster") then { 
                        missionNameSpace setVariable ["myShellUsed", "cluster shells out"]; 
                    } else { 
                        if (_shellType == "6Rnd_155mm_Mo_smoke") then { 
                            missionNameSpace setVariable ["myShellUsed", "smoke round out"]; 
                        } else { 
                            if (_shellType == "2Rnd_155mm_Mo_LG") then { 
                                missionNameSpace setVariable ["myShellUsed", "laser guided missile out"]; 
                            } else { 
                                if (_shellType == "6Rnd_155mm_Mo_AT_mine") then { 
                                    missionNameSpace setVariable ["myShellUsed", "at mine cluster out"]; 
                                }; 
                            }; 
                        }; 
                    }; 
                }; 
            }; 
        }; 
    } else { 
        if (_artillery iskindOf "MBT_01_mlrs_base_F") then { 
            _minDist = 830; 
            _maxDist = 73000; 
            if (_shellType == "12Rnd_230mm_rockets") then { //230mm Titan Missile
                missionNameSpace setVariable ["myShellUsed", "230mm rocket out"]; 
            }; 
        }; 
    }; 
}; 

//--- Fire Rounds 
missionNameSpace setVariable ["noShell", 0]; 
_targetdist = _artillery distance _target; 

for "_s" from 1 to _amrounds do { 
    if (_targetdist < _minDist) then { 
        missionNameSpace setVariable ["noShell", 1]; 
    } else { 
        if (_targetdist > _maxDist) then { 
            missionNameSpace setVariable ["noShell", 2]; 
        } else { 
            _drift = 0; 
            if (_shellType == "2Rnd_155mm_Mo_LG") then { 
                _drift = 0; 
            } else { 
                if (_shellType == "12Rnd_230mm_rockets") then { 
                    _drift = 3; 
                } else { 
                    _drift = 15; 
                }; 
            }; 
            artyFired = 0; 
            _hitpos = [_target, random _drift, random 360] call BIS_fnc_relPos; 
            sleep _delay; 
            _artillery commandArtilleryFire [_hitpos, _shellType, 1]; 

            _artillery addEventHandler ["Fired", { 
                _artillery = _this select 0; 
                _splashRound = _this select 6; 
                [_splashRound, _artillery] spawn fn_splash; 
                artyFired = artyFired + 1; 
           }]; 

            waitUntil { artyFired > 0 }; 
            artyFired = 0; 

            _artilleryFiredHint = format ["%1:  Fired...  %2", _artillery, (missionNameSpace getVariable "myShellUsed")]; 
            logic_artillery sideChat _artilleryFiredHint; 
            logic_artillery_SideChat = _artilleryFiredHint; publicVariable "logic_artillery_SideChat"; 

            _artillery removeAllEventHandlers "FIRED"; 
            missionNameSpace setVariable ["noShell", 3]; 
        }; 
    }; 
}; 

sleep 5; 

if (missionNameSpace getVariable "noShell" == 1) then { 
    _tooCloseHint = format ["%1:  Cannot Fire, target is too close", _artillery]; 
    logic_artillery sideChat _tooCloseHint; 
    logic_artillery_SideChat = _tooCloseHint; publicVariable "logic_artillery_SideChat"; 
} else { 
    if (missionNameSpace getVariable "noShell" == 2) then { 
        _tooFarHint = format ["%1:  Cannot Fire, target is too far", _artillery]; 
        logic_artillery sideChat _tooFarHint; 
        logic_artillery_SideChat = _tooFarHint; publicVariable "logic_artillery_SideChat"; 
    } else { 
        if (missionNameSpace getVariable "noShell" == 3) then { 
            _roundsCompleteHint = format ["%1:  Rounds complete", _artillery]; 
            logic_artillery sideChat _roundsCompleteHint; 
            logic_artillery_SideChat = _roundsCompleteHint; publicVariable "logic_artillery_SideChat"; 
        } else { 
            if (missionNameSpace getVariable "noShell" == 0) then { 
                _noAquireHint = format ["%1:  Cannot acquire target, resend orders", _artillery]; 
                logic_artillery sideChat _noAquireHint; 
                logic_artillery_SideChat = _noAquireHint; publicVariable "logic_artillery_SideChat"; 
            }; 
        }; 
    }; 
}; 

[_artillery] join grpNull; 
deleteVehicle _dummy; 
waitForComplete = false; publicVariable "waitForComplete"; 
[] spawn TRT_fnc_delMarker; 
