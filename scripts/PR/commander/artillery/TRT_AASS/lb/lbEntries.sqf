
// We wait until the dialog is created and open before executing the script.
waitUntil {!isNull (findDisplay 1111) && {dialog}}; 

// These variables catch a variety of things, from config entries to IDCs.
_listBox = 1500; // ListBox (TRT_LB1) IDC.
_display = 1006; // RscText (TRT_SELARTYRES) IDC.

_listBoxAmmoCount = 3001; 
_listBoxAmmo = 3002; // ListBox (PR_AVAILABLEAMMOLIST) 

_cfg = configFile >> "cfgVehicles"; // Config path to CfgVehicles.
_player = typeOf player; // Which type of unit the player is (ClassName).
_playerside = getNumber (_cfg >> _player >> "side"); // This fetches the side value of the player. 0 = CSAT, 1 = NATO, 2 = AAF.
_count = 0; // Simple count array to limit units in ListBox.
_ArtyUnits = []; // Used for storing all units names.

{
    // This whole scope is there to add names, pictures, and set data of the artillery units on the players side.
    // All this gets then added to the ListBox with IDC 1500.
    // The lbSetData Values are referenced in the main functions\fn_fireMission.sqf script, aswell as sub-scripts in lb\selection.
    // Full credits to Iceman77 for this part of the script. Modified accordingly by tryteyker.
    _className = configName (_cfg >> typeOf _x); // Fetches class name of unit.
    _side = getNumber (_cfg >> _className >> "side"); // Gets side value of unit (uses same base as _playerside, look there for notes).
    _displayName = getText (_cfg >> _className >> "displayName"); // Fetches display name of unit.
    _picture = getText (_cfg >> _className >> "picture"); // Gets picture of unit (as defined in config, black and white pictures).
    _artillery = getNumber (_cfg >> _classname >> "artilleryScanner"); // This value identifies the unit as artillery. The value is unique to artillery, units that do not have arty computers have this set to 0.
  
    if (_artillery == 1 && {_side == _playerside} && {_count < 4}) then { // All 3 conditions have to be true, but cond 2 & 3 are only evaluated if the preceding one is true. Lazy Evaluation.
        _index = lbAdd [_listBox, _displayName]; // Add display name to ListBox entry. Goes from 0 to 3. (0-index based)
        lbSetPicture [_listBox, _index, _picture]; // Adds the previously defined picture to the listbox entry. Again, 0 to 3. It will show up to the left of the text.
        lbSetData [_listBox, _index, _className]; // This command is used for later reference, it has no immediate effect on the listbox.
        _ArtyUnits = _ArtyUnits + [_x];
        _count = _count + 1; 
        z_ArtyUnits = _ArtyUnits; 
    }; 
} forEach vehicles; // This whole loop executes for all vehicles on map, regardless if they're friendly to player or not.

if (isNil "ammoIndex") then { 
    ammoIndex = 0; 
}; 
if (isNil "ammoCountIndex") then { 
    ammoCountIndex = 0; 
}; 

fn_artyAmmo = { 
    //private ["_rounds","_listBoxAmmo","_arty","_listBoxAmmoCount"]; 
    _rounds = _this select 0; 
    _listBoxAmmo = _this select 1; 
    _arty = _this select 2; 
    _listBoxAmmoCount = _this select 3; 

    if ((count _rounds) > 0) then { 
        _roundsName0 = (_rounds select 0) select 0; //classname of rounds
        _roundsCount0 = (_rounds select 0) select 1; //amount of rounds //155mm HE Shells
        _displayName0 = _roundsName0 call ISSE_Cfg_Magazine_GetName; //gets display name of rounds
        _index0 = lbAdd [_listBoxAmmo, _displayName0]; // Add rounds & display name to ListBox entry. 
        _textValue0 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString0 = format ["%1", _roundsCount0]; //convert rounds & display name to a string
        _count0 = lbAdd [_listBoxAmmoCount, _roundsString0]; // Add rounds & display name to ListBox entry. 
        _textValueCount0 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 1) then { 
        _roundsName1 = (_rounds select 1) select 0; 
        _roundsCount1 = (_rounds select 1) select 1; //guided
        _displayName1 = _roundsName1 call ISSE_Cfg_Magazine_GetName; 
        _index1 = lbAdd [_listBoxAmmo, _displayName1]; 
        _textValue1 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString1 = format ["%1", _roundsCount1]; 
        _count1 = lbAdd [_listBoxAmmoCount, _roundsString1]; 
        _textValueCount1 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 2) then { 
        _roundsName2 = (_rounds select 2) select 0; 
        _roundsCount2 = (_rounds select 2) select 1; //guided
        _displayName2 = _roundsName2 call ISSE_Cfg_Magazine_GetName; 
        _index2 = lbAdd [_listBoxAmmo, _displayName2]; 
        _textValue2 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString2 = format ["%1", _roundsCount2]; 
        _count2 = lbAdd [_listBoxAmmoCount, _roundsString2]; 
        _textValueCount2 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 3) then { 
        _roundsName3 = (_rounds select 3) select 0; 
        _roundsCount3 = (_rounds select 3) select 1; //Mine Cluster 
        _displayName3 = _roundsName3 call ISSE_Cfg_Magazine_GetName; 
        _index3 = lbAdd [_listBoxAmmo, _displayName3]; 
        _textValue3 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString3 = format ["%1", _roundsCount3]; 
        _count3 = lbAdd [_listBoxAmmoCount, _roundsString3]; 
        _textValueCount3 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 4) then { 
        _roundsName4 = (_rounds select 4) select 0; 
        _roundsCount4 = (_rounds select 4) select 1; //Cluster Shells
        _displayName4 = _roundsName4 call ISSE_Cfg_Magazine_GetName; 
        _index4 = lbAdd [_listBoxAmmo, _displayName4]; 
        _textValue4 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString4 = format ["%1", _roundsCount4]; 
        _count4 = lbAdd [_listBoxAmmoCount, _roundsString4]; 
        _textValueCount4 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 5) then { 
        _roundsName5 = (_rounds select 5) select 0; 
        _roundsCount5 = (_rounds select 5) select 1; //Smoke (White)
        _displayName5 = _roundsName5 call ISSE_Cfg_Magazine_GetName; 
        _index5 = lbAdd [_listBoxAmmo, _displayName5]; 
        _textValue5 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString5 = format ["%1", _roundsCount5]; 
        _count5 = lbAdd [_listBoxAmmoCount, _roundsString5]; 
        _textValueCount5 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 6) then { 
        _roundsName6 = (_rounds select 6) select 0; 
        _roundsCount6 = (_rounds select 6) select 1; //Laser Guided
        _displayName6 = _roundsName6 call ISSE_Cfg_Magazine_GetName; 
        _index6 = lbAdd [_listBoxAmmo, _displayName6]; 
        _textValue6 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString6 = format ["%1", _roundsCount6]; 
        _count6 = lbAdd [_listBoxAmmoCount, _roundsString6]; 
        _textValueCount6 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    if ((count _rounds) > 7) then { 
        _roundsName7 = (_rounds select 7) select 0; 
        _roundsCount7 = (_rounds select 7) select 1; //AT Mine Cluster
        _displayName7 = _roundsName7 call ISSE_Cfg_Magazine_GetName; 
        _index7 = lbAdd [_listBoxAmmo, _displayName7]; 
        _textValue7 = lbText [_listBoxAmmo, ammoIndex]; 
        ammoIndex = ammoIndex + 1; 
        _roundsString7 = format ["%1", _roundsCount7]; 
        _count7 = lbAdd [_listBoxAmmoCount, _roundsString7]; 
        _textValueCount7 = lbText [_listBoxAmmoCount, ammoCountIndex]; 
        ammoCountIndex = ammoCountIndex + 1; 
    }; 
    missionNamespace setVariable ["ArtilleryReady", 1]; 
}; 

while {dialog} do { 

    switch (lbCurSel _listBox) do {

        case 0: {
            private ["_rounds"]; 
            missionNamespace setVariable ["ArtilleryUsed", _ArtyUnits select 0]; 
            missionNamespace setVariable ["ArtillerySelected", 1]; 
            _rounds = magazinesAmmo (missionNamespace getVariable "ArtilleryUsed"); //array
            _arty = missionNamespace getVariable "ArtilleryUsed"; 
            ammoIndex = ammoIndex + 1;
            [_rounds, _listBoxAmmo, _arty, _listBoxAmmoCount] spawn fn_artyAmmo; 
            _textValue = lbText [_listBox,0];
            ctrlSetText [_display, _textValue]; 
        };
        case 1: {
            private ["_rounds"]; 
            missionNamespace setVariable ["ArtilleryUsed", _ArtyUnits select 1];
            missionNamespace setVariable ["ArtillerySelected", 1]; 
            _rounds = magazinesAmmo (missionNamespace getVariable "ArtilleryUsed"); 
            _arty = missionNamespace getVariable "ArtilleryUsed"; 
            ammoIndex = ammoIndex + 1; 
            [_rounds, _listBoxAmmo, _arty, _listBoxAmmoCount] spawn fn_artyAmmo; 
            _textValue = lbText [_listBox,1];
            ctrlSetText [_display, _textValue]; 
        };
        case 2: {
            private ["_rounds"]; 
            missionNamespace setVariable ["ArtilleryUsed", _ArtyUnits select 2];
            missionNamespace setVariable ["ArtillerySelected", 1]; 
            _rounds = magazinesAmmo (missionNamespace getVariable "ArtilleryUsed"); 
            _arty = missionNamespace getVariable "ArtilleryUsed"; 
            ammoIndex = ammoIndex + 1; 
            [_rounds, _listBoxAmmo, _arty, _listBoxAmmoCount] spawn fn_artyAmmo; 
            _textValue = lbText [_listBox,2];
            ctrlSetText [_display, _textValue]; 
        };
        case 3: {
            private ["_rounds"]; 
            missionNamespace setVariable ["ArtilleryUsed", _ArtyUnits select 3];
            missionNamespace setVariable ["ArtillerySelected", 1]; 
            _rounds = magazinesAmmo (missionNamespace getVariable "ArtilleryUsed"); 
            _arty = missionNamespace getVariable "ArtilleryUsed"; 
            ammoIndex = ammoIndex + 1; 
            [_rounds, _listBoxAmmo, _arty, _listBoxAmmoCount] spawn fn_artyAmmo; 
            _textValue = lbText [_listBox,3];
            ctrlSetText [_display, _textValue]; 
        };
    };

    sleep 5; 
    lbClear 3001; 
    lbClear 3002; 
    if (ammoIndex > 500) then { 
        ammoIndex = 0; 
    }; 
    if (ammoCountIndex > 500) then { 
        ammoCountIndex = 0; 
    }; 
}; 

missionNamespace setVariable ["ArtillerySelected", 0]; 
missionNamespace setVariable ["ArtilleryReady", 0]; 
