// Maintains the text box on the right below "SELECTED ROUNDS". 
// Shows amount of rounds selected as a text value, as well as updating missionNamespace variable "RoundsUsed" for future reference.

waitUntil {!isNull (findDisplay 1111) && {dialog}}; 

_values = ["1","2","3","4","5","6","7","8"]; 
_display = 2100; 

{ 
    _index = lbAdd [_display, _x]; 
} forEach _values; 

while {dialog} do { 
    waitUntil {lbCurSel _display != -1}; 
    missionNamespace setVariable ["AmountRoundsUsed", lbCurSel _display + 1]; 
    ctrlSetText [1008, "Amount: " + str (lbCurSel _display + 1)]; 
    missionNamespace setVariable ["amountRoundsSelected", 1]; 
}; 

missionNamespace setVariable ["amountRoundsSelected", 0]; 
