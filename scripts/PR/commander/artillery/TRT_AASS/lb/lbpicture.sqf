// This adds a picture of the current world the player is playing on (eg Altis, Stratis). It fetches the picture of the map and displays it in the top right.
// It works with all maps that have a properly defined "pictureMap" parameter in their config.

// We wait until the dialog is created and open until executing this script.
waitUntil {!isNull (findDisplay 1111) && {dialog}}; 
_display = 1200; // RscPicture (TRT_PIC1) IDC value.
_cfg = configFile >> "CfgWorlds"; // Config path to "CfgWorlds".
_world = worldName; // Fetch world name of current world (eg. Altis)
_picture = getText (_cfg >> _world >> "pictureMap"); // Use _cfg + _world to reach "pictureMap" value. This is the main picture of the map.
_unavailable = "TRT_AASS\pictures\mapnotavailable.png"; // Custom-made image that displays instead of the _picture value, in cases where the picture is not properly defined.

if (_picture != "") then { 
    ctrlSetText [_display, _picture]; 
} else { 
    ctrlSetText [_display, _unavailable]; 
}; 




/*

waitUntil {!isNull (findDisplay 1111) && {dialog}}; 
_display = 38555; // RscPicture (TRT_PIC1) IDC value.
//_cfg = configFile >> "CfgWorlds"; // Config path to "CfgWorlds".
//_world = worldName; // Fetch world name of current world (eg. Altis)
//_picture = getText (_cfg >> _world >> "pictureMap"); // Use _cfg + _world to reach "pictureMap" value. This is the main picture of the map.
_picture = ( [ "myMiniMap" ] call BIS_fnc_rscLayer ) cutRsc [ "myMap", "PLAIN", 1, false ]; .
//_unavailable = "TRT_AASS\pictures\mapnotavailable.paa"; // Custom-made image that displays instead of the _picture value, in cases where the picture is not properly defined.
lbSetPicture [_display, _picture]; // Adds the previously defined picture to the listbox entry. Again, 0 to 3. It will show up to the left of the text.
//if (_picture != "") then { 
    //ctrlSetText [_display, _picture]; 
//} else { 
//    ctrlSetText [_display, _unavailable]; 
//}; 
*/