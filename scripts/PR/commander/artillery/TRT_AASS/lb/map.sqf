// This script runs when the player presses the "Map" Button on the GUI. 
// It opens the map, closes the dialog, and opens everything again once the player has closed the map. 

openMap true; 
closeDialog 0; 
hint "Dialog closed. It will open again once you close the map."; 
waitUntil {!visibleMap}; 
createDialog "TRT_ArtyGUI"; 

[] execVM "scripts\PR\commander\artillery\TRT_AASS\lb\lbEntries.sqf"; // Add entries to listbox 1500. 
//[] execVM "scripts\PR\commander\artillery\TRT_AASS\lb\lbPicture.sqf"; // Add pictures to RscPicture 1200. 
[] execVM 'scripts\PR\commander\artillery\TRT_AASS\lb\selection\amountRoundsSelect.sqf'; // Add round selections. 
[] execVM "scripts\PR\commander\artillery\TRT_AASS\lb\selection\delaySelect.sqf"; // Add delay selections. 
[] execVM "scripts\PR\commander\artillery\TRT_AASS\lb\selection\typeShellSelect.sqf"; // Add ammo type selection. 
[] execVM "scripts\PR\commander\artillery\TRT_AASS\lb\control\fire.sqf"; // Control FIRE button. 