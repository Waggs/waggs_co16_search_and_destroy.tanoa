// This scipt fires after lb\selection\markerSelect.sqf. It only fires if no pre-placed marker that meets marker criteria (DESTROY type, colored red) was found on the map.

//_unit = player; 

// We start with an opening hint that informs the player that he has to place a marker.
hint "No pre-set marker found. Place one on the map by using a single click."; 

// Then we close the dialog "TRT_ArtyGUI" and open the map. 
closeDialog 0; 
openMap true; 

// This stacked EH calls functions\fn_mapClick.sqf each time the player clicks on the map.
_mapclick = ["MapClickRef", "onMapSingleClick", "call TRT_fnc_mapClick"] call BIS_fnc_addStackedEventHandler; 


// We wait until the missionNamespace var "MarkerSetByClick" equals the STRING true. 
// The reason why a STRING and not BOOL value is used because using a BOOL value can cause unexpected issues (and has during testing).

waitUntil {(missionNamespace getVariable "MarkerSetByClick") == 1}; 

// We remove the stacked EH and call the main functions\fn_fireMission.sqf. 
["MapClickRef", "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler; 
hint "Marker placed."; 

call TRT_fnc_fireMission; 
