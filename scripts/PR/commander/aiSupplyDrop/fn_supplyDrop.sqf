/* fn_supplyDrop.sqf 
*  Author: PapaReap 

*  ver 1.3 - 2016-01-07 code cleanup and made into function 
*  ver 1.3 - 2015-07-18 compacted all scripts into 1
*  ver 1.2 - 2015-01-01 more refinement
*  ver 1.1 - 2014-12-26 complete medical system rework in progress
*  ver 1.0 - 2014-12-15

*  "medHeliSpawn" marker required in mission for spawn location // side  1 = Blufor, 2 = Opfor, 3 = Indedpendent // gunner crew  0 = off, 1 = ON
*  ["spawn marker", "drop marker", side, gunner crew, "type of crate", custom crate, grid location of caller]

*  ["medHeliSpawn", "medDrop", 1, 1, Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf";

*  From a trigger use:
*  if !(heliSupplyDrop) then {["medHeliSpawn", "medDrop", 1, 1, "AGM_Box_Medical", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf"} else {hint format["Only run one instance of AiLift at a time"];};

*  if (true) then {["medHeliSpawn", "medDrop", 1, 1, "Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf"};
*/ //diag_log format ["***test***"];

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_supplyDrop start"]; }; 

if !(isServer) exitWith {}; 

private ["_spawn","_drop","_side","_crew","_crate","_custom","_gridPos"]; 

_spawnPos = [0,0,0]; 

_caller = _this select 0; 
_supplyDropArr = _caller getVariable "dropArray"; 
_spawn      = _supplyDropArr select 0; 
_drop       = _supplyDropArr select 1; 
_side       = _supplyDropArr select 2; 
_crew       = _supplyDropArr select 3; 
_crate      = _supplyDropArr select 4; 
_custom     = _supplyDropArr select 5; 
_gridPos    = _supplyDropArr select 6; 
_typeDrop   = _supplyDropArr select 7; 
_typeSupply = _supplyDropArr select 8; 

_dropbox    = _crate createVehicle (getMarkerPos _spawn); 
_rtb        = "Land_HelipadEmpty_F" createVehicle (getMarkerPos _spawn); 

_spawnPos = getMarkerPos _spawn; 
_dropPos = getMarkerPos _drop; 
_dir = [_spawnPos, _dropPos] call BIS_fnc_dirTo; 
pr_newPos = [_dropPos, 50, _dir] call BIS_fnc_relPos; 
pr_newPos2 = [pr_newPos, 150, _dir] call BIS_fnc_relPos; 

_dropland1  = "Land_HelipadEmpty_F" createVehicle pr_newPos; 
_supplyHeli = objNull; 

if (isNil "noHCSwap") then { noHCSwap = []; publicVariable "noHCSwap"; }; 
sleep 1; 
_deployActionName = ""; 
if (_custom == 1) then { 
    if (_typeDrop == "Ammo") then { 
        [[_dropbox], "fnc_natoBoxAmmo"] call BIS_fnc_MP; 
    } else { 
        if (_typeDrop == "Med") then { 
            [[_dropbox], "fnc_aceMedBox"] call BIS_fnc_MP; 
        } else { 
            if (_typeDrop == "Mash") then { 
                [[_dropbox], "fnc_aceMedBox"] call BIS_fnc_MP; 
                _dropbox setVariable ["fieldMash", 0, true]; 
                _dropbox setVariable ["fieldMash_deployed", "false", true]; 
                _dropbox setVariable ["fieldMash_reset", "false", true]; 
                _dropbox setVariable ["mashNew", 1, true]; 
            } else { 
                if (_typeDrop == "MH9") then { 
                    [[_dropbox], "fnc_natoBoxAmmo"] call BIS_fnc_MP; 
                }; 
            }; 
        }; 
    }; 
}; 

sleep 10; 
//if (_side == 1) then {
if ((_typeDrop == "Ammo") || (_typeDrop == "Med")) then { 
    _supplyHeli = "b_heli_Transport_01_camo_F" createVehicle (getMarkerPos _spawn); 
    if (true) then { _supplyHeli animateDoor ['door_R', 1]; _supplyHeli animateDoor ['door_L', 1] }; 
} else { 
    if ((_typeDrop == "Mash") || (_typeDrop == "MH9")) then { 
        _supplyHeli = "B_Heli_Transport_03_F" createVehicle (getMarkerPos _spawn); 
    }; 
}; 

pr_supplyHeli = _supplyHeli; 
missionNameSpace setVariable ["scriptFinished", 0]; 

while { ((alive _supplyHeli) && (canMove _supplyHeli) && (missionNameSpace getVariable "scriptFinished" == 0)) } do { 
    scopeName "loop1"; 
    _supplyHeli allowDamage false; 
    _grouppilots = createGroup West; 
    "b_helipilot_F" createUnit [getMarkerPos _spawn, _grouppilots, "this moveInDriver _supplyHeli", 0.6, "corporal"]; 
    "b_helipilot_F" createUnit [getMarkerPos _spawn, _grouppilots, "this moveInTurret [_supplyHeli, [0]]", 0.6, "corporal"]; 
    missionNameSpace setVariable ["supplyHeliPilots", _grouppilots]; 
    _grouppilots allowFleeing 0; 
    { _x allowDamage false } forEach units _grouppilots; 
    { noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _grouppilots; 

    missionNameSpace setVariable ["supplyHeliCrew", grpNull]; 
    if (_crew == 1) then { 
        _grpCrew = createGroup West; 
        "b_helicrew_F" createUnit [getMarkerPos _spawn, _grpCrew, "this moveInTurret [_supplyHeli, [1]]", 0.6, "corporal"]; 
        "b_helicrew_F" createUnit [getMarkerPos _spawn, _grpCrew, "this moveInTurret [_supplyHeli, [2]]", 0.6, "corporal"]; 
        missionNameSpace setVariable ["supplyHeliCrew", _grpCrew]; 
        _grpCrew allowFleeing 0; 
        _grpCrew setBehaviour "COMBAT"; 
        { noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _grpCrew; 
    }; 

    _supplyDropRequestHint = format ["Carrier Pigeon, Squad Lead is Requesting a %1 Drop at Grid %2", _typeSupply, _gridPos]; 
    logic_firefly sideChat _supplyDropRequestHint; 
    logic_firefly_SideChat = _supplyDropRequestHint; publicVariable "logic_firefly_SideChat"; 

    _dropbox allowDamage false; 

    _waypoint2 = _grouppilots addWaypoint [pr_newPos2, 2]; 
    _waypoint2 setWayPointBehaviour "CARELESS"; 
    _waypoint2 setWayPointSpeed "NORMAL"; 
    _waypoint2 setWayPointType "MOVE"; 
    _waypoint2 setWayPointCombatMode "WHITE"; 

    waitUntil { (position _supplyHeli select 2 > 10) }; 
    _hvelocity = velocity _supplyHeli; 
    _dropbox setVelocity _hvelocity; 

    if ((_typeDrop == "Ammo") || (_typeDrop == "Med")) then { 
            _dropbox attachTo [_supplyHeli, [0, 2, -5]]; 
    } else { 
        if ((_typeDrop == "Mash") || (_typeDrop == "MH9")) then { 
            _dropbox attachTo [_supplyHeli, [0, 2, -7.5]]; 
        }; 
    }; 
    sleep 1; 

    _copyBaseFireflyHint = format ["Copy Base Firefly, %1 Drop in Route to Grid Location %2", _typeSupply, _gridPos]; 
    logic_pigeon sideChat _copyBaseFireflyHint; 
    logic_pigeon_SideChat = _copyBaseFireflyHint; publicVariable "logic_pigeon_SideChat"; 

    _supplyHeli flyInHeight 100; 

    waitUntil { ((_supplyHeli distance2D _dropland1 < 500) || (!alive _supplyHeli) || (!canMove _supplyHeli)) }; 
    _supplyHeli allowDamage true; 
    { _x allowDamage true } forEach units _grouppilots; 
    if ((!alive _supplyHeli) || (!canMove _supplyHeli)) then { breakOut "loop1" }; 

    waitUntil { 
        ((_supplyHeli distance2D _dropland1 < 40) || (speed _supplyHeli < 2) || (!alive _supplyHeli) || (!canMove _supplyHeli)) 
    }; 

    if ((!alive _supplyHeli) || (!canMove _supplyHeli)) then { breakOut "loop1" }; 
    detach _dropbox; 
    sleep 2; 

    _velocity = velocity _dropbox; 
    _chute = createVehicle ["B_Parachute_02_F", [100, 100, 200], [], 0, 'FLY']; 
    _chute setPos [position _dropbox select 0, position _dropbox select 1, (position _dropbox select 2) - 4]; 
    _chute setVelocity _velocity; 
    _dropbox attachTo [_chute, [0, 0, -0.6]]; 

    _packageDroppedHint = format ["Base Firefly, %1 Dropped at Grid %2", _typeSupply, _gridPos]; 
    logic_pigeon sideChat _packageDroppedHint; 
    logic_pigeon_SideChat = _packageDroppedHint; publicVariable "logic_pigeon_SideChat"; 
    sleep 5; 

    _copyRTBHint = format ["Copy Carrier Pigeon, RTB"]; 
    logic_firefly sideChat _copyRTBHint; 
    logic_firefly_SideChat = _copyRTBHint; publicVariable "logic_firefly_SideChat"; 

    _waypoint3 = _grouppilots addWaypoint [getPos _rtb, 5]; 
    _waypoint3 setWayPointBehaviour "CARELESS"; 
    _waypoint3 setWayPointSpeed "FULL"; 
    _waypoint3 setWayPointType "MOVE"; 
    _waypoint3 setWayPointCombatMode "WHITE"; 

    if (((getPos _dropbox) select 2) > 3) then { [_dropbox, _typeDrop] spawn pr_fnc_findBoxAir; }; 

    waitUntil { position _dropbox select 2 < 0.5 || isNull _chute; }; 
    detach _dropbox; 
    _dropbox setPos [position _dropbox select 0, position _dropbox select 1, 0]; 
    sleep 5; 
    if !(isNull _chute) then { deleteVehicle _chute }; 
    _dropbox allowDamage true; 

    if ({_x distance _dropbox > 50} count playableUnits >0 ) then { [_dropbox, _typeDrop] spawn pr_fnc_findBoxLand; }; 

    waitUntil { ((_supplyHeli distance _rtb < 400) || (!alive _supplyHeli) || (!canMove _supplyHeli)) }; 
    if ((!alive _supplyHeli) || (!canMove _supplyHeli)) then { breakOut "loop1" }; 
    _supplyHeli land "Land"; 
    waitUntil { ((((getPos _supplyHeli) select 2) < 0.5) || (!alive _supplyHeli) || (!canMove _supplyHeli)) }; 
    missionNameSpace setVariable ["scriptFinished", 1]; 
    if ((!alive _supplyHeli) || (!canMove _supplyHeli)) then { breakOut "loop1" }; 
    sleep 5; 

    _pigeonHasRTBHint = format ["Base Firefly, Carrier Pigeon has Returned to Base. Resupplying in Progress..."]; 
    logic_pigeon sideChat _pigeonHasRTBHint; 
    logic_pigeon_SideChat = _pigeonHasRTBHint; publicVariable "logic_pigeon_SideChat"; 
    sleep 5; 
    _copyPigeonRTBHint = format ["Copy Carrier Pigeon, Respond When Ready for New Assignment, Out"]; 
    logic_firefly sideChat _copyPigeonRTBHint; 
    logic_firefly_SideChat = _copyPigeonRTBHint; publicVariable "logic_firefly_SideChat"; 
}; 

sleep 120; 
if ((alive _supplyHeli) && (canMove _supplyHeli) && (missionNameSpace getVariable "scriptFinished" == 1)) then { 
    _pigeonIsReadyHint = format ["Base Firefly, Carrier Pigeon is Ready for Next Assignment"]; 
    logic_pigeon sideChat _pigeonIsReadyHint; 
    logic_pigeon_SideChat = _pigeonIsReadyHint; publicVariable "logic_pigeon_SideChat"; 
    sleep 6; 

    _copyStandbyHint = format ["Copy Carrier Pigeon, Standby for Further Orders"]; 
    logic_firefly sideChat _copyStandbyHint; 
    logic_firefly_SideChat = _copyStandbyHint; publicVariable "logic_firefly_SideChat"; 
} else { 
    if ((!alive _supplyHeli) || (!canMove _supplyHeli) || (missionNameSpace getVariable "scriptFinished" != 1)) then { 
        _noResponseHint = format ["Carrier Pigeon, Do You Copy"]; 
        logic_firefly sideChat _noResponseHint; 
        logic_firefly_SideChat = _noResponseHint; publicVariable "logic_firefly_SideChat"; 
        sleep 30; 
        _noResponseHint2 = format ["Carrier Pigeon, Repeat, Do You Copy"]; 
        logic_firefly sideChat _noResponseHint2; 
        logic_firefly_SideChat = _noResponseHint2; publicVariable "logic_firefly_SideChat"; 
        sleep 60; 
        _newPigeonHint = format ["Squad Lead, Carrier Pigeon is Non-Responsive, Assigning New Carrier, Out"]; 
        logic_firefly sideChat _newPigeonHint; 
        logic_firefly_SideChat = _newPigeonHint; publicVariable "logic_firefly_SideChat"; 
    }; 
}; 

//--- clean-up time 
deleteVehicle _dropland1; 
deleteVehicle _rtb; 
deleteVehicle _supplyHeli; 
_grouppilots = missionNameSpace getVariable "supplyHeliPilots"; 
{ noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap" } forEach units _grouppilots; 
{ deleteVehicle _x } forEach units _grouppilots; deleteGroup _grouppilots; 
{ _x = nil } forEach (units _grouppilots); 

_grpCrew = missionNameSpace getVariable "supplyHeliCrew"; 
{ noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap" } forEach units _grpCrew; 
{ deleteVehicle _x } forEach units _grpCrew; deleteGroup _grpCrew; 
{ _x = nil } forEach (units _grpCrew); 

if (missionNameSpace getVariable "supplyDropMarker" != "") then { 
    _supplyDrop = missionNameSpace getVariable "supplyDropMarker"; 
    if (getMarkerColor _supplyDrop == "ColorBlue") then { deleteMarker _supplyDrop }; 
    missionNameSpace setVariable ["supplyDropMarker", ""]; 
}; 

if (true) exitWith { heliSupplyDrop = false; publicVariable "heliSupplyDrop" }; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_supplyDrop complete"]; }; 
