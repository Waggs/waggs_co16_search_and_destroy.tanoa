/* fn_supplyDrop.sqf 
*  Author: PapaReap 

*  ver 1.3 - 2016-01-07 code cleanup and made into function 
*  ver 1.3 - 2015-07-18 compacted all scripts into 1
*  ver 1.2 - 2015-01-01 more refinement
*  ver 1.1 - 2014-12-26 complete medical system rework in progress
*  ver 1.0 - 2014-12-15

*  "medHeliSpawn" marker required in mission for spawn location // side  1 = Blufor, 2 = Opfor, 3 = Indedpendent // gunner crew  0 = off, 1 = ON
*  ["spawn marker", "drop marker", side, gunner crew, "type of crate", custom crate, grid location of caller]

*  ["medHeliSpawn", "medDrop", 1, 1, Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf";

*  From a trigger use:
*  if !(heliSupplyDrop) then {["medHeliSpawn", "medDrop", 1, 1, "AGM_Box_Medical", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf"} else {hint format["Only run one instance of AiLift at a time"];};

*  if (true) then {["medHeliSpawn", "medDrop", 1, 1, "Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf"};
*/ //diag_log format ["***test***"];

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_findBoxLand start"]; }; 

private["_dropbox", "_chemlight", "_smoke"]; 
_dropbox = _this select 0; 
_typeDrop = _this select 1; 
_chemlight = ""; 
_smoke = ""; 
_flare = ""; 
scopename "findLandScope"; 
if (_typeDrop == "Ammo") then { 
    _chemlight = "Chemlight_Blue"; 
    _smoke = "SmokeShellBlue"; 
    _flare = "F_40mm_white"; 
} else { 
    if (_typeDrop == "Med") then { 
        _chemlight = "Chemlight_Red"; 
        _smoke = "SmokeShellRed"; 
        _flare = "F_40mm_red"; 
    } else { 
        if (_typeDrop == "Mash") then { 
            _chemlight = "Chemlight_Green"; 
            _smoke = "SmokeShellGreen"; 
            _flare = "F_40mm_white"; 
        } else { 
            if (_typeDrop == "MH9") then { 
                _chemlight = "Chemlight_Blue"; 
                _smoke = "SmokeShellBlue"; 
                _flare = ""; 
            }; 
        }; 
    }; 
}; 

while { ({ _x distance _dropbox > 60 } count playableUnits >0 ) } do { 
    if ({ _x distance _dropbox < 60 } count playableUnits >0 ) then { breakto "findLandScope" }; 
    if !(isNil "_lightLand") then { deleteVehicle _lightLand }; 
    if !(isNil "_smokeLand") then { deleteVehicle _smokeLand }; 
    _lightLand =  _chemlight createVehicle position _dropbox; 
    _smokeLand =  _smoke createVehicle position _dropbox; 
    _smokeLand attachTo [_dropbox, [0,0,0]]; 
    sleep 50; 
    if ({ _x distance _dropbox < 60 } count playableUnits >0 ) then { deleteVehicle _lightLand; deleteVehicle _smokeLand; breakto "findLandScope" }; 
    sleep 50; 
    if ({ _x distance _dropbox < 60 } count playableUnits >0 ) then { 
        if !(isNil "_lightLand") then { deleteVehicle _lightLand }; if !(isNil "_smokeLand") then { deleteVehicle _smokeLand }; breakto "findLandScope"; 
    }; 
    _smokeLand =  _smoke createVehicle position _dropbox; 
    _smokeLand attachTo [_dropbox,[0,0,0]]; 
    _flareLand = _flare createVehicle position _dropbox; 
    _flareLand attachTo [_dropbox,[0,0,0]]; 
    sleep 50; 
    if ({ _x distance _dropbox < 60 } count playableUnits >0 ) then { deleteVehicle _lightLand; deleteVehicle _smokeLand; breakto "findLandScope" }; 
    sleep 50; 
    if ({ _x distance _dropbox < 60 } count playableUnits >0 ) then { 
        if !(isNil "_lightLand") then { deleteVehicle _lightLand }; if !(isNil "_smokeLand") then { deleteVehicle _smokeLand }; breakto "findLandScope"; 
    }; 
}; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_findBoxLand complete"]; }; 
