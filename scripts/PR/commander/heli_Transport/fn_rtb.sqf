/* fn_rtb.sqf
*  Author: PapaReap
*  function name: pr_fnc_rtb

*  Arguments
*  0: VEHICLE
*  1: GROUP
*  2: POSITION
*  [_heli, _heliGroup, _startPos] spawn pr_fnc_rtb;
*/

_heli = _this select 0;
_heliGroup = _this select 1;
_startPos = _this select 2;

_wpStatement = "_heli = vehicle this; { _heli animateDoor [_x, 1] } forEach ['door_back_L','door_back_R','door_L','door_R'];";
_wpStatement2 = "_group = group this; _units = crew _heli;";
_wpStatement3 = "_heli land 'LAND';";
_wpStatement4 = "{ deleteVehicle _x } forEach _units; deleteVehicle _heli; deleteGroup _group;";

_behaviour = ["CARELESS", "BLUE", "FULL", "FILE"];

if (pr_rtb_order) then {
    pr_rtb_order = false; publicVariable "pr_rtb_order";
    _requestRTBHint = format ["Angel Wing, Return to Base"];
    logic_Leader sideChat _requestRTBHint;
    logic_Leader_SideChat = _requestRTBHint; publicVariable "logic_Leader_SideChat";
    sleep 10;
    _RTBHint = format ["Copy, Angle Wing is RTB"];
    logic_pigeon sideChat _RTBHint;
    logic_pigeon_SideChat = _RTBHint; publicVariable "logic_pigeon_SideChat";
};

// RTB WP
[_heliGroup, _startPos, ["MOVE",0,50], _behaviour, _wpStatement + _wpStatement2 + _wpStatement3 + "heliTransportEnd = true; publicVariable 'heliTransportEnd';"] call pr_fnc_addWaypoint;

waitUntil { sleep 10; ((heliTransportEnd) || (!heli_transport)) };

if ((!heli_transport) && ((isNull _heli) || (!canMove _heli))) then {
    if (!isNull _heli) then { deleteVehicle _heli; _heli = objNull };
    { noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap" } forEach units _heliGroup;
    if (!isNull _heliGroup) then { { deleteVehicle _x } forEach units _heliGroup; deleteGroup _heliGroup };
    heliTransportDestroyed = true; publicVariable "heliTransportDestroyed";
} else {
    if ((!heli_transport) && (!isNull _heli)) then {
        waitUntil { sleep 10; ((heliTransportEnd) || ((isNull _heli) || (!canMove _heli))) };
        if (heliTransportEnd) then {
            if (!isNull _heli) then {
                sleep 120;
                deleteVehicle _heli; _heli = objNull;
                { noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap" } forEach units _heliGroup;
                if (!isNull _heliGroup) then { { deleteVehicle _x } forEach units _heliGroup; deleteGroup _heliGroup };
                heli_transport = false; publicVariable "heli_transport";
                heliTransportCleanup = true; publicVariable "heliTransportCleanup";
                heliTransportEnd = false; publicVariable "heliTransportEnd";
				startHeliTimer = false; publicVariable "startHeliTimer";
            };
        } else {
            if ((isNull _heli) || (!canMove _heli)) then {
                if (!isNull _heli) then { deleteVehicle _heli; _heli = objNull };
                { noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap" } forEach units _heliGroup;
                if (!isNull _heliGroup) then { { deleteVehicle _x } forEach units _heliGroup; deleteGroup _heliGroup };
                heliTransportDestroyed = true; publicVariable "heliTransportDestroyed";
            };
        };
    } else {
        if (heliTransportEnd) then {
            if (!isNull _heli) then {
                sleep 120;
                deleteVehicle _heli; _heli = objNull;
                { noHCSwap = noHCSwap - [_x]; } forEach units _heliGroup; publicVariable "noHCSwap";
                if (!isNull _heliGroup) then { { deleteVehicle _x } forEach units _heliGroup; deleteGroup _heliGroup };
                heli_transport = false; publicVariable "heli_transport";
                heliTransportCleanup = true; publicVariable "heliTransportCleanup";
                heliTransportEnd = false; publicVariable "heliTransportEnd";
				startHeliTimer = false; publicVariable "startHeliTimer";
            };
        };
    };
};
