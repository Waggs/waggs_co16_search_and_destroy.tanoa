
waitUntil { !isNull (findDisplay 1801) && { dialog } };

_display = 1823;
_string = ["10","20","50","100","200","500"];
_arr = [0,1,2,3,4,5];
_choice = -1;
{
    _index = lbAdd [_display, _x];
} forEach _string;

while { dialog } do {

    waitUntil { lbCurSel _display != -1 };
    _count = lbCurSel _display;

    if (_count == 0) then {
        _choice = _arr select 0;
    } else {
        if (_count == 1) then {
            _choice = _arr select 1;
        } else {
            if (_count == 2) then {
                _choice = _arr select 2;
            } else {
                if (_count == 3) then {
                    _choice = _arr select 3;
                } else {
                    if (_count == 4) then {
                        _choice = _arr select 4;
                    } else {
                        if (_count == 5) then {
                            _choice = _arr select 5;
                        };
                    };
                };
            };
        };
    };

    missionNamespace setVariable ["setFlyHeightType", _choice];
    missionNamespace setVariable ["flyHeightSelected", 1];
};

missionNamespace setVariable ["flyHeightSelected", 0];