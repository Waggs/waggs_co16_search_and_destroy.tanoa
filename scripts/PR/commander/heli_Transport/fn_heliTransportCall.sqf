/* fn_heliTransportCall.sqf
*  Author: PapaReap
*
*  ver 1.1 - 2016-01-07 code cleanup and made into function
*  ver 1.0 - 2015-03-11
*/

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_heliTransportCall start"]; };

if (isDedicated) exitWith {};
waitUntil { !isNull Player };
if !((p1) == (player)) exitWith {};

// set vars
if (isNil "startHeliTimer") then { startHeliTimer = false; publicVariable "startHeliTimer"; };
if (isNil "heli_transport") then { heli_transport = false; publicVariable "heli_transport"; };
if (isNil "pr_transportAlive") then { pr_transportAlive = false; publicVariable "pr_transportAlive"; };
if (isNil "heliTransportDestroyed") then { heliTransportDestroyed = false; publicVariable "heliTransportDestroyed"; };
if (isNil "heliTransportCleanup") then { heliTransportCleanup = false; publicVariable "heliTransportCleanup"; };
if (isNil "heliTransportEnd") then { heliTransportEnd = false; publicVariable "heliTransportEnd"; };
if (isNil "new_Heli_transport") then { new_Heli_transport = true; publicVariable "new_Heli_transport"; };
if (isNil "pr_rtb_order") then { pr_rtb_order = false; publicVariable "pr_rtb_order"; };
if (isNil "pr_liftoff_order") then { pr_liftoff_order = false; publicVariable "pr_liftoff_order"; };
if (isNil "transportMarkers") then { transportMarkers = []; publicVariable "transportMarkers"; };
if (isNil "pr_landPadArray") then { pr_landPadArray = []; publicVariable "pr_landPadArray"; };
if (isNil "pr_acceptRouteChanges") then { pr_acceptRouteChanges = false; publicVariable "pr_acceptRouteChanges"; };
if (isNil "pr_disAbleFuel") then { pr_disAbleFuel = false; publicVariable "pr_disAbleFuel"; };

missionNamespace setVariable ["quickMove", false];
private _quickMove = missionNamespace getVariable "quickMove";
if (isNil "_quickMove") then {
    missionNamespace setVariable ["quickMove", false];
};


if (isNil "TAll") then { TAll = false; publicVariable "TAll"; };
if (isNil "TAlpha") then { TAlpha = false; publicVariable "TAlpha"; };
if (isNil "TBravo") then { TBravo = false; publicVariable "TBravo"; };
if (isNil "TCharlie") then { TCharlie = false; publicVariable "TCharlie"; };
if (isNil "TDelta") then { TDelta = false; publicVariable "TDelta"; };

_teamType = missionNamespace getVariable "setTeam";
if (_teamType == 0) then {
    TAll = true; publicVariable "TAll";
} else {
    if (_teamType == 1) then {
        TAlpha = true; publicVariable "TAlpha";
    } else {
        if (_teamType == 2) then {
            TBravo = true; publicVariable "TBravo";
        } else {
            if (_teamType == 3) then {
                TCharlie = true; publicVariable "TCharlie";
            } else {
                if (_teamType == 4) then {
                    TDelta = true; publicVariable "TDelta";
                };
            };
        };
    };
};

_transportType = missionNamespace getVariable "setTransport";
_heliType = "";
if (_transportType == 0) then {
    _heliType = b_littleBird;
} else {
    if (_transportType == 1) then {
        _heliType = b_hueyArmed;
    } else {
        if (_transportType == 2) then {
            _heliType = b_blackhawk;
        } else {
            if (_transportType == 3) then {
                _heliType = b_mohawk;
            } else {
                if (_transportType == 4) then {
                    _heliType = b_chinookArmed;
                };
            };
        };
    };
};

_transportAction = missionNamespace getVariable "setAction";
_actionType = "";
if (_transportAction == 0) then {
    _actionType = "Land";
} else {
    if (_transportAction == 1) then {
        _actionType = "FastRope";
    } else {
        if (_transportAction == 2) then {
            _actionType = "Paradrop";
        } else {
            if (_transportAction == 3) then {
                _actionType = "Rooftop";
            };
        };
    };
};


missionNamespace setVariable ["setTeam", -1];
missionNamespace setVariable ["setTransport", -1];
missionNamespace setVariable ["setAction", -1];

if ((heli_transport) || (!new_Heli_transport)) then {
    TAll = false; publicVariable "TAll";
    TAlpha = false; publicVariable "TAlpha";
    TBravo = false; publicVariable "TBravo";
    TCharlie = false; publicVariable "TCharlie";
    TDelta = false; publicVariable "TDelta";

    if (heli_transport) then {
        _sleep = ["CDWait",600] call BIS_fnc_getParamValue;
        _name = name player;
        timeNowTransport = floor(serverTime); publicVariable "timeNowTransport";
        placeTimeLeftTransport = _sleep - (timeNowTransport - placeTimeTransport); publicVariable "placeTimeLeftTransport";
        _min = floor(placeTimeLeftTransport/60);
        _sec = floor(placeTimeLeftTransport)- (60 * _min);
        _secHand = "";
        if ((floor(placeTimeLeftTransport/60)) < 1) then { _secHand = 'sec' } else { _secHand = 'min' };
        hint parseText format ["%1<br/><t color='#ff5d00'>No Transports Available<br/>Wait %2m %3s for Heli to RTB</t>",_name,_min,_sec,_secHand];
    } else {
        if (!new_Heli_transport) then {
            _name = name player;
            hint parseText format ["%1<br/><t color='#ff5d00'>No Transports Available<br/>Heli Status Unknown, Check Later</t>",_name];
        };
    };
    onMapSingleClick "";  //don't think need
    sleep 15;
    hintSilent "";
    if ((heli_transport) || (!new_Heli_transport)) exitWith {};
};

if ((!heli_transport) && (new_Heli_transport)) then {
    missionNameSpace setVariable ["pickUpMarker", "PickUp"];
    _pickUpMarker = missionNameSpace getVariable "pickUpMarker";

    pickUpClick = false; publicVariable "pickUpClick"; pickUpClick2 = false; publicVariable "pickUpClick2";
    openMap [true, false];
    ["<t size='0.7' color='#00E0FD'>" + "Commander<br/>Place pick up location on map<br/>(click location)<br/><br/>" + "</t>" + "<t size='0.7' color='#ff9900'>exit map to cancel" + "</t>",0,0.8,8,1] spawn BIS_fnc_dynamicText;
    if (isNil "TrID") then { TrID = 1 };
    if (isNil "TrID_new") then { TrID_new = 0 };
    TrID = TrID + TrID_new;

    ["TrID","onMapSingleClick", {
        _pickUpMarker = missionNameSpace getVariable "pickUpMarker";
        click = _pos;
        pickUpClick = true; publicVariable "pickUpClick";

        _pickup = "";
        if (getMarkerColor (missionNameSpace getVariable "pickUpMarker") == "") then {
            _pickup = createMarker [_pickUpMarker, click];
            _pickup setMarkerType "hd_pickup";
            _pickup setMarkerColor "ColorBlue";
            _pickup setMarkerText _pickUpMarker;
            missionNameSpace setVariable ["pickUpMarker", _pickup];
			transportMarkers = transportMarkers + [_pickup]; publicVariable "transportMarkers";
        } else {
           _pickup setMarkerPos click;
        };

        ["<t size='0.7' color='#00E0FD'>" + "Commander<br/>Place drop off location on map<br/>(click location)<br/><br/>" + "</t>" + "<t size='0.7' color='#ff9900'>exit map to cancel" + "</t>",0,0.8,8,1] spawn BIS_fnc_dynamicText;
        onMapSingleClick "
            click = _pos;
            if !(getMarkerColor 'dropOff' == 'ColorBlue') then {
                _extract = createMarker ['dropOff', click];
                _extract setMarkerType 'hd_end';
                _extract setMarkerColor 'ColorBlue';
                _extract setMarkerText 'drop off';
                transportMarkers = transportMarkers + [_extract]; publicVariable 'transportMarkers';
            } else {
                'dropOff' setMarkerPos click;
            };
            onMapSingleClick """";
            pickUpClick2 = true; publicVariable ""pickUpClick2"";
        ";
    }] call BIS_fnc_addStackedEventHandler;

    sleep .01;
    waitUntil { (!visibleMap) || ((pickUpClick) && (pickUpClick2)) };

    if (pickUpClick2) then {
        ["<t size='0.7' color='#00E0FD'>" + "Commander has called for transport" + "</t>",0,0.8,4,1] spawn BIS_fnc_dynamicText;
        heli_transport = true; publicVariable "heli_transport";
        new_Heli_transport = false; publicVariable "new_Heli_transport";
        pickMark = "Land_PenBlack_F" createVehicle getMarkerPos "pickUp";
        pickMark setPos getMarkerPos "pickUp"; publicVariable "pickMark";
        waitUntil { (!isNull pickMark) };
        //pickMarkGridPos = mapGridPosition pickMark;
        _GridPos = mapGridPosition pickMark;
        true;
        if (TAll) then {
            [group player, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, _heliType, _actionType, grpAll] remoteExec ["pr_fnc_heliTransport", 2];
            TAll = false; publicVariable "TAll";
            _requestTransportHint = format ["Base Firefly, Requesting Heli Transport for All Units on Grid Location %1", _GridPos];
            logic_Leader sideChat _requestTransportHint;
            logic_Leader_SideChat = _requestTransportHint; publicVariable "logic_Leader_SideChat";
        } else {
            if (TAlpha) then {
                [PG1, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, _heliType, _actionType] remoteExec ["pr_fnc_heliTransport", 2];
                TAlpha = false; publicVariable "TAlpha";
                _requestTransportHint = format ["Base Firefly, Requesting Heli Transport for Team Alpha on Grid Location %1", _GridPos];
                logic_Leader sideChat _requestTransportHint;
                logic_Leader_SideChat = _requestTransportHint; publicVariable "logic_Leader_SideChat";
            } else {
                if (TBravo) then {
                    [PG2, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, _heliType, _actionType] remoteExec ["pr_fnc_heliTransport", 2];
                    TBravo = false; publicVariable "TBravo";
                    _requestTransportHint = format ["Base Firefly, Requesting Heli Transport Team Bravo on Grid Location %1", _GridPos];
                    logic_Leader sideChat _requestTransportHint;
                    logic_Leader_SideChat = _requestTransportHint; publicVariable "logic_Leader_SideChat";
                } else {
                    if (TCharlie) then {
                        [PG3, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, _heliType, _actionType] remoteExec ["pr_fnc_heliTransport", 2];
                        TCharlie = false; publicVariable "TCharlie";
                        _requestTransportHint = format ["Base Firefly, Requesting Heli Transport Team Charlie on Grid Location %1", _GridPos];
                        logic_Leader sideChat _requestTransportHint;
                        logic_Leader_SideChat = _requestTransportHint; publicVariable "logic_Leader_SideChat";
                    } else {
                        if (TDelta) then {
                            [PG4, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, _heliType, _actionType] remoteExec ["pr_fnc_heliTransport", 2];
                            TDelta = false; publicVariable "TDelta";
                            _requestTransportHint = format ["Base Firefly, Requesting Heli Transport Team Delta on Grid Location %1", _GridPos];
                            logic_Leader sideChat _requestTransportHint;
                            logic_Leader_SideChat = _requestTransportHint; publicVariable "logic_Leader_SideChat";
                        };
                    };
                };
            };
        };
        startHeliTimer = true; publicVariable "startHeliTimer";
    } else {
        if ((!pickUpClick)||(!pickUpClick2)) exitWith {
            titleText ["","PLAIN",0.5];
            click = nil;
            onMapSingleClick "";
            ["<t size='0.7' color='#ff9900'>" + "Pick up request cancelled" + "</t>",0,0.8,4,1] spawn BIS_fnc_dynamicText;
            if (pickUpClick) then {
                deleteMarker "pickUp";
            };
        };
    };

    if (startHeliTimer) then {
        _sleep = ["CDWait",1200] call BIS_fnc_getParamValue;
        sleep .01;
        placeTimeTransport = floor(serverTime); publicVariable "placeTimeTransport";
        TrID_new = (TrID + TrID_new) - 1;
        ["TrID", "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler;
        _gridPos = mapGridPosition pickMark;
        sleep 15;
        hintSilent "";

        waitUntil { ((!heli_transport) || (_sleep - (floor(serverTime) - placeTimeTransport) < 1) || !(startHeliTimer)) };

        deleteVehicle pickMark;
        if !(startHeliTimer) exitWith {};

        startHeliTimer = false; publicVariable "startHeliTimer";
        if (heli_transport) then { heli_transport = false; publicVariable "heli_transport" };
        waitUntil { sleep 10; ((heliTransportCleanup) || (heliTransportDestroyed)) };

        if (heliTransportCleanup) then {
            _transportAvailableHint = format ["Commander, Transport Available and Standing By"];
            logic_firefly sideChat _transportAvailableHint;
            logic_firefly_SideChat = _transportAvailableHint; publicVariable "logic_firefly_SideChat";
            heliTransportCleanup = false; publicVariable "heliTransportCleanup";
        } else {
            if (heliTransportDestroyed) then {
                _transportDownHint = format ["Commander, Transport is Down, Wait Until Next Available Transport"];
                logic_firefly sideChat _transportDownHint;
                logic_firefly_SideChat = _transportDownHint; publicVariable "logic_firefly_SideChat";
                sleep 180;

                _newTransportAvailableHint = format ["Commander, New Transport Available and Standing By"];
                logic_firefly sideChat _newTransportAvailableHint;
                logic_firefly_SideChat = _newTransportAvailableHint; publicVariable "logic_firefly_SideChat";
                heliTransportDestroyed = false; publicVariable "heliTransportDestroyed";
            };
        };
        if (getMarkerColor "pickUp" == "ColorBlue") then { deleteMarker "pickUp"};
        if (getMarkerColor "dropOff" == "ColorBlue") then { deleteMarker "dropOff"};
        new_Heli_transport = true; publicVariable "new_Heli_transport";
    };
};

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_heliTransportCall complete"]; };
