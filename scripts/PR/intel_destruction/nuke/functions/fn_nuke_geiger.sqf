private ["_posx","_posy","_blast_time","_radius","_all_radius","_current_radius","_v","_distance","_radiation","_sleep","_count","_random"]; 

_bomb = _this select 0; 
_bArr = _bomb getVariable "nukeArray"; 
_radius    = _bArr select 1; 
_irradTime = _bArr select 5; 
_reqGPS    = _bArr select 6; 
_carArmor  = _bArr select 9; 
_radFree   = _bArr select 11; 

_pos = getpos _bomb; 
_posx = _pos select 0; 
_posy = _pos select 1; 
_posz = _pos select 2; 

_blast_time = _this select 1; 

_all_radius = _radius * 1.3; 
_radius2 = _all_radius; 
_v = _radius2 / (2 * _irradTime); 

_radiation = 0; 

while { _radius2 > 1 } do { 
    _current_radius = _radius2 - (time - _blast_time) * _v; 
    _distance = [_posx, _posy] distance player; 
    _sleep = 1; 
    _safeZone = objNull; 
    _disToSafe = 0; 


    { 
        if ((_x isKindOf "Man") || count (crew _x) > 0) then { 
            if (_distance < _current_radius) then { 
                _radiation = (1 - _distance / _current_radius) * _current_radius / _all_radius; 
                if (_x isKindOf "Man") then { 
                    _man = _x; 
                    { 
                        _safeZone = _x; 
                        if ((_safeZone isKindOf "House") || (_safeZone isKindOf "Building")) then { 
                            _disToSafe = _man distance _safeZone; 
                            if ((_disToSafe < 10) && (_man call pr_fnc_inHouse) && !(_safeZone animationPhase "Door_1_rot" > 0) && !(_safeZone animationPhase "Door_2_rot" > 0)) then { 
                                while { ((_disToSafe < 10) && (_man call pr_fnc_inHouse) && !(_safeZone animationPhase "Door_1_rot" > 0) && !(_safeZone animationPhase "Door_2_rot" > 0)) } do { sleep 1; }; 
                            }; 
                        }; 
                    } forEach _radFree; 
                } else { 
                    {
                        _safeZone = _x; 
                        _crew = crew _safeZone; 
                        {
                            if (vehicle _x == _safeZone) then { 
                                while { ((vehicle _x == _safeZone) && !((_safeZone animationPhase (getText([_safeZone, [1]] call pr_fnc_getTurret >> "animationSourceHatch"))) == 1)) } do { 
                                    sleep 1; 
                                }; 
                            }; 
                        } forEach _crew; 
                    } forEach _radFree; 
                }; 
            }; 
        }; 
    } forEach ([_posx, _posy, 0] nearObjects ["All", _current_radius]); 

    if (vehicle player != player) then { 
        _radiation = _carArmor * _radiation; 
    }; 
    _sleep = 4; 
    _count = _radiation * 10; 

    for "_x" from 0 to _count do { 
        if (_reqGPS) then { 
            if ("ItemGPS" in (items player + assignedItems player)) then { 
                playSound "nuclear_geiger"; 
            }; 
        } else { 
            playSound "nuclear_geiger"; 
        }; 
        _random = (4 * random 1) / 10; 
        _sleep = _sleep - _random; 
        sleep _random; 
    }; 

    sleep _sleep; 
    if ((time - _blast_time) > _irradTime) then { 
        _radius2 = _radius2 / 2; 
        _blast_time = _blast_time + _irradTime; 
        _v = _radius2 / (2 * _irradTime); 
    }; 
}; 
