
pr_shockWave = { 
    private ["_magnitude", "_power", "_duration", "_frequency", "_offset", "_compensation", "_eqsound", "_fatiguedefault", "_fatigue"];
    if( isNil "earthquake_inprogress" ) then { earthquake_inprogress = false; };

    waitUntil{ !earthquake_inprogress };
    _fatiguedefault = getFatigue player;
    _power = 2.1;
    _duration = 20;
    _compensation = 6;
    _frequency = 30; 
    _offset = 1; 
    _eqsound1 = "Earthquake_01"; 
    _eqsound4 = "Earthquake_04"; 
    _fatigue = 1; 

    enableCamShake true; 
    earthquake_inprogress = true; 
    playsound _eqsound4; // playsound "Earthquake_04"
    "DynamicBlur" ppEffectEnable true;
    "DynamicBlur" ppEffectAdjust [_power/2];
    "DynamicBlur" ppEffectCommit _offset;
    sleep _offset;
    "DynamicBlur" ppEffectAdjust [0];
    "DynamicBlur" ppEffectCommit (_duration - _compensation);
    player setFatigue _fatigue;
    addCamShake [_power, _duration, _frequency]; // addCamShake [2.1, 20, 30]; 
    _t = time;
    sleep ( _duration - _compensation ); 

    player setFatigue _fatiguedefault; 
    sleep 3; 
    earthquake_inprogress = false; 
}; 

_bomb = _this select 0; 
_pos = getpos _bomb; 
_yield = _this select 1; 
_radius = _this select 2; 
_posx = _pos select 0; 
_posy = _pos select 1; 
_timefactor = 0.03*_yield^0.5; 
_effectSize = 0.1*_yield^0.3; 
_lifetime = 3*_timefactor; 
for [{_agl=0},{_agl<360},{_agl=_agl+1}] do { 
    _velx = sin(_agl)*400.0; 
    _vely = cos(_agl)*400.0; 
    _velz = 50; 
    drop ["\A3\data_f\cl_basic","","Billboard",1,_lifetime,[_posx,_posy, -10],[_velx,_vely,_velz],1,1.25,1.0,0.0,[50*_effectSize,300*_effectSize,750*_effectSize,1500*_effectSize],[[1.0,1.0,1.0,0.0],[1.0,1.0,1.0,0.2],[1.0,1.0,1.0,0.1],[1.0,1.0,1.0,0.05],[1.0,1.0,1.0,0.025],[1.0,1.0,1.0,0.012],[1.0,1.0,1.0,0.0]],[0],0.0,2.0,"","",""]; 
}; 

//_nil = [] spawn pr_shockWave; 
//[4] call BIS_fnc_earthquake; 
