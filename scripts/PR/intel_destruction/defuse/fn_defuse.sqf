/* 
_null = [bomb, timeToDetonate] spawn pr_fnc_bombTimer; 
trigger: _null = [satPhone, 90] spawn pr_fnc_bombTimer; 

Bomb 
name: satPhone 
Init: this enableSimulation false; this addAction [("<t color='#E61616'>" + ("Defuse the Bomb") + "</t>"),"scripts\PR\scripts\misc\intel_destruction\defuse\defuseAction.sqf","",1,true,true,"","(_target distance _this) < 5"]; 

codeHolder: Laptop or other item to get codes 
name: laptop1
*/ 


CODEINPUT = [];
DEFUSED = false;
ARMED = false;
WIRE = [];
CODE = [];
pr_wireArray = ["BLUE", "WHITE", "YELLOW", "GREEN"];

if (isNil "pr_codeHolderCnt") then { pr_codeHolderCnt = 0; };

pr_fnc_searchAction = {
    _codeHolder    = _this select 0;
    _codeHolderArr = _codeHolder getVariable "holderCode";
    _codes         = _codeHolderArr select 0;
    _wires         = _codeHolderArr select 1;
    _order         = _codeHolderArr select 2;
    _data          = _codeHolderArr select 3;
    _condition     = _codeHolderArr select 4;

    if (_data) then {
        waitUntil { (missionNamespace getVariable _condition) };
    } else {
        _caller = _this select 1;
        _id     = _this select 2;
        _codeHolder removeAction _id;
        _caller playMove "AmovPercMstpSrasWrflDnon_AinvPercMstpSrasWrflDnon_Putdown";
    };

    cutText [format ["Code: %1\n Wire: %2, this is part %3 of %4", _codes, _wires, _order, pr_codeHolderCnt], "PLAIN DOWN"];
};

pr_fnc_codeBreifing = {
    _codeHolder = _this select 0;
    _codeHolderArr = _codeHolder getVariable "holderCode";
    _codes = _codeHolderArr select 0;
    _wires = _codeHolderArr select 1;
    _order = _codeHolderArr select 2;
    _condition = _codeHolderArr select 4;
    _wires = format ["%1",_wires];

    _diaryText = "";
    _diaryText = format ["Code: %1 Wire: %2, this is part %3 of %4", _codes, _wires, _order, pr_codeHolderCnt]; pr_diaryText=_diaryText;

    waitUntil { (missionNamespace getVariable _condition) };
    [west, ["Diary", "DEFUSAL CODES", _diaryText]] call FHQ_fnc_ttaddBriefing;
};

pr_fnc_defuse = {
    _defuser = _this select 0;
    if !(_defuser == player) exitWith {};
    createDialog "KeypadDefuse";
};

pr_fnc_defuseAction = {
    _defuseDevice = _this select 0;
    _requireSpecialist = _this select 1;
    _condition = "(_target distance _this) < 2";
    if (_requireSpecialist) then {
        _condition = _condition + " && ((getText (configFile >> 'CfgVehicles' >> typeOf _this >> 'Displayname') == 'Explosive Specialist') || (getNumber(configFile >> 'CfgVehicles' >> typeOf _this >> 'engineer') == 1))";
    };
    _defuseDevice addAction [
        ("<t color='#E61616'>" + ("Defuse the Bomb") + "</t>"),
        { [[_this select 1], "pr_fnc_defuse", true, false] call BIS_fnc_MP; },
        "",
        30,
        true,
        true,
        "",
        _condition
    ];
    buildDefuse = true;
};

pr_fnc_code = {
    private ["_codeCnt","_wireCnt","_randomCount","_codes","_wires","_data"];
    _order = 0;
    _codeCnt = 0;
    _wireCnt = 0;
    _randomCount = [];
    _codes = [];
    _wires = [];
    _data = false;
    pr_codeHolderCnt = pr_codeHolderCnt + 1; publicVariable "pr_codeHolderCnt";

    _codeHolder = _this select 0;
    if (count _codeHolder > 0) then {
        _codeHolderArr = _this select 0;
        _codeHolder = _codeHolderArr select 0;
        _order = _codeHolderArr select 1;
    };

    _code = _this select 1;
    if (count _code > 0) then {
        _codeArr = _this select 1;
        _code = _codeArr select 0;
        _codeCnt = _codeArr select 1;
    };

    _wire = _this select 2;
    if (count _wire > 0) then {
        _wireArr = _this select 2;
        _wire = _wireArr select 0;
        _wireCnt = _wireArr select 1;
    };

    if (_codeCnt > 0) then {
        _count = _codeCnt - 1;
        for "_i" from 0 to _count do {
            _randomCount = [(round(random 9))];
            _codes = _codes + _randomCount;
            CODE = CODE + _randomCount;
        };
    } else {
        _randomCount = (round(random 9));
        _codes = _codes + _randomCount;
        CODE = CODE + _randomCount;
    };

    publicVariable "CODE";

    if !(count pr_wireArray == 0) then {
        if (_wireCnt > 0) then {
            _count = _wireCnt - 1;
            for "_i" from 0 to _count do {
                if !(count pr_wireArray == 0) then {
                    _wire = pr_wireArray call bis_fnc_selectRandom;
                    _wires = _wires + [_wire];
                    pr_wireArray = pr_wireArray - [_wire];
                    WIRE = WIRE + [_wire];
                };
            };
        } else {
            if (count WIRE <= 4) then {
            _wire = pr_wireArray call bis_fnc_selectRandom;
                _wires = _wires + [_wire];
                pr_wireArray = pr_wireArray - [_wire];
                WIRE = WIRE + [_wire];
            };
        };
    };

    publicVariable "pr_wireArray";
    publicVariable "WIRE";

    _type = "";
    if (count _this > 3) then {
        _type = _this select 3;
    };

    _condition = false;
    if (_type == "DATA") then {
        waitUntil { !isNil "compiledDownload" };
        _data = true;
        _condition = format [ "%1_%2", "T8_pubVarDataTask", _codeHolder ];
        _varExists = missionNamespace getVariable _condition;
        if (isNil "_varExists") then {
            missionNamespace setVariable [_condition, false];
        };
        _codeHolder setVariable ["holderCode", [_codes, _wires, _order, _data, _condition], true];
        [[_codeHolder], "pr_fnc_searchAction", true, false] call BIS_fnc_MP;
    } else {
        _codeHolder setVariable ["holderCode", [_codes, _wires, _order, _data, _condition], true];
        _codeHolder addAction [
            ("<t color='#E61616'>" + ("The Code") + "</t>"),
            { [[_this select 0, _this select 1, _this select 2], "pr_fnc_searchAction", true, false] call BIS_fnc_MP; },
            "",
            1,
            false,
            true,
            "",
            "(_target distance _this) < 2"
        ];
    };

    buildCodes = true;
};

/*
[] spawn {
    waitUntil { DEFUSED };
    ["Task_Defuse", "Succeeded"] call BIS_fnc_taskSetState;
    sleep 2;
    ["end1", true] call BIS_fnc_endMission;
};

[] spawn {
    waitUntil { ARMED };
    ["Task_Defuse", "Failed"] call BIS_fnc_taskSetState;
    sleep 10;
    ["LOSER", false] call BIS_fnc_endMission;
};
*/
defusedInitCompiled = true;
