class button_click { 
    name = ""; 
    sound[] = {"scripts\PR\intel_destruction\defuse\sounds\click.ogg", 0.8, 1}; 
    titles[] = {}; 
}; 

class button_close { 
    name = ""; 
    sound[] = {"scripts\PR\intel_destruction\defuse\sounds\close.ogg", 0.8, 1}; 
    titles[] = {}; 
}; 

class button_wrong { 
    name = ""; 
    sound[] = {"scripts\PR\intel_destruction\defuse\sounds\wrong.ogg", 0.8, 1}; 
    titles[] = {}; 
}; 

class wire_cut { 
    name = ""; 
    sound[] = {"scripts\PR\intel_destruction\defuse\sounds\cutting.ogg", 1, 1}; 
    titles[] = {}; 
}; 
