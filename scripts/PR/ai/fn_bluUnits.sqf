/* fn_redUnits.sqf
    usage: [b_fireTeam, "spawnMrk", "area1", "CO"] call PR_fnc_unitSpawn;
*/
diag_log format ["*PR* fn_bluUnits start"];
waitUntil { !(isNil "objectVarsCompiled") };


//--- Standard Arma 3 Weapons & Uniforms - Default
//-- Blufor Men
_sl = "B_Soldier_SL_F";
_tl = "B_Soldier_TL_F";
_at = "B_Soldier_LAT_F";
_atS = "B_soldier_AT_F";
_atA = "B_soldier_AAT_F";
_ri = "B_Soldier_F";
_mk = "B_soldier_M_F";
_med = "B_medic_F";
_mg = "B_Soldier_AR_F";
_mgA = "B_Soldier_AAR_F";
_gr = "B_Soldier_GL_F";
_aa = "B_soldier_AA_F";
_eng = "B_engineer_F";
_of = "B_officer_F";
_snp = "B_sniper_F";
_spt = "B_spotter_F";
_pP = "B_Pilot_F";
_hP = "B_helipilot_F";
_hC = "B_helicrew_F";
_bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

//-- Blufor Vehicles
// car
_qd = "B_Quadbike_01_F"; // quad
_jp = "B_LSV_01_unarmed_F"; // jeep
_jpA = "B_LSV_01_armed_F"; // jeep armed
_hmv = "B_MRAP_01_F"; // mrap
_hmvH = "B_MRAP_01_hmg_F"; // mrap HMG
_hmvG = "B_MRAP_01_gmg_F"; // mrap GMG
_hmvM = _hmv; // mrap Med
natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

// support
_sup = "B_Truck_01_mover_F"; // engine
_supA = "B_Truck_01_ammo_F"; // ammo
_supB = "B_Truck_01_box_F"; // box
_supF = "B_Truck_01_fuel_F"; // fuel
_supM = "B_Truck_01_medical_F"; // medical
_supR = "B_Truck_01_Repair_F"; // repair
_supT = "B_Truck_01_transport_F"; // transport
_supTC = "B_Truck_01_covered_F"; // transport covered
natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

// apc
_apcW = "B_APC_Wheeled_01_cannon_F"; //wheeled
_apcT = "B_APC_Tracked_01_rcws_F"; //tracked
_apcT2 = "B_APC_Tracked_01_CRV_F"; //tractor
natoAPC = [_apcW,_apcT,_apcT2];

// anti-air
_armAA = "B_APC_Tracked_01_AA_F"; //cheetah
natoAA = [_armAA];

// tank
_tank1 = "BWA3_Leopard2A6M_Fleck"; //slammer/fleck
_tank2 = "B_MBT_01_cannon_F"; //slammer/mbt
_tank3 = "B_MBT_01_TUSK_F"; //slammer/tusk
natoTank = [_tank1,_tank2,_tank3];

// artillary
_art1 = "B_MBT_01_arty_F"; // Artillary
_art2 = "B_MBT_01_mlrs_F"; // MLRS
natoArty = [_art1,_art2];

// air
_lb = "B_Heli_Light_01_F"; // little bird
_lbA = "B_Heli_Light_01_armed_F"; // little bird armed
_lbM = _lb; // little bird med
_hu = "B_Heli_Transport_01_F"; // huey
_huA = _hu; // huey armed
_huM = _hu; // huey med
_bh = _hu; // blackhawk
_bhC = "B_Heli_Transport_01_camo_F"; // blackhawk cammo
_bhM = _hu; // blackhawk med
_mh = "I_Heli_Transport_02_F"; // mohawk
_ch = "B_Heli_Transport_03_F"; // chinook
_chA = "B_Heli_Transport_03_unarmed_F"; // chinook armed
_ah = "B_Heli_Attack_01_F"; // attack heli
_pCas = "B_Plane_CAS_01_F"; // plane CAS
natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

// sea
_ab = "B_Boat_Transport_01_F"; // assault boat
_rb = "B_Lifeboat"; // rescue boat
_gb = "B_Boat_Armed_01_minigun_F"; // gun boat
_sub = "B_SDV_01_F"; // submarine
natoSea = [_ab,_rb,_gb,_sub];

// static
_stH = "B_HMG_01_high_F"; // HMG
_stG = "B_GMG_01_high_F"; // GMG
_stM = "B_Mortar_01_F"; // mortar
_stAT = "B_static_AT_F"; // AT
_stAA = "B_static_AA_F"; // AA
natoStatic = [_stH,_stG,_stM,_stAT,_stAA];

if ((prWeapons == 2) || (prWeapons == 3) || (prWeapons == 4)) then {  
} else {
    //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
    if ((prWeapons == 5) || (prWeapons == 6) && (masOn)) then {  // Massi SF Weapons / UK Uniforms & UK Winter Uniforms
        //-- Blufor Men
        _sl = "";
        _tl = "";
        _at = "";
        _atS = "";
        _atA = "";
        _ri = "";
        _mk = "";
        _med = "";
        _mg = "";
        _mgA = "";
        _gr = "";
        _aa = "";
        _eng = "";
        _of = "";
        _snp = "";
        _spt = "";
        _pP = "";
        _hP = "";
        _hC = "";
        _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

        //-- Blufor Vehicles
        // car
        _qd = "B_mas_uk_Quadbike_01_F"; // quad
        _jp = "B_mas_uk_SUV_01_F"; // jeep/svu
        _jpA = "B_mas_uk_MRAP_01_hmg_F"; // jeep armed
        _hmv = "B_mas_uk_MRAP_01_F"; // mrap
        _hmvH = "B_mas_uk_MRAP_01_hmg_F"; // mrap HMG
        _hmvG = "B_mas_uk_MRAP_01_gmg_F"; // mrap GMG
        _hmvM = "B_mas_uk_MRAP_01_med_F"; // mrap Med
        natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

        // support
        //_sup = ""; // engine
        _supA = "B_mas_uk_Truck_02_reammo_F"; // ammo
        _supB = "B_mas_uk_Truck_02_covered_F"; // box
        _supF = "B_mas_uk_Truck_02_refuel_F"; // fuel
        _supM = _hmvM; // medical
        _supR = "B_mas_uk_Truck_02_repair_F"; // repair
        _supT = "B_mas_uk_Truck_02_transport_F"; // transport
        _supTC = _supB; // transport covered
        natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

        // apc
        _apcW = "B_mas_uk_APC_Wheeled_01_cannon_F"; //wheeled
        _apcT = "B_mas_uk_APC_tracked_03_cannon_F"; //tracked
        //_apcT2 = _apcT; //tractor
        natoAPC = [_apcW,_apcT,_apcT2];

        // anti-air
        _armAA = "B_mas_uk_APC_Tracked_01_AA_F"; // anti-air
        natoAA = [_armAA];

        // tank
        _tank1 = "B_mas_uk_MBT_03_cannon_F"; // armored tank1
        _tank2 = _tank1; // armored tank2
        _tank3 = _tank1; // armored tank3
        natoTank = [_tank1,_tank2,_tank3];

        // artillary
        _art1 = "B_mas_uk_MBT_01_arty_F"; // Artillary
        _art2 = "B_mas_uk_MBT_01_mlrs_F"; // MLRS
        natoArty = [_art1,_art2];

        // air
        _lb = "B_mas_uk_Heli_Light_01_F"; // little bird
        _lbA = "B_mas_uk_Heli_Light_01_armed_F"; // little bird armed
        _lbM = _lb; // little bird med
        _hu = ""; // huey
        _huA = ""; // huey armed
        _huM = ""; // huey med
        _bh = "B_mas_uk_Heli_light_03_unarmed_F"; // blackhawk
        _bhC = "B_mas_uk_Heli_light_03_unarmed_F"; // blackhawk camo
        _bhM = "B_mas_uk_Heli_light_03_med_F"; // blackhawk med
        _mh = "B_mas_uk_Heli_Transport_02_F"; // mohawk
        _ch = "B_mas_uk_CH_47F"; // chinook
        _chA = _ch; // chinook armed
        _ah = "B_mas_uk_Heli_light_03_F"; // attack heli
        _pCas = "B_mas_uk_Plane_CAS_02_F"; // plane CAS
        natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

        // sea
        _ab = "B_mas_uk_Boat_Transport_01_F"; // assault boat
        //_rb = ""; // rescue boat
        _gb = "B_mas_uk_Boat_Armed_01_F"; // gun boat
        _sub = "B_mas_uk_SDV_01_F"; // submarine
        natoSea = [_ab,_rb,_gb,_sub];

        // static
        _stH = "B_mas_uk_HMG_01_high_F"; // HMG
        _stG = "B_mas_uk_Soldier_GL_F_a"; // GMG
        _stM = "B_mas_uk_Soldier_GL_F_a"; // mortar
        _stAT = "B_mas_uk_static_AT_F"; // AT
        _stAA = "B_mas_uk_Soldier_GL_F_a"; // AA
        natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
    } else {
        if ((prWeapons == 7) || (prWeapons == 8) && (masOn)) then {  // Massi SF Weapons / US Marines Uniforms and US Marines Winter Uniform
            //-- Blufor Men
            _sl = "";
            _tl = "";
            _at = "";
            _atS = "";
            _atA = "";
            _ri = "";
            _mk = "";
            _med = "";
            _mg = "";
            _mgA = "";
            _gr = "";
            _aa = "";
            _eng = "";
            _of = "";
            _snp = "";
            _spt = "";
            _pP = "";
            _hP = "";
            _hC = "";
            _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

            //-- Blufor Vehicles
            // car
            _qd = "B_mas_mar_Quadbike_01_F"; // quad
            _jp = "B_mas_mar_HMMWV_UNA"; // jeep
            _jpA = "B_mas_mar_HMMWV_M2"; // jeep armed
            _hmv = "B_mas_mar_MRAP_01_F"; // mrap
            _hmvH = "B_mas_mar_MRAP_01_hmg_F"; // mrap HMG
            _hmvG = "B_mas_mar_MRAP_01_gmg_F"; // mrap GMG
            _hmvM = "B_mas_mar_MRAP_01_med_F"; // mrap Med
            natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

            // support
            //_sup = ""; // engine
            _supA = "B_mas_mar_Truck_01_reammo_F"; // ammo
            _supB = "B_mas_mar_Truck_01_covered_F"; // box
            _supF = "B_mas_mar_Truck_01_refuel_F"; // fuel
            _supM = _hmvM; // medical
            _supR = "B_mas_mar_Truck_01_repair_F"; // repair
            _supT = "B_mas_mar_Truck_01_transport_F"; // transport
            _supTC = _supB; // transport covered
            natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

            // apc
            _apcW = "B_mas_mar_APC_Wheeled_01_cannon_F";
            //_apcT = _apcW;
            //_apcT2 = _apcW; //bobcat
            natoAPC = [_apcW,_apcT,_apcT2];

            // anti-air
            _armAA = ""; //cheetah
            natoAA = [_armAA];

            // tank
            //_tank1 = ""; // armored tank1
            //_tank2 = _tank1; // armored tank2
            //_tank3 = _tank1; // armored tank3
            natoTank = [_tank1,_tank2,_tank3];

            // artillary
            //_art1 = ""; // Artillary
            //_art2 = ""; // MLRS
            natoArty = [_art1,_art2];

            // air
            _lb = "B_mas_mar_Heli_Light_01_F"; // little bird
            _lbA = "B_mas_mar_Heli_Light_01_armed_F"; // little bird armed
            _lbM = _lb; // little bird med
            _hu = "B_mas_mar_UH1Y_UNA_F"; // huey
            _huA = "B_mas_mar_UH1Y_F"; // huey armed
            _huM = "B_mas_mar_UH1Y_MEV_F"; // huey med
            _bh = "B_mas_mar_Heli_Transport_01_F"; // blackhawk
            _bhC = _bh; // blackhawk cammo
            _bhM = "B_mas_mar_Heli_Med_01_F"; // blackhawk med
            _mh = "B_mas_UH60M"; // mohawk
            _ch = "B_mas_CH_47F"; // chinook
            _chA = _ch; // chinook armed
            //_ah = ""; // attack heli
            _pCas = "mas_mar_F_35C"; // plane CAS
            natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

            // sea
            _ab = "B_mas_mar_Boat_Transport_01_F"; // assault boat
            //_rb = ""; // rescue boat
            _gb = "B_mas_mar_Boat_Armed_01_F"; // gun boat
            _sub = "B_mas_mar_SDV_01_F"; // submarine
            natoSea = [_ab,_rb,_gb,_sub];

            // static
            _stH = "B_mas_mar_Soldier_GL_F_d"; // HMG
            _stG = "B_mas_mar_GMG_01_F_d"; // GMG
            _stM = "B_mas_mar_Mortar_01_F_d"; // mortar
            _stAT = "B_mas_mar_static_AT_F_d"; // AT
            _stAA = "B_mas_mar_static_AA_F_d"; // AA
            natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
        } else {
            if ((prWeapons == 10) && (rhsOn)) then {  // RHS Weapons / US Marines Desert Uniform
                //-- Blufor Men
                _sl = "";
                _tl = "";
                _at = "";
                _atS = "";
                _atA = "";
                _ri = "";
                _mk = "";
                _med = "";
                _mg = "";
                _mgA = "";
                _gr = "";
                _aa = "";
                _eng = "";
                _of = "";
                _snp = "";
                _spt = "";
                _pP = "";
                _hP = "";
                _hC = "";
                _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

                //-- Blufor Vehicles
                // car
                _qd = ""; // quad
                _jp = ""; // jeep
                _jpA = ""; // jeep armed
                _hmv = ""; // mrap
                _hmvH = ""; // mrap HMG
                _hmvG = ""; // mrap GMG
                _hmvM = ""; // mrap Med
                natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

                // support
                _sup = ""; // engine
                _supA = ""; // ammo
                _supB = ""; // box
                _supF = ""; // fuel
                _supM = ""; // medical
                _supR = ""; // repair
                _supT = ""; // transport
                _supTC = ""; // transport covered
                natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

                // apc
                _apcW = "";
                _apcT = "";
                //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
                _apcT2 = ""; //bobcat
                natoAPC = [_apcW,_apcT,_apcT2];

                // anti-air
                _armAA = ""; //cheetah
                natoAA = [_armAA];

                // tank
                _tank1 = ""; // armored tank1
                _tank2 = _tank1; // armored tank2
                _tank3 = _tank1; // armored tank3
                natoTank = [_tank1,_tank2,_tank3];

                // artillary
                _art1 = ""; // Artillary
                _art2 = ""; // MLRS
                natoArty = [_art1,_art2];

                // air
                _lb = ""; // little bird
                _lbA = ""; // little bird armed
                _lbM = ""; // little bird med
                _hu = ""; // huey
                _huA = ""; // huey armed
                _huM = ""; // huey med
                _bh = ""; // blackhawk
                _bhC = ""; // blackhawk cammo
                _bhM = ""; // blackhawk med
                _mh = ""; // mohawk
                _ch = ""; // chinook
                _chA = ""; // chinook armed
                _ah = ""; // attack heli
                _pCas = ""; // plane CAS
                natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

                // sea
                _ab = ""; // assault boat
                _rb = ""; // rescue boat
                _gb = ""; // gun boat
                _sub = ""; // submarine
                natoSea = [_ab,_rb,_gb,_sub];

                // static
                _stH = ""; // HMG
                _stG = ""; // GMG
                _stM = ""; // mortar
                _stAT = ""; // AT
                _stAA = ""; // AA
                natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
            } else {
                if ((prWeapons == 15) && (unsungOn)) then {  // UNSUNG / US 173RD Airborne Brigade
                    //-- Blufor Men
                    _sl = "";
                    _tl = "";
                    _at = "";
                    _atS = "";
                    _atA = "";
                    _ri = "";
                    _mk = "";
                    _med = "";
                    _mg = "";
                    _mgA = "";
                    _gr = "";
                    _aa = "";
                    _eng = "";
                    _of = "";
                    _snp = "";
                    _spt = "";
                    _pP = "";
                    _hP = "";
                    _hC = "";
                    _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

                    //-- Blufor Vehicles
                    // car
                    _qd = ""; // quad
                    _jp = ""; // jeep
                    _jpA = ""; // jeep armed
                    _hmv = ""; // mrap
                    _hmvH = ""; // mrap HMG
                    _hmvG = ""; // mrap GMG
                    _hmvM = ""; // mrap Med
                    natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

                    // support
                    _sup = ""; // engine
                    _supA = ""; // ammo
                    _supB = ""; // box
                    _supF = ""; // fuel
                    _supM = ""; // medical
                    _supR = ""; // repair
                    _supT = ""; // transport
                    _supTC = ""; // transport covered
                    natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

                    // apc
                    _apcW = "";
                    _apcT = "";
                    //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
                    _apcT2 = ""; //bobcat
                    natoAPC = [_apcW,_apcT,_apcT2];

                    // anti-air
                    _armAA = ""; //cheetah
                    natoAA = [_armAA];

                    // tank
                    _tank1 = ""; // armored tank1
                    _tank2 = _tank1; // armored tank2
                    _tank3 = _tank1; // armored tank3
                    natoTank = [_tank1,_tank2,_tank3];

                    // artillary
                    _art1 = ""; // Artillary
                    _art2 = ""; // MLRS
                    natoArty = [_art1,_art2];

                    // air
                    _lb = ""; // little bird
                    _lbA = ""; // little bird armed
                    _lbM = ""; // little bird med
                    _hu = ""; // huey
                    _huA = ""; // huey armed
                    _huM = ""; // huey med
                    _bh = ""; // blackhawk
                    _bhC = ""; // blackhawk cammo
                    _bhM = ""; // blackhawk med
                    _mh = ""; // mohawk
                    _ch = ""; // chinook
                    _chA = ""; // chinook armed
                    _ah = ""; // attack heli
                    _pCas = ""; // plane CAS
                    natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

                    // sea
                    _ab = ""; // assault boat
                    _rb = ""; // rescue boat
                    _gb = ""; // gun boat
                    _sub = ""; // submarine
                    natoSea = [_ab,_rb,_gb,_sub];

                    // static
                    _stH = ""; // HMG
                    _stG = ""; // GMG
                    _stM = ""; // mortar
                    _stAT = ""; // AT
                    _stAA = ""; // AA
                    natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
                } else {
                    if ((prWeapons == 16) && (unsungOn)) then {  // UNSUNG / US 1st Infantry Division
                        //-- Blufor Men
                        _sl = "";
                        _tl = "";
                        _at = "";
                        _atS = "";
                        _atA = "";
                        _ri = "";
                        _mk = "";
                        _med = "";
                        _mg = "";
                        _mgA = "";
                        _gr = "";
                        _aa = "";
                        _eng = "";
                        _of = "";
                        _snp = "";
                        _spt = "";
                        _pP = "";
                        _hP = "";
                        _hC = "";
                        _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

                        //-- Blufor Vehicles
                        // car
                        _qd = ""; // quad
                        _jp = ""; // jeep
                        _jpA = ""; // jeep armed
                        _hmv = ""; // mrap
                        _hmvH = ""; // mrap HMG
                        _hmvG = ""; // mrap GMG
                        _hmvM = ""; // mrap Med
                        natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

                        // support
                        _sup = ""; // engine
                        _supA = ""; // ammo
                        _supB = ""; // box
                        _supF = ""; // fuel
                        _supM = ""; // medical
                        _supR = ""; // repair
                        _supT = ""; // transport
                        _supTC = ""; // transport covered
                        natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

                        // apc
                        _apcW = "";
                        _apcT = "";
                        //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
                        _apcT2 = ""; //bobcat
                        natoAPC = [_apcW,_apcT,_apcT2];

                        // anti-air
                        _armAA = ""; //cheetah
                        natoAA = [_armAA];

                        // tank
                        _tank1 = ""; // armored tank1
                        _tank2 = _tank1; // armored tank2
                        _tank3 = _tank1; // armored tank3
                        natoTank = [_tank1,_tank2,_tank3];

                        // artillary
                        _art1 = ""; // Artillary
                        _art2 = ""; // MLRS
                        natoArty = [_art1,_art2];

                        // air
                        _lb = ""; // little bird
                        _lbA = ""; // little bird armed
                        _lbM = ""; // little bird med
                        _hu = ""; // huey
                        _huA = ""; // huey armed
                        _huM = ""; // huey med
                        _bh = ""; // blackhawk
                        _bhC = ""; // blackhawk cammo
                        _bhM = ""; // blackhawk med
                        _mh = ""; // mohawk
                        _ch = ""; // chinook
                        _chA = ""; // chinook armed
                        _ah = ""; // attack heli
                        _pCas = ""; // plane CAS
                        natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

                        // sea
                        _ab = ""; // assault boat
                        _rb = ""; // rescue boat
                        _gb = ""; // gun boat
                        _sub = ""; // submarine
                        natoSea = [_ab,_rb,_gb,_sub];

                        // static
                        _stH = ""; // HMG
                        _stG = ""; // GMG
                        _stM = ""; // mortar
                        _stAT = ""; // AT
                        _stAA = ""; // AA
                        natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
                    } else {
                        if ((prWeapons == 17) && (unsungOn)) then {  // UNSUNG / US 5th Special Forces Group
                            //-- Blufor Men
                            _sl = "";
                            _tl = "";
                            _at = "";
                            _atS = "";
                            _atA = "";
                            _ri = "";
                            _mk = "";
                            _med = "";
                            _mg = "";
                            _mgA = "";
                            _gr = "";
                            _aa = "";
                            _eng = "";
                            _of = "";
                            _snp = "";
                            _spt = "";
                            _pP = "";
                            _hP = "";
                            _hC = "";
                            _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

                            //-- Blufor Vehicles
                            // car
                            _qd = ""; // quad
                            _jp = ""; // jeep
                            _jpA = ""; // jeep armed
                            _hmv = ""; // mrap
                            _hmvH = ""; // mrap HMG
                            _hmvG = ""; // mrap GMG
                            _hmvM = ""; // mrap Med
                            natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

                            // support
                            _sup = ""; // engine
                            _supA = ""; // ammo
                            _supB = ""; // box
                            _supF = ""; // fuel
                            _supM = ""; // medical
                            _supR = ""; // repair
                            _supT = ""; // transport
                            _supTC = ""; // transport covered
                            natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

                            // apc
                            _apcW = "";
                            _apcT = "";
                            //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
                            _apcT2 = ""; //bobcat
                            natoAPC = [_apcW,_apcT,_apcT2];

                            // anti-air
                            _armAA = ""; //cheetah
                            natoAA = [_armAA];

                            // tank
                            _tank1 = ""; // armored tank1
                            _tank2 = _tank1; // armored tank2
                            _tank3 = _tank1; // armored tank3
                            natoTank = [_tank1,_tank2,_tank3];

                            // artillary
                            _art1 = ""; // Artillary
                            _art2 = ""; // MLRS
                            natoArty = [_art1,_art2];

                            // air
                            _lb = ""; // little bird
                            _lbA = ""; // little bird armed
                            _lbM = ""; // little bird med
                            _hu = ""; // huey
                            _huA = ""; // huey armed
                            _huM = ""; // huey med
                            _bh = ""; // blackhawk
                            _bhC = ""; // blackhawk cammo
                            _bhM = ""; // blackhawk med
                            _mh = ""; // mohawk
                            _ch = ""; // chinook
                            _chA = ""; // chinook armed
                            _ah = ""; // attack heli
                            _pCas = ""; // plane CAS
                            natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

                            // sea
                            _ab = ""; // assault boat
                            _rb = ""; // rescue boat
                            _gb = ""; // gun boat
                            _sub = ""; // submarine
                            natoSea = [_ab,_rb,_gb,_sub];

                            // static
                            _stH = ""; // HMG
                            _stG = ""; // GMG
                            _stM = ""; // mortar
                            _stAT = ""; // AT
                            _stAA = ""; // AA
                            natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
                        } else {
                            if ((prWeapons == 18) && (unsungOn)) then {  // UNSUNG / USMC 3rd Battalion, 4th Marines
                                //-- Blufor Men
                                _sl = "";
                                _tl = "";
                                _at = "";
                                _atS = "";
                                _atA = "";
                                _ri = "";
                                _mk = "";
                                _med = "";
                                _mg = "";
                                _mgA = "";
                                _gr = "";
                                _aa = "";
                                _eng = "";
                                _of = "";
                                _snp = "";
                                _spt = "";
                                _pP = "";
                                _hP = "";
                                _hC = "";
                                _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

                                //-- Blufor Vehicles
                                // car
                                _qd = ""; // quad
                                _jp = ""; // jeep
                                _jpA = ""; // jeep armed
                                _hmv = ""; // mrap
                                _hmvH = ""; // mrap HMG
                                _hmvG = ""; // mrap GMG
                                _hmvM = ""; // mrap Med
                                natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

                                // support
                                _sup = ""; // engine
                                _supA = ""; // ammo
                                _supB = ""; // box
                                _supF = ""; // fuel
                                _supM = ""; // medical
                                _supR = ""; // repair
                                _supT = ""; // transport
                                _supTC = ""; // transport covered
                                natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

                                // apc
                                _apcW = "";
                                _apcT = "";
                                //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
                                _apcT2 = ""; //bobcat
                                natoAPC = [_apcW,_apcT,_apcT2];

                                // anti-air
                                _armAA = ""; //cheetah
                                natoAA = [_armAA];

                                // tank
                                _tank1 = ""; // armored tank1
                                _tank2 = _tank1; // armored tank2
                                _tank3 = _tank1; // armored tank3
                                natoTank = [_tank1,_tank2,_tank3];

                                // artillary
                                _art1 = ""; // Artillary
                                _art2 = ""; // MLRS
                                natoArty = [_art1,_art2];

                                // air
                                _lb = ""; // little bird
                                _lbA = ""; // little bird armed
                                _lbM = ""; // little bird med
                                _hu = ""; // huey
                                _huA = ""; // huey armed
                                _huM = ""; // huey med
                                _bh = ""; // blackhawk
                                _bhC = ""; // blackhawk cammo
                                _bhM = ""; // blackhawk med
                                _mh = ""; // mohawk
                                _ch = ""; // chinook
                                _chA = ""; // chinook armed
                                _ah = ""; // attack heli
                                _pCas = ""; // plane CAS
                                natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

                                // sea
                                _ab = ""; // assault boat
                                _rb = ""; // rescue boat
                                _gb = ""; // gun boat
                                _sub = ""; // submarine
                                natoSea = [_ab,_rb,_gb,_sub];

                                // static
                                _stH = ""; // HMG
                                _stG = ""; // GMG
                                _stM = ""; // mortar
                                _stAT = ""; // AT
                                _stAA = ""; // AA
                                natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
                            } else {
                                if ((prWeapons == 19) && (unsungOn)) then {  // UNSUNG / USMC 3rd Reconnaissance Company MFR's
                                    //-- Blufor Men
                                    _sl = "";
                                    _tl = "";
                                    _at = "";
                                    _atS = "";
                                    _atA = "";
                                    _ri = "";
                                    _mk = "";
                                    _med = "";
                                    _mg = "";
                                    _mgA = "";
                                    _gr = "";
                                    _aa = "";
                                    _eng = "";
                                    _of = "";
                                    _snp = "";
                                    _spt = "";
                                    _pP = "";
                                    _hP = "";
                                    _hC = "";
                                    _bMenDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

                                    //-- Blufor Vehicles
                                    // car
                                    _qd = ""; // quad
                                    _jp = ""; // jeep
                                    _jpA = ""; // jeep armed
                                    _hmv = ""; // mrap
                                    _hmvH = ""; // mrap HMG
                                    _hmvG = ""; // mrap GMG
                                    _hmvM = ""; // mrap Med
                                    natoCar = [_qd,_jp,_jpA,_hmv,_hmvH,_hmvG,_hmvM];

                                    // support
                                    _sup = ""; // engine
                                    _supA = ""; // ammo
                                    _supB = ""; // box
                                    _supF = ""; // fuel
                                    _supM = ""; // medical
                                    _supR = ""; // repair
                                    _supT = ""; // transport
                                    _supTC = ""; // transport covered
                                    natoSupport = [_sup,_supA,_supB,_supF,_supM,_supR,_supT,_supTC];

                                    // apc
                                    _apcW = "";
                                    _apcT = "";
                                    //_apcT2 = [_apc1, _apcT] call BIS_fnc_selectRandom;
                                    _apcT2 = ""; //bobcat
                                    natoAPC = [_apcW,_apcT,_apcT2];

                                    // anti-air
                                    _armAA = ""; //cheetah
                                    natoAA = [_armAA];

                                    // tank
                                    _tank1 = ""; // armored tank1
                                    _tank2 = _tank1; // armored tank2
                                    _tank3 = _tank1; // armored tank3
                                    natoTank = [_tank1,_tank2,_tank3];

                                    // artillary
                                    _art1 = ""; // Artillary
                                    _art2 = ""; // MLRS
                                    natoArty = [_art1,_art2];

                                    // air
                                    _lb = ""; // little bird
                                    _lbA = ""; // little bird armed
                                    _lbM = ""; // little bird med
                                    _hu = ""; // huey
                                    _huA = ""; // huey armed
                                    _huM = ""; // huey med
                                    _bh = ""; // blackhawk
                                    _bhC = ""; // blackhawk cammo
                                    _bhM = ""; // blackhawk med
                                    _mh = ""; // mohawk
                                    _ch = ""; // chinook
                                    _chA = ""; // chinook armed
                                    _ah = ""; // attack heli
                                    _pCas = ""; // plane CAS
                                    natoAir = [_lb,_lbA,_lbM,_hu,_huA,_huM,_bh,_bhC,_bhM,_mh,_ch,_chA,_ah,_pCas];

                                    // sea
                                    _ab = ""; // assault boat
                                    _rb = ""; // rescue boat
                                    _gb = ""; // gun boat
                                    _sub = ""; // submarine
                                    natoSea = [_ab,_rb,_gb,_sub];

                                    // static
                                    _stH = ""; // HMG
                                    _stG = ""; // GMG
                                    _stM = ""; // mortar
                                    _stAT = ""; // AT
                                    _stAA = ""; // AA
                                    natoStatic = [_stH,_stG,_stM,_stAT,_stAA];
                                };
                            };
                        };
                    };
                };
            };
        };
    };
};

//--- Bluefor Men
_cd = _bMenDefault;
_sl  = _cd select 0;
_tl  = _cd select 1;
_at  = _cd select 2;
_atS = _cd select 3;
_atA = _cd select 4;
_ri  = _cd select 5;
_mk  = _cd select 6;
_med = _cd select 7;
_mg  = _cd select 8;
_mgA = _cd select 9;
_gr  = _cd select 10;
_aa  = _cd select 11;
_eng = _cd select 12;
_of  = _cd select 13;
_snp = _cd select 14;
_spt = _cd select 15;
_pP  = _cd select 16;
_hP  = _cd select 17;
_hC  = _cd select 18;

b_rifleSquad = [_sl,_tl,_at,_ri,_mk,_med,_mg,_mgA];
b_weaponsSquad = [_sl,_at,_mk,_gr,_med,_mg,_atA,_mgA];
b_fireTeam = [_tl,_at,_gr,_mg];
b_antiArmorTeam = [_tl,_atS,_atS,_atA];
b_antiArmorRPG = [_at,_at,_at];
b_antiAirTeam = [_tl,_aa,_aa,_ri];
b_sentry = [_ri,_gr];
b_sniperTeam = [_snp,_spt];
b_assaultSquad = [_sl,_mk,_at,_mk,_mg,_med,_mg,_mgA];
b_supportTeamCls = [_tl,_med,_med,_mg];

b_Men = [ _aa,_mg,_med,_eng,_gr,_mk,_atS,_of,_ri,_at,_sl,_tl,_atA,_mgA];
b_Men_noAA = [_mg,_med,_eng,_gr,_mk,_atS,_of,_ri,_at,_sl,_tl,_atA,_mgA];
b_rifle = [_ri];
b_mg = [_mg];
b_officer = [_of];
b_marksman = [_mk];
b_sniper = [_snp];
b_spotter = [_spt];
b_pilot = [_pP];
b_heliPilot = [_hP];
b_heliCrew = [_hC];

//-- Blufor Vehicles
// car
_nCar = natoCar;
_qd = _nCar select 0; b_quad = _qd; // quad
_jp = _nCar select 1; b_jeep = _jp; // jeep
_jpA = _nCar select 2; b_jeepArmed = _jpA; // jeep armed
_hmv = _nCar select 3; b_mrap = _hmv; // mrap
_hmvH = _nCar select 4; b_mrapHMG = _hmvH;// mrap HMG
_hmvG = _nCar select 5; b_mrapGMG = _hmvG;// mrap GMG
_hmvM = _nCar select 6; b_mrapMed = _hmvM;// mrap Med

// support
_nSup = natoSupport;
_sup = _nSup select 0; b_sup = _sup; // engine
_supA = _nSup select 1; b_supAmmo = _supA;// ammo
_supB = _nSup select 2; b_supBox = _supB;// box
_supF = _nSup select 3; b_supFuel = _supF;// fuel
_supM = _nSup select 4; b_supMed = _supM;// medical
_supR = _nSup select 5; b_supRepair = _supR;// repair
_supT = _nSup select 6; b_supTransport = _supT;// transport
_supTC = _nSup select 7; b_supTransCovered = _supTC;// transport covered

// apc
_nAPC = natoAPC;
_apcW = _nAPC select 0; b_apcWheeled = _apcW; //wheeled
_apcT = _nAPC select 1; b_apcTracked = _apcT; // tracked
_apcT2 = _nAPC select 2; b_apcTractor = _apcT2; // tracked 2

// anti-air
_nAA = natoAA;
_armAA = _nAA select 0; b_antiAir = _armAA; // anti-air

// tank
_nTnk = natoTank;
_tank1 = _nTnk select 0; b_tank1 = _tank1; // armored tank1
_tank2 = _nTnk select 1; b_tank2 = _tank2; // armored tank2
_tank3 = _nTnk select 2; b_tank3 = _tank3; // armored tank3

// artillery
_nArt = natoArty;
_art1 = _nArt select 0; b_artillary = _art1; // Artillery
_art2 = _nArt select 1; b_artillaryMLRS = _art2; // MLRS

// air
_nAir = natoAir;
_lb   = _nAir select 0; b_littleBird = _lb; // little bird
_lbA  = _nAir select 1; b_littleBirdArmed = _lbA; // little bird armed
_lbM  = _nAir select 2; b_littleBirdMed = _lbM; // little bird med
_hu   = _nAir select 3; b_huey = _hu; // huey
_huA  = _nAir select 4; b_hueyArmed = _huA; // huey armed
_huM  = _nAir select 5; b_hueyMed = _huM; // huey med
_bh   = _nAir select 6; b_blackhawk = _bh; // blackhawk
_bhC  = _nAir select 7; b_blackhawkCamo = _bhC; // blackhawk cammo
_bhM  = _nAir select 8; b_blackhawkMed = _bhM; // blackhawk med
_mh   = _nAir select 9; b_mohawk = _mh; // mohawk
_ch   = _nAir select 10; b_chinook = _ch; // chinook
_chA  = _nAir select 11; b_chinookArmed = _chA; // chinook armed
_ah   = _nAir select 12; b_attackHeli = _ah; // attack heli
_pCas = _nAir select 13; b_planeCAS = _pCas; // plane CAS


// sea
_nSea = natoSea;
_ab   = _nSea select 0; b_assaultBoat = _ab; // assault boat
_rb   = _nSea select 1; b_rescueBoat = _rb; // rescue boat
_gb   = _nSea select 2; b_gunBoat = _gb; // gun boat
_sub  = _nSea select 3; b_sub = _sub; // submarine


// static
_nSt  = natoStatic;
_stH  = _nSt select 0; b_staticHMG = _stH; // HMG
_stG  = _nSt select 1; b_staticGMG = _stG; // GMG
_stM  = _nSt select 2; b_mortar = _stM; // mortar
_stAT = _nSt select 3; b_staticAT = _stAT; // AT
_stAA = _nSt select 4; b_staticAA = _stAA; // AA

//b_heliTransport = [b_hueyArmed,];
//b_heliCargo = [];
b_heli_LV = [_huA, _bh, _bhC];
//b_veh_LV = b_car + b_supportVeh + b_ifv + b_armor + b_mbt + b_artillery; // used for LV scripts
//b_heli_LV = b_heliTransport + b_heliCargo; // need to add to LV along with plane

/*
if ((prEnemyUnits == 20) || (prEnemyUnits == 21)) then {  // AAF
    b_menDiver = ["I_diver_TL_F","I_diver_F","I_diver_exp_F"];
} else {
    b_menDiver = ["O_diver_TL_F","O_diver_F","O_diver_exp_F"];
};

//--- Diver squad
b_diverTeam = ["O_diver_TL_F","O_diver_exp_F","O_diver_F","O_diver_F"];
//--- Diver team-boat
b_diverTeamBoat = ["O_diver_TL_F","O_diver_exp_F","O_diver_F","O_diver_F","O_Boat_Transport_01_F"];
//--- Diver team-sub
b_diverTeamSub = ["O_diver_TL_F","O_diver_exp_F","O_diver_F","O_diver_F","O_SDV_01_F","O_SDV_01_F"];

//--- Specials
b_staticMortar = ["O_Mortar_01_F"];
b_staticLight = ["O_SearchLight"];
b_staticLightCool = ["O_SearchLight_Cool"];
b_staticAA = ["O_static_AA_F"];
b_staticHmgHigh = ["O_HMG_01_high_F"];

b_offroad = ["O_G_Offroad_01_F"];
//r_offroadFull = b_fireTeam + ["O_G_Offroad_01_F"];
b_offroadArmed = ["O_G_Offroad_01_armed_F"];

b_t100 = ["O_MBT_02_cannon_F"];
b_ifrit = ["O_MRAP_02_F"];
b_ifritHmg = ["O_MRAP_02_hmg_F"];
b_ifritGmg = ["O_MRAP_02_gmg_F"];

b_ship = ["O_Boat_Armed_01_hmg_F","O_Boat_Transport_01_F","O_Lifeboat","O_G_Boat_Transport_01_F"];
b_sub = ["O_SDV_01_F"];
b_static = ["O_HMG_01_F","O_HMG_01_high_F","O_HMG_01_A_F","O_GMG_01_F","O_GMG_01_high_F","O_GMG_01_A_F","O_Mortar_01_F","O_G_Mortar_01_F","O_Static_Designator_02_F","O_static_AA_F","O_static_AT_F"];
b_autono = ["O_UGV_01_F","O_UGV_01_rcws_F","O_UAV_01_F","O_UAV_02_F","O_UAV_02_CAS_F"];

//--- animals
b_dogs = ["Alsatian_Sandblack_F","Alsatian_Sand_F","Fin_random_F"];
*/

//--- [r_carTransport] call r_randomSpawn;
randomUnit = "";
r_randomSpawn = {
    _units = _this select 0;
    randomUnit = _units call BIS_fnc_selectRandom;
    randomUnit = [randomUnit];
};
    
blu_unitsRead = true;
diag_log format ["*PR* fn_bluUnits complete"];
