/* fn_redUnits.sqf
    usage: [r_fireteam, "spawnMrk", "area1", "CO"] call PR_fnc_unitSpawn;
*/
diag_log format ["*PR* fn_redUnits start"];

//--- Opfor Teams
//--- Infantry

_csatDefault = [];
_csatGuerilla = [];
_csatUrban = [];
_csatViper = [];
_csatPacific = [];
_csatPacificViper = [];
_csatRhsMsvGeneral = [];
_csatRhsMsvMountain = [];
_csatRhsVdvGeneral = [];
_csatRhsVdvMountain = [];
_csatRhsVdvMulti = [];
_csatRhsVdvDesert = [];
_csatUnsungNvaGeneral = [];
_csatUnsungNvaJungle = [];
_csatUnsungVcRecon = [];
_csatUnsungVcGuerilla = [];
_csatAaf = [];
_csatAafGuerilla = [];
_csatAafBandits = [];
_csatAafParamilitary = [];

_useRHS = false;
_useMSV = false;
_useVDV = false;
_useUNSUNG = false;
_useAAF = false;

//--- Default Arma 3 Opfor units
_sl = "O_Soldier_SL_F";
_tl = "O_Soldier_TL_F";
_at = "O_Soldier_LAT_F";
_atS = "O_soldier_AT_F";
_atA = "O_soldier_AAT_F";
_ri = "O_Soldier_F";
_mk = "O_soldier_M_F";
_med = "O_medic_F";
_mg = "O_Soldier_AR_F";
_mgA = "O_Soldier_AAR_F";
_gr = "O_Soldier_GL_F";
_aa = "O_soldier_AA_F";
_eng = "O_engineer_F";
_of = "O_officer_F";
_snp = "O_sniper_F";
_spt = "O_spotter_F";
_pP = "O_Pilot_F";
_hP = "O_helipilot_F";
_hC = "O_helicrew_F";
_csatDefault = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];

if (((prEnemyUnits == 10) || (prEnemyUnits == 11) || (prEnemyUnits == 12) || (prEnemyUnits == 13) || (prEnemyUnits == 14) || (prEnemyUnits == 15)) && (rhsOn)) then {
    _useRHS  = true;
    if ((prEnemyUnits == 10) || (prEnemyUnits == 11)) then { 
        _useMSV = true; 
    } else { 
        if ((prEnemyUnits == 12) || (prEnemyUnits == 13) || (prEnemyUnits == 14) || (prEnemyUnits == 15)) then { 
            _useVDV = true; 
        };
    };
};

if ((prEnemyUnits == 20) || (prEnemyUnits == 21) || (prEnemyUnits == 22) || (prEnemyUnits == 23) && (unsungOn)) then { _useUNSUNG = true; };

if ((prEnemyUnits == 50) || (prEnemyUnits == 51) || (prEnemyUnits == 52) || (prEnemyUnits == 53)) then { _useAAF = true; };

if ((prEnemyUnits == 1) || (prEnemyUnits == 2) || (prEnemyUnits == 3) || (prEnemyUnits == 4) || (prEnemyUnits == 5) || (prEnemyUnits == 6) || (prEnemyUnits == 7)) then {
    if (prEnemyUnits == 2) then {  // CSAT - Guerilla
        _sl = "O_G_Soldier_SL_F";
        _tl = "O_G_Soldier_TL_F";
        _at = "O_G_Soldier_LAT_F";
        _atS = "O_G_Soldier_LAT_F";
        _atA = "O_G_Soldier_F";
        _ri = "O_G_Soldier_F";
        _mk = "O_G_Soldier_M_F";
        _med = "O_G_medic_F";
        _mg = "O_G_Soldier_AR_F";
        _mgA = "O_G_Soldier_AR_F";
        _gr = "O_G_Soldier_GL_F";
        _aa = "O_soldier_AA_F";
        _eng = "O_G_engineer_F";
        _of = "O_G_officer_F";
        _snp = _mk;
        _spt = _mk;
        //_uni = "o_g_soldier_universal_f";
        _csatGuerilla = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
        _csatDefault = _csatGuerilla;
        //r_antiAirTeam = [_uni,_tl,_aa,_aa,_ri];
        //r_menPilot = [_uni,"O_Pilot_F","O_helipilot_F","O_helicrew_F","O_soldier_PG_F"];
    } else {
        if (prEnemyUnits == 3) then {  // CSAT - Urban
            _sl = "O_SoldierU_SL_F";
            _tl = "O_soldierU_TL_F";
            _at = "O_soldierU_LAT_F";
            _atS = "O_soldierU_AT_F";
            _atA = "O_soldierU_AAT_F";
            _ri = "O_soldierU_F";
            _mk = "O_soldierU_M_F";
            _med = "O_soldierU_medic_F";
            _mg = "O_soldierU_AR_F";
            _mgA = "O_soldierU_AR_F";
            _gr = "O_SoldierU_GL_F";
            _aa = "O_soldierU_AA_F";
            _eng = "O_engineer_U_F";
            _of = _sl;
            _snp = "O_Urban_Sharpshooter_F";
            _spt = _mk;
            //_uni = "o_g_soldier_universal_f";
            _csatUrban = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
            _csatDefault = _csatUrban;
            //r_menPilot = [_uni,"O_Pilot_F","O_helipilot_F","O_helicrew_F","O_soldier_PG_F"];
        } else {
            if (prEnemyUnits == 5) then {  // CSAT - Viper
                _sl = "O_V_Soldier_TL_hex_F";
                _tl = "O_V_Soldier_TL_hex_F";
                _at = "O_V_Soldier_LAT_hex_F";
                _atS = "O_V_Soldier_LAT_hex_F";
                _atA = "O_V_Soldier_hex_F";
                _ri = "O_V_Soldier_hex_F";
                _mk = "O_V_Soldier_M_hex_F";
                _med = "O_V_Soldier_Medic_hex_F";
                _mg = "O_V_Soldier_hex_F";
                _mgA = "O_V_Soldier_hex_F";
                _gr = "O_V_Soldier_hex_F";
                _aa = "O_V_Soldier_LAT_hex_F";
                _eng = "O_V_Soldier_Exp_hex_F";
                _of = _sl;
                _snp = _mk;
                _spt = _mk;
                //_uni = "o_g_soldier_universal_f";
                _csatViper = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                _csatDefault = _csatViper;
                //r_menPilot = [_uni,"O_Pilot_F","O_helipilot_F","O_helicrew_F","O_soldier_PG_F"];
            } else {
                if (prEnemyUnits == 6) then {  // CSAT - Pacific
                    _sl = "O_T_Soldier_SL_F";
                    _tl = "O_T_Soldier_TL_F";
                    _at = "O_T_Soldier_LAT_F";
                    _atS = "O_T_Soldier_AT_F";
                    _atA = "O_T_Soldier_AAT_F";
                    _ri = "O_T_Soldier_F";
                    _mk = "O_T_Soldier_M_F";
                    _med = "O_T_Medic_F";
                    _mg = "O_T_Soldier_AR_F";
                    _mgA = "O_T_Soldier_AAR_F";
                    _gr = "O_T_Soldier_GL_F";
                    _aa = "O_T_Soldier_AA_F";
                    _eng = "O_T_Engineer_F";
                    _of = _sl;
                    _snp = "O_T_Sniper_F";
                    _spt = "O_T_Spotter_F";
                    _pP = "O_T_Pilot_F";
                    _hP = "O_T_Helipilot_F";
                    _hC = "O_T_Helicrew_F";

                    _uni = "o_g_soldier_universal_f";
                    _csatPacific = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                    _csatDefault = _csatPacific;
                    //r_menPilot = [_uni,"O_T_Pilot_F","O_T_Helipilot_F","O_T_Helicrew_F","O_T_Soldier_PG_F"];
                } else {
                    if (prEnemyUnits == 7) then {  // CSAT - Pacific Viper
                        _sl = "O_V_Soldier_TL_ghex_F";
                        _tl = "O_V_Soldier_TL_ghex_F";
                        _at = "O_V_Soldier_LAT_ghex_F";
                        _atS = "O_V_Soldier_LAT_ghex_F";
                        _atA = "O_V_Soldier_ghex_F";
                        _ri = "O_V_Soldier_ghex_F";
                        _mk = "O_V_Soldier_M_ghex_F";
                        _med = "O_V_Soldier_Medic_ghex_F";
                        _mg = "O_V_Soldier_ghex_F";
                        _mgA = "O_V_Soldier_ghex_F";
                        _gr = "O_V_Soldier_ghex_F";
                        _aa = "O_V_Soldier_LAT_ghex_F";
                        _eng = "O_V_Soldier_Exp_ghex_F";
                        _of = _sl;
                        _snp = _mk;
                        _spt = _mk;
                        _pP = "O_T_Pilot_F";
                        _hP = "O_T_Helipilot_F";
                        _hC = "O_T_Helicrew_F";

                        //_uni = "o_g_soldier_universal_f";
                        _csatPacificViper = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                        _csatDefault = _csatPacificViper;
                        //r_menPilot = [_uni,"O_T_Pilot_F","O_T_Helipilot_F","O_T_Helicrew_F","O_T_Soldier_PG_F"];
                    };
                };
            };
        };
    };
} else {
    if (_useRHS) then {
        _pP = "rhs_pilot";
        _hP = "rhs_pilot_combat_heli";
        if (prEnemyUnits == 10) then {  // RHS - Russian Ground Forces [MSV] - General
            _sl = "rhs_msv_emr_sergeant";
            _tl = "rhs_msv_emr_junior_sergeant";
            _at = "rhs_msv_emr_LAT";
            _atS = "rhs_msv_emr_at";
            _atA = "rhs_msv_emr_strelok_rpg_assist";
            _ri = "rhs_msv_emr_rifleman";
            _mk = "rhs_msv_emr_marksman";
            _med = "rhs_msv_emr_medic";
            _mg = "rhs_msv_emr_machinegunner";
            _mgA = "rhs_msv_emr_machinegunner_assistant";
            _gr = "rhs_msv_emr_grenadier";
            _aa = "rhs_msv_emr_aa";
            _eng = "rhs_msv_emr_engineer";
            _of = "rhs_msv_emr_officer";
            _snp = _mk;
            _spt = _mk;
            _hC = "rhs_msv_emr_crew";
            _csatRhsMsvGeneral = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
            _csatDefault = _csatRhsMsvGeneral;
        } else {
            if (prEnemyUnits == 11) then {  // RHS - Russian Ground Forces [MSV] - Mountain
                _sl = "rhs_msv_sergeant";
                _tl = "rhs_msv_junior_sergeant";
                _at = "rhs_msv_LAT";
                _atS = "rhs_msv_at";
                _atA = "rhs_msv_strelok_rpg_assist";
                _ri = "rhs_msv_rifleman";
                _mk = "rhs_msv_marksman";
                _med = "rhs_msv_medic";
                _mg = "rhs_msv_machinegunner";
                _mgA = "rhs_msv_machinegunner_assistant";
                _gr = "rhs_msv_grenadier";
                _aa = "rhs_msv_aa";
                _eng = "rhs_msv_engineer";
                _of = "rhs_msv_officer";
                _snp = _mk;
                _spt = _mk;
                _hC = "rhs_msv_crew";
                _csatRhsMsvMountain = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                _csatDefault = _csatRhsMsvMountain;
            } else {
                if (prEnemyUnits == 12) then {  // RHS - Russian Airborne Troops [VDV] - General
                    _sl = "rhs_vdv_sergeant";
                    _tl = "rhs_vdv_junior_sergeant";
                    _at = "rhs_vdv_LAT";
                    _atS = "rhs_vdv_at";
                    _atA = "rhs_vdv_strelok_rpg_assist";
                    _ri = "rhs_vdv_rifleman";
                    _mk = "rhs_vdv_marksman";
                    _med = "rhs_vdv_medic";
                    _mg = "rhs_vdv_machinegunner";
                    _mgA = "rhs_vdv_machinegunner_assistant";
                    _gr = "rhs_vdv_grenadier";
                    _aa = "rhs_vdv_aa";
                    _eng = "rhs_vdv_engineer";
                    _of = "rhs_vdv_officer";
                    _snp = _mk;
                    _spt = _mk;
                    _hC = "rhs_vdv_crew";
                    _csatRhsVdvGeneral = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                    _csatDefault = _csatRhsVdvGeneral;
                } else {
                    if (prEnemyUnits == 13) then {  // RHS - Russian Airborne Troops [VDV] - Mountain
                        _sl = "rhs_vdv_flora_sergeant";
                        _tl = "rhs_vdv_flora_junior_sergeant";
                        _at = "rhs_vdv_flora_LAT";
                        _atS = "rhs_vdv_flora_at";
                        _atA = "rhs_vdv_flora_strelok_rpg_assist";
                        _ri = "rhs_vdv_flora_rifleman";
                        _mk = "rhs_vdv_flora_marksman";
                        _med = "rhs_vdv_flora_medic";
                        _mg = "rhs_vdv_flora_machinegunner";
                        _mgA = "rhs_vdv_flora_machinegunner_assistant";
                        _gr = "rhs_vdv_flora_grenadier";
                        _aa = "rhs_vdv_flora_aa";
                        _eng = "rhs_vdv_flora_engineer";
                        _of = "rhs_vdv_flora_officer";
                        _snp = _mk;
                        _spt = _mk;
                        _hC = "rhs_vdv_flora_crew";
                        _csatRhsVdvMountain = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                        _csatDefault = _csatRhsVdvMountain;
                    } else {
                        if (prEnemyUnits == 14) then {  // RHS - Russian Airborne Troops [VDV] - Multi
                            _sl = "rhs_vdv_mflora_sergeant";
                            _tl = "rhs_vdv_mflora_junior_sergeant";
                            _at = "rhs_vdv_mflora_LAT";
                            _atS = "rhs_vdv_mflora_at";
                            _atA = "rhs_vdv_mflora_strelok_rpg_assist";
                            _ri = "rhs_vdv_mflora_rifleman";
                            _mk = "rhs_vdv_mflora_marksman";
                            _med = "rhs_vdv_mflora_medic";
                            _mg = "rhs_vdv_mflora_machinegunner";
                            _mgA = "rhs_vdv_mflora_machinegunner_assistant";
                            _gr = "rhs_vdv_mflora_grenadier";
                            _aa = "rhs_vdv_mflora_aa";
                            _eng = "rhs_vdv_mflora_engineer";
                            _of = "rhs_vdv_mflora_officer";
                            _snp = _mk;
                            _spt = _mk;
                            _hC = "rhs_vdv_mflora_crew";
                            _csatRhsVdvMulti = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                            _csatDefault = _csatRhsVdvMulti;
                        } else {
                            if (prEnemyUnits == 15) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                                _sl = "rhs_vdv_des_sergeant";
                                _tl = "rhs_vdv_des_junior_sergeant";
                                _at = "rhs_vdv_des_LAT";
                                _atS = "rhs_vdv_des_at";
                                _atA = "rhs_vdv_des_strelok_rpg_assist";
                                _ri = "rhs_vdv_des_rifleman";
                                _mk = "rhs_vdv_des_marksman";
                                _med = "rhs_vdv_des_medic";
                                _mg = "rhs_vdv_des_machinegunner";
                                _mgA = "rhs_vdv_des_strelok_rpg_assist";
                                _gr = "rhs_vdv_des_grenadier";
                                _aa = "rhs_vdv_des_aa";
                                _eng = "rhs_vdv_des_engineer";
                                _of = "rhs_vdv_des_officer";
                                _snp = _mk;
                                _spt = _mk;
                                _hC = "rhs_vdv_des_crew";
                                _csatRhsVdvDesert = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                                _csatDefault = _csatRhsVdvDesert;
                            };
                        };
                    };
                };
            };
        };
    } else {
        if (_useUNSUNG) then {  //--- unsung Units
            _pP = "uns_nvaf_pilot3";
            _hP = "uns_nvaf_pilot2";
            _hC = "uns_nvaf_pilot4";
            if (prEnemyUnits == 20) then {  // CSAT - UNSUNG - [NVA] - General
                _sl = "uns_men_NVA_65_COM";
                _tl = "uns_men_NVA_65_nco";
                _at = "uns_men_NVA_65_AT2";
                _atS = "uns_men_NVA_65_AT";
                _atA = "uns_men_NVA_65_AS3";
                _ri = "uns_men_NVA_65_RF1";
                _mk = "uns_men_NVA_65_MRK";
                _med = "uns_men_NVA_65_MED";
                _mg = "uns_men_NVA_65_LMG";
                _mgA = "uns_men_NVA_65_AS6";
                _gr = "uns_men_NVA_65_RTO";
                _aa = "uns_men_NVA_65_AA";
                _eng = "uns_men_NVA_65_SAP";
                _of = "uns_men_NVA_65_off";
                _snp = _mk;
                _spt = _mk;
                _csatUnsungNvaGeneral = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                _csatDefault = _csatUnsungNvaGeneral;
            } else {
                if (prEnemyUnits == 21) then {  // CSAT - UNSUNG - [NVA] - Jungle
                    _sl = "uns_men_NVA_68_COM";
                    _tl = "uns_men_NVA_68_nco";
                    _at = "uns_men_NVA_68_AT2";
                    _atS = "uns_men_NVA_68_AT";
                    _atA = "uns_men_NVA_68_AS3";
                    _ri = "uns_men_NVA_68_RF3";
                    _mk = "uns_men_NVA_68_MRK";
                    _med = "uns_men_NVA_68_MED";
                    _mg = "uns_men_NVA_68_LMG";
                    _mgA = "uns_men_NVA_68_AS6";
                    _gr = "uns_men_NVA_68_RTO";
                    _aa = "uns_men_NVA_68_AA";
                    _eng = "uns_men_NVA_68_SAP";
                    _of = "uns_men_NVA_68_off";
                    _snp = _mk;
                    _spt = _mk;
                    _csatUnsungNvaJungle = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                    _csatDefault = _csatUnsungNvaJungle;
                } else {
                    if (prEnemyUnits == 22) then {  // CSAT - UNSUNG - [VC] - Recon
                        _sl = "uns_men_VC_recon_nco";
                        _tl = "uns_men_VC_recon_nco";
                        _at = "uns_men_VC_recon_AT";
                        _atS = "uns_men_VC_recon_AT";
                        _atA = "uns_men_VC_recon_MTS";
                        _ri = "uns_men_VC_recon_RF6";
                        _mk = "uns_men_VC_recon_MRK";
                        _med = "uns_men_VC_recon_MED";
                        _mg = "uns_men_VC_recon_LMG";
                        _mgA = "uns_men_VC_recon_MGS";
                        _gr = "uns_men_VC_recon_RTO";
                        _aa = "uns_men_VC_recon_MGS";
                        _eng = "uns_men_VC_recon_SAP";
                        _of = "uns_men_VC_recon_off";
                        _snp = _mk;
                        _spt = _mk;
                        _csatUnsungVcRecon = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                        _csatDefault = _csatUnsungVcRecon;
                    } else {
                        if (prEnemyUnits == 23) then {  // CSAT - UNSUNG - [VC] - Guerilla
                            _sl = "uns_men_VC_local_off";
                            _tl = "uns_men_VC_mainforce_nco";
                            _at = "uns_men_VC_mainforce_AT2";
                            _atS = "uns_men_VC_mainforce_AT";
                            _atA = "uns_men_VC_mainforce_RF4";
                            _ri = "uns_men_VC_mainforce_RF3";
                            _mk = "uns_men_VC_mainforce_MRK";
                            _med = "uns_men_VC_mainforce_MED";
                            _mg = "uns_men_VC_mainforce_HMG";
                            _mgA = "uns_men_VC_recon_MGS";
                            _gr = "uns_men_VC_mainforce_RTO";
                            _aa = "uns_men_VC_recon_MGS";
                            _eng = "uns_men_VC_mainforce_SAP";
                            _of = "uns_men_VC_mainforce_off";
                            _snp = _mk;
                            _spt = _mk;
                            _csatUnsungVcGuerilla = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                            _csatDefault = _csatUnsungVcGuerilla;
                        };
                    };
                };
            };
        } else {
            if (_useAAF) then {  //--- AAF Units
                _pP = "I_pilot_F";
                _hP = "I_helipilot_F";
                _hC = "I_helicrew_F";
                if (prEnemyUnits == 50) then {  // AAF
                    _sl = "I_Soldier_SL_F";
                    _tl = "I_Soldier_TL_F";
                    _at = "I_Soldier_LAT_F";
                    _atS = "I_Soldier_AT_F";
                    _atA = "I_Soldier_lite_F";
                    _ri = "I_soldier_F";
                    _mk = "I_Soldier_M_F";
                    _med = "I_medic_F";
                    _mg = "I_Soldier_AR_F";
                    _mgA = "I_Soldier_lite_F";
                    _gr = "I_Soldier_GL_F";
                    _aa = "I_Soldier_AA_F";
                    _eng = "I_engineer_F";
                    _of = "I_officer_F";
                    _snp = "I_Sniper_F";
                    _spt = "I_Spotter_F";
                    _csatAaf = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                    _csatDefault = _csatAaf;
                } else {
                    if (prEnemyUnits == 51) then {  // AAF - Guerilla
                        _sl = "I_G_Soldier_SL_F";
                        _tl = "I_G_Soldier_TL_F";
                        _at = "I_G_Soldier_LAT_F";
                        _atS = "I_G_Soldier_LAT_F";
                        _atA = "I_G_Soldier_lite_F";
                        _ri = "I_G_Soldier_F";
                        _mk = "I_G_Soldier_M_F";
                        _med = "I_G_medic_F";
                        _mg = "I_G_Soldier_AR_F";
                        _mgA = "I_G_Soldier_lite_F";
                        _gr = "I_G_Soldier_GL_F";
                        _aa = "I_G_Soldier_F";
                        _eng = "I_G_engineer_F";
                        _of = "I_G_officer_F";
                        _snp = _mk;
                        _spt = _mk;
                        _csatAafGuerilla = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                        _csatDefault = _csatAafGuerilla;
                    } else {
                        if (prEnemyUnits == 52) then {  // AAF - Bandits
                            _sl = "I_C_Soldier_Bandit_7_F";
                            _tl = "I_C_Soldier_Bandit_5_F";
                            _at = "I_C_Soldier_Bandit_2_F";
                            _atS = "I_C_Soldier_Bandit_2_F";
                            _atA = "I_C_Soldier_Bandit_8_F";
                            _ri = "I_C_Soldier_Bandit_4_F";
                            _mk = "I_C_Soldier_Bandit_4_F";
                            _med = "I_C_Soldier_Bandit_1_F";
                            _mg = "I_C_Soldier_Bandit_3_F";
                            _mgA = "I_C_Soldier_Bandit_3_F";
                            _gr = "I_C_Soldier_Bandit_6_F";
                            _aa = "I_C_Soldier_Bandit_2_F";
                            _eng = "I_C_Soldier_Bandit_8_F";
                            _of = _sl;
                            _snp = _mk;
                            _spt = _mk;
                            _csatAafBandits = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                            _csatDefault = _csatAafBandits;
                        } else {
                            if (prEnemyUnits == 53) then {  // AAF - Paramilitary
                                _sl = "I_C_Soldier_Para_2_F";
                                _tl = "I_C_Soldier_Para_7_F";
                                _at = "I_C_Soldier_Para_5_F";
                                _atS = "I_C_Soldier_Para_5_F";
                                _atA = "I_C_Soldier_Para_1_F";
                                _ri = "I_C_Soldier_Para_1_F";
                                _mk = "I_C_Soldier_Para_2_F";
                                _med = "I_C_Soldier_Para_3_F";
                                _mg = "I_C_Soldier_Para_4_F";
                                _mgA = "I_C_Soldier_Para_4_F";
                                _gr = "I_C_Soldier_Para_6_F";
                                _aa = "I_C_Soldier_Para_5_F";
                                _eng = "I_C_Soldier_Para_8_F";
                                _of = _sl;
                                _snp = _mk;
                                _spt = _mk;
                                _csatAafParamilitary = [_sl,_tl,_at,_atS,_atA,_ri,_mk,_med,_mg,_mgA,_gr,_aa,_eng,_of,_snp,_spt,_pP,_hP,_hC];
                                _csatDefault = _csatAafParamilitary;
                            };
                        };
                    };
                };
            };
        };
    };
};

_cd = _csatDefault;
_sl  = _cd select 0;
_tl  = _cd select 1;
_at  = _cd select 2;
_atS = _cd select 3;
_atA = _cd select 4;
_ri  = _cd select 5;
_mk  = _cd select 6;
_med = _cd select 7;
_mg  = _cd select 8;
_mgA = _cd select 9;
_gr  = _cd select 10;
_aa  = _cd select 11;
_eng = _cd select 12;
_of  = _cd select 13;
_snp = _cd select 14;
_spt = _cd select 15;
_pP  = _cd select 16;
_hP  = _cd select 17;
_hC  = _cd select 18;

r_rifleSquad = [_sl,_tl,_at,_ri,_mk,_med,_mg,_mgA];
r_weaponsSquad = [_sl,_at,_mk,_gr,_med,_mg,_atA,_mgA];
r_fireTeam = [_tl,_at,_gr,_mg];
r_antiArmorTeam = [_tl,_atS,_atS,_atA];
r_antiArmorRPG = [_at,_at,_at];
r_antiAirTeam = [_tl,_aa,_aa,_ri];
r_sentry = [_ri,_gr];
r_sniperTeam = [_snp,_spt];
r_assaultSquad = [_sl,_mk,_at,_mk,_mg,_med,_mg,_mgA];
r_supportTeamCls = [_tl,_med,_med,_mg];

//--- Opfor Men
r_Men = [ _aa,_mg,_med,_eng,_gr,_mk,_atS,_of,_ri,_at,_sl,_tl,_atA,_mgA];
r_Men_noAA = [_mg,_med,_eng,_gr,_mk,_atS,_of,_ri,_at,_sl,_tl,_atA,_mgA];
r_rifle = [_ri];
r_mg = [_mg];
r_officer = [_of];
r_marksman = [_mk];
r_sniper = [_snp];
r_spotter = [_spt];
r_pilot = [_pP];
r_heliPilot = [_hP];
r_heliCrew = [_hC];


//Opfor Vehicles
fnc_defaultRedVeh = {
    r_car = ["O_MRAP_02_F","O_Quadbike_01_F","O_Truck_02_covered_F","O_Truck_02_transport_F","O_G_Offroad_01_F","O_G_Offroad_01_armed_F","O_G_Quadbike_01_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_Truck_03_device_F","O_G_Van_01_transport_F"];
    r_transportSm = ["O_MRAP_02_F","O_G_Offroad_01_F"];
    r_transportLg = ["O_Truck_02_covered_F","O_Truck_02_transport_F","O_Truck_03_transport_F","O_Truck_03_covered_F"];
    r_supportVeh = ["O_Truck_03_repair_F", "O_Truck_03_ammo_F", "O_Truck_03_fuel_F", "O_Truck_03_medical_F", "O_Truck_02_box_F", "O_Truck_02_medical_F", "O_Truck_02_Ammo_F", "O_Truck_02_fuel_F", "O_G_Van_01_fuel_F", "O_G_Offroad_01_repair_F"];
    r_ifv = ["O_MRAP_02_hmg_F","O_MRAP_02_gmg_F"];
    r_armor = ["O_APC_Wheeled_02_rcws_F","O_APC_Tracked_02_cannon_F","O_APC_Tracked_02_AA_F"];
    r_mbt = ["O_MBT_02_cannon_F"];
    r_artillery = ["O_MBT_02_arty_F"];
    r_aaArmor = ["O_APC_Tracked_02_AA_F"];
    // air
    r_heliSpotter = ["O_Heli_Light_02_unarmed_F"];
    r_heliTransport = ["O_Heli_Light_02_F","O_Heli_Light_02_v2_F","O_Heli_Light_02_unarmed_F"];
    r_heliCargo = ["O_Heli_Transport_04_F","O_Heli_Transport_04_ammo_F","O_Heli_Transport_04_bench_F","O_Heli_Transport_04_box_F","O_Heli_Transport_04_covered_F","O_Heli_Transport_04_fuel_F","O_Heli_Transport_04_medevac_F","O_Heli_Transport_04_repair_F","O_Heli_Transport_04_black_F","O_Heli_Transport_04_ammo_black_F","O_Heli_Transport_04_bench_black_F","O_Heli_Transport_04_box_black_F","O_Heli_Transport_04_covered_black_F","O_Heli_Transport_04_fuel_black_F","O_Heli_Transport_04_medevac_black_F","O_Heli_Transport_04_repair_black_F"];
    r_heliAttack = ["O_Heli_Attack_02_F","O_Heli_Attack_02_black_F"];
    r_plane = ["O_Plane_CAS_02_F"];
    // boat
    r_assaultBoat = ["O_Boat_Transport_01_F"];
};

[] call fnc_defaultRedVeh;

//if ((prEnemyUnits == 1) || (prEnemyUnits == 2) || (prEnemyUnits == 3) || (prEnemyUnits == 4)) then {  // Default
//    [] call fnc_defaultRedVeh;
//} else {
if ((_useRHS) || ((_useUNSUNG) && (rhsOn))) then {
    if (_useMSV) then {  // RHS - Russian Ground Forces [MSV] - General
        r_car = ["rhs_tigr_msv","rhs_tigr_3camo_msv","rhs_tigr_ffv_3camo_msv","rhs_tigr_ffv_msv","RHS_UAZ_MSV_01","rhs_uaz_open_MSV_01"];
        r_transportSm = ["rhs_tigr_msv","rhs_tigr_3camo_msv","rhs_tigr_ffv_3camo_msv","rhs_tigr_ffv_msv"/*,"rhs_uaz_open_MSV_01"*/];
        r_transportLg = ["rhs_gaz66_msv","rhs_gaz66_flat_msv","rhs_gaz66o_msv","rhs_gaz66o_flat_msv","RHS_Ural_MSV_01","RHS_Ural_Flat_MSV_01","RHS_Ural_Open_MSV_01","RHS_Ural_Open_Flat_MSV_01"];
        r_supportVeh = ["RHS_BM21_MSV_01","rhs_gaz66_msv","rhs_gaz66_ammo_msv","rhs_gaz66_flat_msv","rhs_gaz66o_msv","rhs_gaz66o_flat_msv","rhs_gaz66_r142_msv","rhs_gaz66_ap2_msv","rhs_gaz66_repair_msv","RHS_Ural_MSV_01","RHS_Ural_Flat_MSV_01","RHS_Ural_Fuel_MSV_01","RHS_Ural_Open_MSV_01","RHS_Ural_Open_Flat_MSV_01"];
        r_ifv = ["rhs_btr60_msv","rhs_btr70_msv","rhs_btr80_msv","rhs_btr80a_msv"];
        r_armor = ["rhs_bmp1_msv","rhs_bmp1d_msv","rhs_bmp1k_msv","rhs_bmp1p_msv","rhs_bmp2_msv","rhs_bmp2e_msv","rhs_bmp2d_msv","rhs_bmp2k_msv","rhs_bmp3_msv","rhs_bmp3_late_msv","rhs_bmp3m_msv","rhs_bmp3mera_msv","rhs_brm1k_msv","rhs_prp3_msv","rhs_zsu234_aa"];
        r_mbt = ["rhs_t72ba_tv","rhs_t72bb_tv","rhs_t72bc_tv","rhs_t72bd_tv","rhs_t80","rhs_t80a","rhs_t80b","rhs_t80bk","rhs_t80bv","rhs_t80bvk","rhs_t80u","rhs_t80u45m","rhs_t80ue1","rhs_t80uk","rhs_t80um","rhs_t90_tv","rhs_t90a_tv"];
        r_artillery = ["rhs_2s3_tv","rhs_9k79","rhs_9k79_K"];
        r_aaArmor = ["rhs_zsu234_aa"];
        // air
        r_heliSpotter = ["RHS_Mi8mt_vvsc"];
        r_heliTransport = ["rhs_ka60_c","RHS_Mi8AMT_vvsc","RHS_Mi8AMTSh_vvsc","RHS_Mi8AMTSh_FAB_vvsc","RHS_Mi8AMTSh_UPK23_vvsc","RHS_Mi8mt_vvsc","RHS_Mi8MTV3_vvsc","RHS_Mi8MTV3_FAB_vvsc","RHS_Mi8MTV3_UPK23_vvsc"];
        r_heliCargo = ["RHS_Mi8mt_Cargo_vvsc"];
        r_heliAttack = ["RHS_Ka52_vvsc","RHS_Ka52_UPK23_vvsc","RHS_Mi24P_AT_vvsc","RHS_Mi24P_CAS_vvsc","RHS_Mi24P_vvsc","RHS_Mi24V_AT_vvsc","RHS_Mi24V_vvsc","RHS_Mi24V_FAB_vvsc","RHS_Mi24V_UPK23_vvsc"];
        r_plane = ["RHS_Su25SM_vvs","RHS_Su25SM_KH29_vvs","RHS_Su25SM_vvsc","RHS_Su25SM_KH29_vvsc"];
        r_assaultBoat = [""];
    } else {
        if ((_useVDV) || (_useUNSUNG)) then {  // RHS - Russian Airborne Troops [VDV] - General
            r_car = ["rhs_tigr_vdv","rhs_tigr_3camo_vdv","rhs_tigr_ffv_3camo_vdv","rhs_tigr_ffv_vdv","rhs_uaz_vdv","rhs_uaz_open_vdv"];
            r_transportSm = ["rhs_tigr_vdv","rhs_tigr_3camo_vdv","rhs_tigr_ffv_3camo_vdv","rhs_tigr_ffv_vdv"/*,"rhs_uaz_vdv","rhs_uaz_open_vdv"*/];
            r_transportLg = ["rhs_gaz66_vdv","rhs_gaz66_flat_vdv","rhs_gaz66o_vdv","rhs_gaz66o_flat_vdv","RHS_Ural_VDV_01","RHS_Ural_Flat_VDV_01","RHS_Ural_Open_VDV_01","RHS_Ural_Open_Flat_VDV_01"];
            r_supportVeh = ["RHS_BM21_VDV_01","rhs_gaz66_vdv","rhs_gaz66_ammo_vdv","rhs_gaz66_flat_vdv","rhs_gaz66o_vdv","rhs_gaz66o_flat_vdv","rhs_gaz66_r142_vdv","rhs_gaz66_ap2_vdv","rhs_gaz66_repair_vdv","rhs_typhoon_vdv","RHS_Ural_VDV_01","RHS_Ural_Flat_VDV_01","RHS_Ural_Fuel_VDV_01","RHS_Ural_Open_VDV_01","RHS_Ural_Open_Flat_VDV_01"];
            r_ifv = ["rhs_btr60_vdv","rhs_btr70_vdv","rhs_btr80_vdv","rhs_btr80a_vdv"];
            r_armor = ["rhs_bmp1_vdv","rhs_bmd1k","rhs_bmd1p","rhs_bmd1pk","rhs_bmd1r","rhs_bmd2","rhs_bmd2k","rhs_bmd2m","rhs_bmd4_vdv","rhs_bmd4m_vdv","rhs_bmd4ma_vdv","rhs_bmp1_vdv","rhs_bmp1d_vdv","rhs_bmp1k_vdv","rhs_bmp1p_vdv","rhs_bmp2_vdv","rhs_bmp2e_vdv","rhs_bmp2d_vdv","rhs_bmp2k_vdv","rhs_brm1k_vdv","rhs_prp3_vdv","rhs_zsu234_aa"];
            r_mbt = ["rhs_sprut_vdv","rhs_t72ba_tv","rhs_t72bb_tv","rhs_t72bc_tv","rhs_t72bd_tv","rhs_t80","rhs_t80a","rhs_t80b","rhs_t80bk","rhs_t80bv","rhs_t80bvk","rhs_t80u","rhs_t80u45m","rhs_t80ue1","rhs_t80uk","rhs_t80um","rhs_t90_tv","rhs_t90a_tv"];
            r_artillery = ["rhs_2s3_tv","rhs_9k79","rhs_9k79_K"];
            r_aaArmor = ["rhs_zsu234_aa"];
            // air
            r_heliSpotter = ["RHS_Mi8mt_vdv","RHS_Mi8mt_vv"];
            r_heliTransport = ["RHS_Mi8mt_vv","RHS_Mi8AMT_vdv","RHS_Mi8mt_vdv","RHS_Mi8MTV3_vdv","RHS_Mi8MTV3_FAB_vdv","RHS_Mi8MTV3_UPK23_vdv"];
            r_heliCargo = ["RHS_Mi8mt_Cargo_vv","RHS_Mi8mt_Cargo_vdv"];
            r_heliAttack = ["RHS_Mi24P_AT_vdv","RHS_Mi24P_CAS_vdv","RHS_Mi24P_vdv","RHS_Mi24V_AT_vdv","RHS_Mi24V_vdv","RHS_Mi24V_FAB_vdv","RHS_Mi24V_UPK23_vdv"];
            r_plane = ["RHS_Su25SM_vvs","RHS_Su25SM_KH29_vvs","RHS_Su25SM_vvsc","RHS_Su25SM_KH29_vvsc"];
            r_assaultBoat = [""];
        };
    };
} else {
    if (_useAAF) then {  // AAF Vehicles
        r_car = ["I_Quadbike_01_F","I_MRAP_03_F","I_Truck_02_covered_F","I_Truck_02_transport_F"];
        r_transportSm = ["I_MRAP_03_F"];
        r_transportLg = ["I_Truck_02_covered_F","I_Truck_02_transport_F","",""];
        r_supportVeh = ["I_Truck_02_ammo_F","I_Truck_02_box_F","I_Truck_02_medical_F","I_Truck_02_fuel_F"];
        r_ifv = ["I_MRAP_03_hmg_F","I_MRAP_03_gmg_F"];
        r_armor = ["I_APC_tracked_03_cannon_F","I_APC_Wheeled_03_cannon_F"];
        r_mbt = ["I_MBT_03_cannon_F"];
        r_artillery = ["I_MBT_03_cannon_F"];
        r_aaArmor = ["I_APC_Wheeled_03_cannon_F"];
        // air
        r_heliSpotter = ["I_Heli_light_03_unarmed_F"];
        r_heliTransport = ["I_Heli_Transport_02_F","I_Heli_light_03_unarmed_F"];
        r_heliCargo = ["I_Heli_Transport_02_F"];
        r_heliAttack = ["I_Heli_light_03_F"];
        r_plane = ["I_Plane_Fighter_03_CAS_F","I_Plane_Fighter_03_AA_F"];
        r_assaultBoat = ["I_Boat_Transport_01_F"];

        if (prEnemyUnits == 51) then {  // AAF - Guerilla
            r_car = ["I_G_Offroad_01_F","I_G_Quadbike_01_F","I_G_Van_01_transport_F"];
            r_transportSm = ["I_G_Offroad_01_F","I_G_Van_01_transport_F"];
            r_supportVeh = ["I_G_Van_01_fuel_F","I_G_Offroad_01_repair_F"];
            r_ifv = ["I_G_Offroad_01_armed_F"];
            r_assaultBoat = ["I_G_Boat_Transport_01_F"];
        };

        if ((prEnemyUnits == 52) || (prEnemyUnits == 53)) then {  // AAF - Bandits & Paramilitary
            r_car = ["I_C_Offroad_02_unarmed_F","I_C_Van_01_transport_F"];
            r_transportSm = ["I_C_Van_01_transport_F"];
            r_heliSpotter = ["I_C_Heli_Light_01_civil_F"];
            r_plane = ["I_C_Plane_Civil_01_F"];
            r_assaultBoat = ["I_C_Boat_Transport_01_F"];
        };
    };
};

if (_useUNSUNG) then {
    r_car = ["uns_nvatruck","uns_nvatruck_open","uns_nvatruck_camo","pook_P12_NVA","pook_P18_NVA","pook_SON50_NVA","pook_URAL_UTILITY_NVA"];
    r_transportSm = ["uns_nvatruck","uns_nvatruck_open","uns_nvatruck_camo"];
    r_transportLg = ["uns_nvatruck","uns_nvatruck_open","uns_nvatruck_camo"];
    r_ifv = ["uns_BTR152_DSHK","uns_BTR152_ZPU","pook_PU12_NVA","uns_Type55","uns_Type55_RR57","uns_Type55_RR73","uns_Type55_mortar","uns_Type55_MG","uns_Type55_M40","uns_Type55_patrol","uns_Type55_LMG","uns_Type55_twinMG","uns_Type55_ZU"];
    r_armor = ["pook_ZSU_NVA","pook_ZSU57_NVA"];
    r_mbt = ["pook_ZSU_NVA","pook_ZSU57_NVA"];
    r_assaultBoat = [""];
};

r_veh_LV = r_car + r_supportVeh + r_ifv + r_armor + r_mbt + r_artillery; // used for LV scripts
r_heli_LV = r_heliTransport + r_heliCargo; // need to add to LV along with plane
r_heli_random = r_heliSpotter + r_heliTransport + r_heliCargo + r_heliAttack;

if (_useAAF) then {  // AAF
    r_menDiver = ["I_diver_TL_F","I_diver_F","I_diver_exp_F"];
} else {
    r_menDiver = ["O_diver_TL_F","O_diver_F","O_diver_exp_F"];
};

//--- Diver squad
r_diverTeam = ["O_diver_TL_F","O_diver_exp_F","O_diver_F","O_diver_F"];
//--- Diver team-boat
r_diverTeamBoat = ["O_diver_TL_F","O_diver_exp_F","O_diver_F","O_diver_F","O_Boat_Transport_01_F"];
//--- Diver team-sub
r_diverTeamSub = ["O_diver_TL_F","O_diver_exp_F","O_diver_F","O_diver_F","O_SDV_01_F","O_SDV_01_F"];

//--- Specials
r_staticMortar = ["O_Mortar_01_F"];
r_staticLight = ["O_SearchLight"];
r_staticLightCool = ["O_SearchLight_Cool"];
r_staticAA = ["O_static_AA_F"];
r_staticHmgHigh = ["O_HMG_01_high_F"];

r_offroad = ["O_G_Offroad_01_F"];
//r_offroadFull = r_fireTeam + ["O_G_Offroad_01_F"];
r_offroadArmed = ["O_G_Offroad_01_armed_F"];

r_t100 = ["O_MBT_02_cannon_F"];
r_ifrit = ["O_MRAP_02_F"];
r_ifritHmg = ["O_MRAP_02_hmg_F"];
r_ifritGmg = ["O_MRAP_02_gmg_F"];

r_ship = ["O_Boat_Armed_01_hmg_F","O_Boat_Transport_01_F","O_Lifeboat","O_G_Boat_Transport_01_F"];
r_sub = ["O_SDV_01_F"];
r_static = ["O_HMG_01_F","O_HMG_01_high_F","O_HMG_01_A_F","O_GMG_01_F","O_GMG_01_high_F","O_GMG_01_A_F","O_Mortar_01_F","O_G_Mortar_01_F","O_Static_Designator_02_F","O_static_AA_F","O_static_AT_F"];
r_autono = ["O_UGV_01_F","O_UGV_01_rcws_F","O_UAV_01_F","O_UAV_02_F","O_UAV_02_CAS_F"];

//--- animals
r_dogs = ["Alsatian_Sandblack_F","Alsatian_Sand_F","Fin_random_F"];

//--- [r_carTransport] call r_randomSpawn;
randomUnit = "";
r_randomSpawn = {
    _units = _this select 0;
    randomUnit = _units call BIS_fnc_selectRandom;
    randomUnit = [randomUnit];
};
    
red_unitsRead = true;
diag_log format ["*PR* fn_redUnits complete"];
