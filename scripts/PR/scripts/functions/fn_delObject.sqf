/* 
* Author: PapaReap 
* Function name: PR_fnc_delObject 
* Delete any object after a given amount of time 
* ver 1.0 - 2015-6-11 
*
* Arguments: 
* 0: <OBJECT> 
* 1: <SECONDS>
* Examples: 
* 0 = [this, Runtime in minutes] spawn PR_fnc_delObject; 
* 0 = [car1, 30] spawn PR_fnc_delObject; 
* 0 = [thisTrigger, 15] spawn PR_fnc_delObject; 
*/

private ["_object", "_sleep"]; 
_object = _this select 0; 
_sleep  = _this select 1; 

if isServer then { 
    sleep _sleep; 
    deleteVehicle _object; 
}; 
