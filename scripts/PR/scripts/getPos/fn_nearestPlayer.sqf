/* fn_nearestPlayer.sqf
*  Author: PapaReap
*  function name: pr_fnc_nearestPlayer
*
*  Arguments:
*  0: <OBJECT>      e.g. - (REQUIRED) object, "marker" or [position]
*
*  1: <DISTANCE>   e.g. - (REQUIRED) distance from (object, "marker" or [position]) to return value
*
*  EXAMPLES:
*  _nearest = [("marker", [position], _object), _dist] call pr_fnc_nearestPlayer
*
*  whoIsClosest = ["testMark", 90] call pr_fnc_nearestPlayer
*/

_object = _this select 0;
_nearestdist = _this select 1;
_pos = [];

_units = [player];
if (isMultiplayer) then {
    _units = [];
    { if (isPlayer _x) then { _units = _units + [_x] } } forEach playableUnits;
};

if (typeName _object == "STRING") then {
    _pos = getMarkerPos _object;
} else {
    if (typeName _object == "ARRAY") then {
        _pos = _object;
    } else {
        _pos = getPos _object;
    };
};

_nearest = objNull;
{
    _dist = (vehicle _x) distance _pos;
    if (isPlayer _x and _dist < _nearestdist) then {
        _nearest = _x;
        _nearestdist = _dist;
    };
} forEach _units;

_nearest
