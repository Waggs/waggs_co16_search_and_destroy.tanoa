/*  fn_vehDamage.sqf 
ver 1.2 - 2015-06-10 convert to function
ver 1.1 - 2014-12-20 *** added fuel loss
ver 1.0 - 2014-12-19
trigger onact: 
0 = [100, thisTrigger,4] spawn fnc_vehDamage; 
0 = [140, thisTrigger, 4] execVM "scripts\pr\scripts\vehicle\fn_vehDamage.sqf";
0 = [distance, thisTrigger, random amount of tires 1-4] spawn fnc_vehDamage; 
*/ 

private ["_c","_distance","_trigger","_veh","_4wheel_array","_8wheel_array","_4wheel_1","_4wheel_2","_4wheel_3","_4wheel_4","_8wheel_tires","_amountLoss","_fuel","_RandomLoss"];

_4wheel_array = ["nqdy_medic_hunter","nqdy_camo_hunter","nqdy_squad_hunter"]; 
_8wheel_array = ["B_Truck_01_transport_F","B_Truck_01_covered_F","B_Truck_01_medical_F","B_Truck_01_Repair_F"]; 

_4wheel_1 = ["wheel_1_1_steering", "wheel_1_2_steering", "wheel_2_1_steering", "wheel_2_2_steering"] call BIS_fnc_selectRandom; 
_4wheel_2 = ["wheel_1_1_steering", "wheel_1_2_steering", "wheel_2_1_steering", "wheel_2_2_steering"] call BIS_fnc_selectRandom; 
_4wheel_3 = ["wheel_1_1_steering", "wheel_1_2_steering", "wheel_2_1_steering", "wheel_2_2_steering"] call BIS_fnc_selectRandom; 
_4wheel_4 = ["wheel_1_1_steering", "wheel_1_2_steering", "wheel_2_1_steering", "wheel_2_2_steering"] call BIS_fnc_selectRandom; 

_8wheel_1 = ["wheel_1_1_steering","wheel_1_2_steering", "wheel_1_3_steering", "wheel_1_4_steering", "wheel_2_1_steering", "wheel_2_2_steering", "wheel_2_3_steering", "wheel_2_4_steering"] call BIS_fnc_selectRandom; 
_8wheel_2 = ["wheel_1_1_steering","wheel_1_2_steering", "wheel_1_3_steering", "wheel_1_4_steering", "wheel_2_1_steering", "wheel_2_2_steering", "wheel_2_3_steering", "wheel_2_4_steering"] call BIS_fnc_selectRandom; 
_8wheel_3 = ["wheel_1_1_steering","wheel_1_2_steering", "wheel_1_3_steering", "wheel_1_4_steering", "wheel_2_1_steering", "wheel_2_2_steering", "wheel_2_3_steering", "wheel_2_4_steering"] call BIS_fnc_selectRandom; 
_8wheel_4 = ["wheel_1_1_steering","wheel_1_2_steering", "wheel_1_3_steering", "wheel_1_4_steering", "wheel_2_1_steering", "wheel_2_2_steering", "wheel_2_3_steering", "wheel_2_4_steering"] call BIS_fnc_selectRandom; 

_distance  = _this select 0; 
_trigger   = _this select 1; 
_amount    = _this select 2; 

if (_amount == 1) then { _amount = 1; }; 
if (_amount == 2) then { _amount = [1, 2] call BIS_fnc_selectRandom; }; 
if (_amount == 3) then { _amount = [1, 2, 3] call BIS_fnc_selectRandom; }; 
if (_amount == 4) then { _amount = [1, 2, 3, 4] call BIS_fnc_selectRandom; } else { _amount = 1; }; 

_RandomLoss = [0.0, 0.25, 0.5, 0.75, 1.0] call BIS_fnc_selectRandom; //percent 

_c = 0; 
while {true} do { 
    if ((vehicle player != player) && (_c == 0)) then { 
        _veh = vehicle player; 
        if ((isEngineOn _veh) && (_veh distance _trigger < _distance) && (_c == 0) && (((typeOf _veh) in _4wheel_array) || ((typeOf _veh) in _8wheel_array))) then { 
            if ((typeOf _veh) in _4wheel_array) then { 
                if (_amount == 1) then { 
                    _random = [_veh setHit [_4wheel_1, 1]] call BIS_fnc_selectRandom; 
                }; 
                if (_amount == 2) then { 
                    _random = [[_veh setHit [_4wheel_1, 1]], [_veh setHit [_4wheel_2, 1]]] call BIS_fnc_selectRandom; 
                }; 
                if (_amount == 3) then { 
                    _random = [[_veh setHit [_4wheel_1, 1]], [_veh setHit [_4wheel_2, 1]], [_veh setHit [_4wheel_3, 1]]] call BIS_fnc_selectRandom; 
                }; 
                if (_amount == 4) then { 
                    _random = [[_veh setHit [_4wheel_1, 1]], [_veh setHit [_4wheel_2, 1]], [_veh setHit [_4wheel_3, 1]], [_veh setHit [_4wheel_4, 1]]] call BIS_fnc_selectRandom; 
                }; 
            }; 
            if ((typeOf _veh) in _8wheel_array) then { 
                if (_amount == 1) then { 
                    _random = [_veh setHit [_8wheel_1, 1]] call BIS_fnc_selectRandom; 
                }; 
                if (_amount == 2) then { 
                    _random = [[_veh setHit [_8wheel_1, 1]], [_veh setHit [_8wheel_2, 1]]] call BIS_fnc_selectRandom; 
                }; 
                if (_amount == 3) then { 
                    _random = [[_veh setHit [_8wheel_1, 1]], [_veh setHit [_8wheel_2, 1]], [_veh setHit [_8wheel_3, 1]]] call BIS_fnc_selectRandom; 
                }; 
                if (_amount == 4) then { 
                    _random = [[_veh setHit [_8wheel_1, 1]], [_veh setHit [_8wheel_2, 1]], [_veh setHit [_8wheel_3, 1]], [_veh setHit [_8wheel_4, 1]]] call BIS_fnc_selectRandom; 
                }; 
            }; 
        _amountLoss = Fuel _veh * _RandomLoss; 
        _fuel = Fuel _veh - _amountLoss; 
        _veh setFuel _fuel; 
        sleep 5; 
        _c = 1; 
        }; 
    }; 
}; 
if (_c == 1) exitWith {}; 
