/* 
fn_setVelocity.sqf 
ver 1.1 - 2015-12-08 converted to function 
ver 1.0 - 2015-04-22 
*** usage *** 
Place in vehicles init. 
1st array entry is the object to setVelocity on. 
2nd array entry sets desired start speed (example shows a speed of 85) 
3rd array entry is used for spawned vehicle, sets velocity to instant desired speed (1 = on) leave blank to not be used ex. [this,85] call fnc_setVelocity;. 
3rd array not recommended to be used in a waypoint due to instant speed changes, if used, try to keep speed changes below 10 to not see instant speed change.
_null = [this, 85, 1] call fnc_setVelocity; 

*** usage in a waypoint *** 
_null = [helitransport, 85] call fnc_setVelocity; 
_null = [helitransport, [85, 10]] call fnc_setVelocity; 
_null = [helitransport, [85, 10], 1] call fnc_setVelocity; 
*/ 

private ["_dir", "_speed", "_vehicle", "_vel"]; 

_vehicle = _this select 0; 
_vehicle setVariable ["SpeedLimit", false, false]; 
_speed = _this select 1; 

//if ((count (_this select 1)) > 1) then { 
if (typeName (_speed) == "ARRAY") then { 
    _speed = (_this select 1) select 0; 
    _height = (_this select 1) select 1; 
    _vehicle flyInHeight _height; 
}; // else { 
 //   _speed = _this select 1; 
//}; 

_dir = direction _vehicle; 
_vel = _speed * 0.2778; // (1 km/h = 0.2778 m/s) exp. 85 * 0.2778 = 23.613 m/s

sleep 1; 

if ((count _this) > 2) then { 
    if (_this select 2 == 1) then { 
        _vehicle setVelocity [sin (_dir) * _vel, cos (_dir) * _vel, 0]; 
    }; 
}; 

if (_speed < speed _vehicle) then { 
    _vehicle setVariable ["SpeedLimit", true, false]; 
    _c = 0; 
    while {((_vehicle getVariable "SpeedLimit") && (_c == 0))} do { 
        if (_speed < speed _vehicle) then { 
            //newSpeedHint = _speed; // for debug
            //hint str newSpeedHint; // for debug
            _newSpeed = speed _vehicle - 1; 
            _vehicle limitSpeed _newSpeed; 
            sleep 0.1; 
            _c = 0;
        } else { 
            if (_speed > speed _vehicle) then { 
                _vehicle setVariable ["SpeedLimit", false, false]; 
                _c = 1;
            }; 
        }; 
    }; 
} else { 
    if (_speed > speed _vehicle) then { 
        _vehicle limitSpeed _speed; 
    }; 
}; 
