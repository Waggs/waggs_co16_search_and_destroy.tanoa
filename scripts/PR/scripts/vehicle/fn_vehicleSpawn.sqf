/*
*  Author: PapaReap
*  Function name: pr_fnc_vehicleSpawn
*  Spawns vehicle and follows waypoints
*  ver 1.0 2015-12-14
*
*  Arguments:
*  0: <SPAWN/WAYPOINT - ARRAY>                                --- required
*     0: <SPAWN POSITION>                                     --- Marker or object, sets spawn position.
*     1: <FIRST WAYPOINT>                                     --- Marker or object, sets first waypoint and vehicle direction, copy of original will create additional waypoints inside.
*     2: <END WAYPOINT>                                       --- Marker or object, sets end waypoint.
*     3; <SPEED OVERRIDE> default - 120                       --- optional, limits running speed of vehicle, e.g. - 25 or 55 etc...

*  1: <VEHICLE TYPE/SIDE - ARRAY>
*     0: <VEHICLE TYPE>   default - "O_Truck_02_transport_F"  --- if using vehicle array, precede this script with example: [r_carTransport] call r_randomSpawn; use as unit; (randomUnit select 0)
*     1: <SIDE>           default - "EAST"                    --- options, use string "east", "west", "resistance", "civilian"

*  2: <VEHICLE STATUS>
*     0: <SET CAPTIVE>    default - false                     --- options, use boolean - true or false
*     1: <ALLOW DAMAGE>   default - true                      --- options, use boolean - true or false

*  3: <START WAYPOINT BEHAVIOR/COMBAT MODE/SPEED - ARRAY>
*     0: <BEHAVIOR>       default - "AWARE"                   --- options, use string - "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH"
*     1: <COMBAT MODE>    default - "YELLOW"                  --- options, use string - "BLUE", "GREEN", "WHITE", "YELLOW" and "RED"
*     2: <WP SPEED>       default - "NORMAL"                  --- options, use string - "UNCHANGED", "LIMITED", "NORMAL" and "FULL"

*  4: <START WAYPOINT STATEMENT> default - ""                 --- action to execute at waypoint completion, e.g. - "car_1 = this" or "hint 'waypoint completed' "

*  5: <END WAYPOINT TYPE/RADIUS - ARRAY>
*     0: <WAYPOINT TYPE>  default - ""                        --- options, use string - "MOVE", "DESTROY", "GETIN", "SAD", "JOIN", "LEADER", "GETOUT", "CYCLE", "LOAD", "UNLOAD", "TR UNLOAD",
*                                                                                      "HOLD", "SENTRY", "GUARD", "TALK", "SCRIPTED", "SUPPORT", "GETIN NEAREST", "DISMISS" and "LOITER"
*     1: <RADIUS>         default - 0                         --- optional, set radius, e.g. - 230, 300 etc...

*  6: <END WAYPOINT BEHAVIOR/COMBAT MODE/SPEED - ARRAY>
*     0: <BEHAVIOR>       default - "AWARE"                   --- options, use string - "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH"
*     1: <COMBAT MODE>    default - "YELLOW"                  --- options, use string - "BLUE", "GREEN", "WHITE", "YELLOW" and "RED"
*     2: <WP SPEED>       default - "NORMAL"                  --- options, use string - "UNCHANGED", "LIMITED", "NORMAL" and "FULL"

*  7: <END WAYPOINT STATEMENT> default - ""                   --- action to execute at waypoint completion, e.g. - "car_1 = vehicle this" or "hint 'waypoint completed' "

* Examples:
[r_carTransport] call r_randomSpawn;
[["car_spawn", "carWp", "carWpEnd", 40], [(randomUnit select 0), "EAST"], [false, true], ["CARELESS","YELLOW","LIMITED"], "car_1 = vehicle this",["tr unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call pr_fnc_vehicleSpawn;

[["car1_spawn", "car1wp", "car1wpEnd", 40], [(randomUnit select 0), "EAST"], [false, true], ["CARELESS","white","full"], "car_1 = vehicle this",["unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call pr_fnc_vehicleSpawn;

[["c130Spawn", "c130WP", "c130End", 400, 5000], ["RHS_C130J", "West"], [false, true], ["CARELESS","white","full"], "c130_1 = vehicle this; ",[""], ["CARELESS","white","full"], ""] call pr_fnc_vehicleSpawn;

cond:  !(isNil "c130_1")
onAct: [c130_1] call fnc_vehEjectAction; p1 moveInCargo c130_1;

[["c130Spawn", "c130WP", "c130End", 350, 5000], ["RHS_C130J", "West"], [false, true], ["CARELESS","white","full"], "c130_1 = vehicle this; if !(isNil 'p1') then {p1 moveInCargo c130_1}; if !(isNil 'p3') then {p3 moveInCargo c130_1}; if !(isNil 'leadmedic') then {leadmedic moveInCargo c130_1}; 0 = [c130_1] spawn fnc_vehAction; ",[""], ["CARELESS","white","full"], "hint 'waypoint completed' "] call pr_fnc_vehicleSpawn;
*/

/* fn_airSpawn.sqf
usage:
[[spawnObject], vehType, height, special init,[optional upsmon parameters; ""]] call pr_fnc_vehicleSpawn;
[[                   0,                                ], [    1   ], [    2      ], [   3  ], [    4    ]] call pr_fnc_vehicleSpawn;

[Vehicle spawn position, 1st waypoint (sets direction), end waypoint, speed] // _this select 0
[Vehicle, side] // _this select 1
[setcaptive, allowdamage] // _this select 2 bool

// starting waypoints
["AWARE","YELLOW","NORMAL] // _this select 3 start waypoint; array defining waypoint behaviour, combatmode and speed
[""] // _this select 4 waypoint statement

// end waypoint
[["patrol", 900]] // _this select 5 mode, modifier
["AWARE","YELLOW","NORMAL] // _this select 6 end waypoint; array defining waypoint behaviour, combatmode and speed
[""] // _this select 7 waypoint statement
*/

//if (!isServer && hasInterface) exitWith {};
//if ((isNull aiSpawnOwner) && !(isServer)) exitWith {};
//if (!(isNull aiSpawnOwner) && !(aiSpawnOwner == player)) exitWith {};

//quick fix
if (!isServer) exitWith {};

// [["car1_spawn", "car1wp", "car1wpEnd", 30], ["O_Truck_02_transport_F", "EAST"], [false, true], ["AWARE","RED","LIMITED"], "car_1 = this", ["tr unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call pr_fnc_vehicleSpawn;

// [["car1_spawn", "car1DirTo"], ["O_Truck_02_transport_F", "EAST"]] call pr_fnc_vehicleSpawn;
// [["car1_spawn", "car1DirTo"], ["O_Truck_02_transport_F", "EAST"], [["car1", ["carCycle", "CYCLE"], 30, 0], [false, true], ["MOVE",0,0], ["SAFE","white","LIMITED"], ""] ] call pr_fnc_vehicleSpawn;

// [[c130_1, "c130WP", "c130End", 400, 2200], [false, true], ["MOVE",0,0], ["CARELESS","white","normal"], "", [], ["CARELESS","white","normal"], ""] call pr_fnc_carWaypoint;
// [[jeep1, "jeepmove", ["jeepCycle", "CYCLE"], 30, 0], [false, true], ["MOVE",0,0], ["SAFE","white","LIMITED"], ""] call pr_fnc_carWaypoint;
// [[["O_Truck_02_transport_F", "EAST"], "jeepmove", ["jeepCycle", "CYCLE"], 30, 0], [false, true], ["MOVE",0,0], ["SAFE","white","LIMITED"], ""] call pr_fnc_carWaypoint;

_this spawn {
    //[[], "pr_fnc_aiSpawner"] call BIS_fnc_MP; //quick fix

    private ["_spawnArray","_spawn","_vehDirTo","_height","_spawnPos","_dir"];
    _height = 0;
    _spawnPos = [0,0,0];
    _dir = [0,0,0];
    _side = east;

    _spawnArray = [_this, 0, [], [[]]] call BIS_fnc_param;  // _this select 0
    _spawn = [_spawnArray, 0, [0, 0, 0]] call BIS_fnc_paramIn;
    _vehDirTo = [_spawnArray, 1, [0, 0, 0]] call BIS_fnc_paramIn;
    if (count _spawnArray > 2) then { _height = _spawnArray select 2; };

    //--- Vehicle type, side
    private ["_vehArray","_vehType","_side","_randomVehArray","_random_veh"];
    _vehArray = [_this, 1, [], [[]]] call BIS_fnc_param;  // _this select 1
    _vehType = _vehArray select 0;
    if (count _vehArray > 1) then {
        _side = _vehArray select 1;
    };

    pr_vehArray = _vehArray;

    if (typeName _vehType == "ARRAY") then {
        _vehType = _vehType select 0; //pr_vehType = _vehType;
        //if (count _vehType > 1) then {
        //    _random_veh = _vehType call BIS_fnc_selectRandom;
        //    _vehType = _random_veh;
        //};
    };

    if (typeName _spawn == "STRING") then { _spawnPos = getMarkerPos _spawn; } else { _spawnPos = getPos _spawn; };
    if (typeName _vehDirTo == "STRING") then { _dir = getMarkerPos _vehDirTo; } else { _dir = getPos _vehDirTo; };

    if (typeName _side == "STRING") then {
        _side = toUpper _side;
        switch (_side) do {
            case "EAST":       { _side = EAST };
            case "WEST":       { _side = WEST };
            case "RESISTANCE": { _side = RESISTANCE };
            case "CIVILIAN":   { _side = CIVILIAN };
            default            { _side = EAST };
        };
    };
    _vehContainer = [[_spawnPos select 0, _spawnPos select 1, _height], [_spawnPos, _dir] call BIS_fnc_dirTo, _vehType, _side] call BIS_fnc_spawnVehicle;

    private ["_veh","_groupVeh"];
    _veh = _vehContainer select 0;
    _groupVeh = _vehContainer select 2;

	//quick fix
	/*
    if (name aiSpawnOwner == "hc") then {
        { hcUnits = hcUnits + [_x] } forEach units _groupVeh; publicVariable "hcUnits";
    } else {
        if (name aiSpawnOwner == "hc2") then {
            { hc2Units = hc2Units + [_x] } forEach units _groupVeh; publicVariable "hc2Units";
        } else {
            { serverUnits = serverUnits + [_x] } forEach units _groupVeh; publicVariable "serverUnits";
        };
    };*/
	
	{ serverUnits = serverUnits + [_x] } forEach units _groupVeh; publicVariable "serverUnits"; //quick fix

    //if (pr_addDragToAll) then { { _nil = [_x] spawn pr_fnc_dragAddDrag; } forEach units _groupVeh; };
    if (pr_addDragToAll) then { { [_x] remoteExec ["pr_fnc_dragAddDrag", 0, true]; } forEach units _groupVeh; };

    diag_log format ["*PR* aiSpawner: %1 has spawned %2", aiSpawnOwner, _groupVeh]; //x_groupVeh = _groupVeh;

    //--- Vehicle type, side
    private ["_vehArray","_vehType","_side","_randomVehArray","_random_veh"];
    if (count _this > 2) then {
        _wpArray = _this select 2;
        _wpArray = [_veh] + _wpArray;
        _wpArray call pr_fnc_carWaypoint;
    };
};
