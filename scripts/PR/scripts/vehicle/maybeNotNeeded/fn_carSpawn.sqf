/* 
* Author: PapaReap 
* Function name: PR_fnc_carSpawn 
* Spawns vehicle and follows waypoints 
* ver 1.0 2015-12-14 
*
*  Arguments: 
*  0: <SPAWN/WAYPOINT - ARRAY>                                --- required 
*     0: <SPAWN POSITION>                                     --- Marker or object, sets spawn position. 
*     1: <FIRST WAYPOINT>                                     --- Marker or object, sets first waypoint and vehicle direction, copied of original will create additional waypoints inside. 
*     2: <END WAYPOINT>                                       --- Marker or object, sets end waypoint. 
*     3; <SPEED OVERRIDE> default - 120                       --- optional, limits running speed of vehicle, e.g. - 25 or 55 etc... 

*  1: <VEHICLE TYPE/SIDE - ARRAY> 
*     0: <VEHICLE TYPE>   default - "O_Truck_02_transport_F"  --- if using vehicle array, precede this script with example: [r_carTransport] call r_randomSpawn; use as unit; (randomUnit select 0)
*     1: <SIDE>           default - "EAST"                    --- options, use string "east", "west", "resistance", "civilian" 

*  2: <VEHICLE STATUS>
*     0: <SET CAPTIVE>    default - false                     --- options, use boolean - true or false 
*     1: <ALLOW DAMAGE>   default - true                      --- options, use boolean - true or false 

*  3: <START WAYPOINT BEHAVIOR/COMBAT MODE/SPEED - ARRAY> 
*     0: <BEHAVIOR>       default - "AWARE"                   --- options, use string - "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH" 
*     1: <COMBAT MODE>    default - "YELLOW"                  --- options, use string - "BLUE", "GREEN", "WHITE", "YELLOW" and "RED" 
*     2: <WP SPEED>       default - "NORMAL"                  --- options, use string - "UNCHANGED", "LIMITED", "NORMAL" and "FULL" 

*  4: <START WAYPOINT STATEMENT> default - ""                 --- action to execute at waypoint completion, e.g. - "car_1 = this" or "hint 'waypoint completed' " 

*  5: <END WAYPOINT TYPE/RADIUS - ARRAY> 
*     0: <WAYPOINT TYPE>  default - ""                        --- options, use string - "MOVE", "DESTROY", "GETIN", "SAD", "JOIN", "LEADER", "GETOUT", "CYCLE", "LOAD", "UNLOAD", "TR UNLOAD", 
*                                                                                      "HOLD", "SENTRY", "GUARD", "TALK", "SCRIPTED", "SUPPORT", "GETIN NEAREST", "DISMISS" and "LOITER" 
*     1: <RADIUS>         default - 0                         --- optional, set radius, e.g. - 230, 300 etc... 

*  6: <END WAYPOINT BEHAVIOR/COMBAT MODE/SPEED - ARRAY> 
*     0: <BEHAVIOR>       default - "AWARE"                   --- options, use string - "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH" 
*     1: <COMBAT MODE>    default - "YELLOW"                  --- options, use string - "BLUE", "GREEN", "WHITE", "YELLOW" and "RED" 
*     2: <WP SPEED>       default - "NORMAL"                  --- options, use string - "UNCHANGED", "LIMITED", "NORMAL" and "FULL" 

*  7: <END WAYPOINT STATEMENT> default - ""                   --- action to execute at waypoint completion, e.g. - "car_1 = vehicle this" or "hint 'waypoint completed' " 

* Examples: 
[r_carTransport] call r_randomSpawn;
[["car_spawn", "carWp", "carWpEnd", 40], [(randomUnit select 0), "EAST"], [false, true], ["CARELESS","YELLOW","LIMITED"], "car_1 = vehicle this",["tr unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call PR_fnc_carSpawn; 

[["car1_spawn", "car1wp", "car1wpEnd", 40], [(randomUnit select 0), "EAST"], [false, true], ["CARELESS","white","full"], "car_1 = vehicle this",["unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call PR_fnc_carSpawn; 

[["c130Spawn", "c130WP", "c130End", 400, 5000], ["RHS_C130J", "West"], [false, true], ["CARELESS","white","full"], "c130_1 = vehicle this; ",[""], ["CARELESS","white","full"], ""] call PR_fnc_carSpawn; 

cond:  !(isNil "c130_1")
onAct: [c130_1] call fnc_vehEjectAction; p1 moveInCargo c130_1;

[["c130Spawn", "c130WP", "c130End", 350, 5000], ["RHS_C130J", "West"], [false, true], ["CARELESS","white","full"], "c130_1 = vehicle this; if !(isNil 'p1') then {p1 moveInCargo c130_1}; if !(isNil 'p3') then {p3 moveInCargo c130_1}; if !(isNil 'leadmedic') then {leadmedic moveInCargo c130_1}; 0 = [c130_1] spawn fnc_vehAction; ",[""], ["CARELESS","white","full"], "hint 'waypoint completed' "] call PR_fnc_carSpawn; 
*/

/* fn_airSpawn.sqf
usage: 
[[spawnObject], vehType, height, special init,[optional upsmon parameters; ""]] call PR_fnc_carSpawn; 
[[                   0,                                ], [    1   ], [    2      ], [   3  ], [    4    ]] call PR_fnc_carSpawn; 

[Vehicle spawn position, 1st waypoint (sets direction), end waypoint, speed] // _this select 0
[Vehicle, side] // _this select 1
[setcaptive, allowdamage] // _this select 2 bool

// starting waypoints 
["AWARE","YELLOW","NORMAL] // _this select 3 start waypoint; array defining waypoint behaviour, combatmode and speed 
[""] // _this select 4 waypoint statement

// end waypoint 
[["patrol", 900]] // _this select 5 mode, modifier
["AWARE","YELLOW","NORMAL] // _this select 6 end waypoint; array defining waypoint behaviour, combatmode and speed 
[""] // _this select 7 waypoint statement
*/

if (!isServer && hasInterface) exitWith {}; 
if ((isNull aiSpawnOwner) && !(isServer)) exitWith {}; 
if (!(isNull aiSpawnOwner) && !(aiSpawnOwner == player)) exitWith {}; 

_this spawn { 
    [[], "pr_fnc_aiSpawner"] call BIS_fnc_MP; 
    //[["car1_spawn", "car1wp", "car1wpEnd", 30], ["O_Truck_02_transport_F", "EAST"], [false, true], ["AWARE","RED","LIMITED"], "car_1 = this", ["tr unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call PR_fnc_carSpawn; 
    private ["_spawnArray","_spawn","_wp","_end","_height","_speed","_spawnPos","_wpPos"]; 
    _height = 0; 
    _spawnPos = [0,0,0]; 
    _wpPos = [0,0,0]; 

    _spawnArray = [_this, 0, [], [[]]] call BIS_fnc_param;  // _this select 0
    _spawn = [_spawnArray, 0, [0, 0, 0]] call BIS_fnc_paramIn; 
    _wp = [_spawnArray, 1, [0, 0, 0]] call BIS_fnc_paramIn; 
    _end = [_spawnArray, 2, [0, 0, 0]] call BIS_fnc_paramIn; 
    if (count _spawnArray > 3) then { _speed = _spawnArray select 3 }; 
    if (count _spawnArray > 4) then { _height = _spawnArray select 4 }; 

    //--- Vehicle type, side 
    private ["_vehArray","_vehType","_side","_randomVehArray","_random_veh"]; 
    _vehArray = [_this, 1, [], [[]]] call BIS_fnc_param;  // _this select 1 
    _vehType = [_vehArray, 0, "O_Truck_02_transport_F", [[]]] call BIS_fnc_paramIn; 
    _side = [_vehArray, 1, "EAST"] call BIS_fnc_paramIn; 

    if (typeName _vehType == "ARRAY") then { 
        _vehType = _vehType select 0; 
        _random_veh = _vehType call BIS_fnc_selectRandom; 
        _vehType = _random_veh; 
    }; 

    //--- SetCaptive, Allowdamage 
    private ["_vehStatus","_captive","_damage"]; 
    _vehStatus = [_this, 2, [], [[]]] call BIS_fnc_param;  // _this select 2 
    _captive = [_vehStatus, 0, false, [false]] call BIS_fnc_paramIn; 
    _damage = [_vehStatus, 1, true, [true]] call BIS_fnc_paramIn; 

    if (typeName _spawn == "STRING") then { _spawnPos = getMarkerPos _spawn; } else { _spawnPos = getPos _spawn; }; 
    if (typeName _wp == "STRING") then { _wpPos = getMarkerPos _wp; } else { _wpPos = getPos _wp; }; 

    if (typeName _side == "STRING") then { 
        _side = toUpper _side; 
        switch (_side) do { 
            case "EAST":       { _side = EAST }; 
            case "WEST":       { _side = WEST }; 
            case "RESISTANCE": { _side = RESISTANCE }; 
            case "CIVILIAN":   { _side = CIVILIAN }; 
            default            { _side = EAST }; 
        }; 
    }; 
    _vehContainer = [[_spawnPos select 0, _spawnPos select 1, _height], [_spawnPos, _wpPos] call BIS_fnc_dirTo, _vehType, _side] call BIS_fnc_spawnVehicle; 

    private ["_veh","_groupVeh"]; 
    _veh = _vehContainer select 0; 
    pr_veh = _veh; publicVariable "pr_veh"; // delete after testing 
    _groupVeh = _vehContainer select 2; 
    //{ noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _groupVeh; 
    if (name aiSpawnOwner == "hc") then { 
        { hcUnits = hcUnits + [_x] } forEach units _groupVeh; publicVariable "hcUnits"; 
    } else { 
        if (name aiSpawnOwner == "hc2") then { 
            { hc2Units = hc2Units + [_x] } forEach units _groupVeh; publicVariable "hc2Units"; 
        } else { 
            { serverUnits = serverUnits + [_x] } forEach units _groupVeh; publicVariable "serverUnits"; 
        }; 
    }; 

    diag_log format ["*PR* aiSpawner: %1 has spawned %2", aiSpawnOwner, _groupVeh]; 

    // need to look at adding this 
    // _vCrew = [_vehicle, _grp] call BIS_fnc_spawnCrew;
    // _crew = crew _vehicle;

    /*
    if (_crew == 1) then { 
        _grpCrew = createGroup West; 
        "b_helicrew_F" createUnit [getMarkerPos _spawn, _grpCrew, "this moveInTurret [_supplyHeli, [1]]", 0.6, "corporal"]; 
        "b_helicrew_F" createUnit [getMarkerPos _spawn, _grpCrew, "this moveInTurret [_supplyHeli, [2]]", 0.6, "corporal"]; 
        missionNameSpace setVariable ["supplyHeliCrew", _grpCrew]; 
        _grpCrew allowFleeing 0; 
        _grpCrew setBehaviour "COMBAT"; 
        { noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _grpCrew; 
    }; */

    //speed
    private "_dir"; 
    _dir = direction _veh; 
    _veh limitSpeed _speed; 
    _groupVeh allowFleeing 0; 
    _veh allowDamage _damage; 
    _veh setCaptive _captive; 

    //--- Waypoint: 1 / array 
    private ["_waypoints","_loop","_wpL","_wpLPos"]; 
    _waypoints = []; 
    if (typeName _wp == "STRING") then { _waypoints = _wp call fnc_collectMarkers; } else { _waypoints = _wp call fnc_collectObjectsNum; }; 

    // start waypoint behaviour
    _wpModes = [_this, 3, [], [[]]] call BIS_fnc_param;  // _this select 3 
    _wpBehaviour = [_wpModes, 0, "AWARE"] call BIS_fnc_paramIn; 
    _wpCombatMode = [_wpModes, 1, "YELLOW"] call BIS_fnc_paramIn; 
    _wpSpeed = [_wpModes, 2, "NORMAL"] call BIS_fnc_paramIn; 

    _statement = [_this, 4, ""] call BIS_fnc_param; ; // OPTIONAL - must be string! (e.g. "hint 'waypoint completed' ")  // _this select 4 "car_1 = vehicle this"
    prc_statement = _statement; 

    for [{ _loop = 0 }, { _loop < count _waypoints }, { _loop = _loop + 1 }] do { 
        {
            _wpL =  _waypoints select _loop; 
            if (typeName _wpL == "STRING") then { _wpLPos = getMarkerPos _wpL; } else { _wpLPos = getPos _wpL; }; 
            [_groupVeh, _wpLPos, [], [_wpBehaviour, _wpCombatMode, _wpSpeed], _statement] call fnc_addWaypoint; 
        } forEach _waypoints; 
        _waypoints spawn { sleep 0.01; }; 
    }; 

    pr_waypoints = _waypoints; publicVariable "pr_waypoints"; // delete after testing
    _veh limitSpeed _speed; 
    _veh flyInHeight _height; 

    //--- Waypoint: End 
    //[group, position, ["mode", modifier, completition radius, force road], ["BEHAVIOUR", "COMBATMODE", "SPEED"], "code"] call ws_fnc_addWaypoint; 
    private ["_endPos","_end","_statementEnd","_statementMode","_statementMofidier","_endDest","_statementLast"]; 
    _endPos = [0,0,0]; 

    pr_end = _end; // delete after testing
    if (typeName _end == "STRING") then { _endPos = getMarkerPos _end; } else { _endPos = getPos _end; }; 
    //_modes = ["move","destroy","getin","sad","join","leader","getout","cycle","load","unload","tr unload","hold","sentry","guard","talk","scripted","support","getin nearest","dismiss","defend","garrison","patrol","ambush"]; 

    _statementEnd = [_this, 5, ["", 0], [[]]] call BIS_fnc_param;  // _this select 5 
    _statementMode = [_statementEnd, 0, "", [""]] call BIS_fnc_paramIn; 
    _statementMofidier = [_statementEnd, 1, 0, []] call BIS_fnc_paramIn; 

    // end waypoint behaviour
    _wp2Modes = [_this, 6, [], [[]]] call BIS_fnc_param;  // _this select 6 
    _wp2Behaviour = [_wp2Modes, 0, "AWARE"] call BIS_fnc_paramIn; 
    _wp2CombatMode = [_wp2Modes, 1, "YELLOW"] call BIS_fnc_paramIn; 
    _wp2Speed = [_wp2Modes, 2, "NORMAL"] call BIS_fnc_paramIn; 

    _endDest = "Land_HelipadEmpty_F" createVehicle _endPos; 
    _statementLast = [_this, 7, ""] call BIS_fnc_param; ; // OPTIONAL - must be string! (e.g. "hint 'waypoint completed' ")  // _this select 7 

    pr_endDest = _endDest; // delete after testing
    [_groupVeh, _endDest, [_statementMode, _statementMofidier], [_wp2Behaviour, _wp2CombatMode, _wp2Speed], _statementLast] call fnc_addWaypoint; 
    pr_statementLast = _statementLast; // delete after testing

    /*
    _groupVeh spawn { 
        _groupVeh = _this select 0; 
        sleep 300; 

        if (_groupVeh != grpNull) then { 
            { noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap"; } forEach units _groupVeh; 
        }; 
    }; 
    */
    carSpawnInit = true; 
}; 

//aiSpawnOwner setVariable ["aiSpawner", false, true]; 