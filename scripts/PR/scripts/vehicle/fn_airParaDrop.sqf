// 0 = [c130] spawn fnc_vehEjectAction;
// [c130_1] call fnc_vehEjectAction;
// if (isServer) then { [[c130_1],"fnc_vehEjectAction"] call BIS_fnc_MP; };
// if (isServer) then { [[c130_1, [true, 40]],"fnc_vehEjectAction"] call BIS_fnc_MP; };

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_airParaDrop start"]; };

//waitUntil { time > 0 }; // varify this don't break
pr_script_Inst = time;

if (isNil "pr_variables") then {
    pr_variables = true;
    pr_paraDropATL = 90;         // The minimum altitude for the drop with parachute
    pr_paraJumpOpenATL = 150;    // The minimum altitude for parachute opening
    pr_paraJumpVelocity = true;  // Parachute get velocity from player or cargo
    pr_actionLUPriority = 30;    // AddAction menu position
    pr_LUSpeed = 10;             // Load and unload (not para) max speed in km/h
    pr_LUAlt = 3;                // Load and unload (not para) max height in m
};

fnc_paraCaptive = {
    // if (isServer) then { [[player, true, 30],"fnc_paraCaptive"] call BIS_fnc_MP; };
    _player = _this select 0;
    _captiveOn = _this select 1;
    _captiveOffHeight = _this select 2;
    if (_captiveOn) then {
        _player setCaptive true;
        while { (getPosATL _player) select 2 > _captiveOffHeight } do { sleep 0.2; };
        _player setCaptive false;
    };
};

fnc_saveBackpack = {
    _player = _this select 0;
    _backpackType = typeof unitBackPack _player;
    _backpack = unitBackPack _player;
    _items = getItemCargo _backpack;
    _magazines = getMagazineCargo _backpack;
    _weapons = getWeaponCargo _backpack;
    removeBackpack _player;
    _player addBackpack "B_Parachute";

    while { !(_player getVariable "PR_saveBackpack") } do { sleep 0.2; };
    removeBackpack _player;
    _player addBackpack _backpackType;
    _backpack = unitBackPack _player;
    _count = count (_items select 0) - 1; for "_i" from 0 to _count do { _backpack addItemCargoGlobal [(_items select 0) select _i, (_items select 1) select _i]; };
    _count = count (_magazines select 0) - 1; for "_i" from 0 to _count do { _backpack addMagazineCargoGlobal [(_magazines select 0) select _i, (_magazines select 1) select _i]; };
    _count = count (_weapons select 0) - 1; for "_i" from 0 to _count do { _backpack addWeaponCargoGlobal [(_weapons select 0) select _i, (_weapons select 1) select _i]; };
};

// [veh, player, true] spawn fnc_getOut;
fnc_getOut = {
    private ["_veh", "_player", "_para", "_chute",  "_backpack", "_pos", "_x_offset", "_dist", "_dist_out", "_dist_out_para", "_velocity"];
    _veh = _this select 0;
    _player = _this select 1;

    _captiveOn = false;
    _captiveOffHeight = 0;
    _captiveArray = _veh getVariable "PR_paraCaptive";
    _captiveOn = _captiveArray select 0;
    _captiveOffHeight = _captiveArray select 1;
    [_player, _captiveOn, _captiveOffHeight] spawn fnc_paraCaptive;

    _para = if (count _this > 2) then { _this select 2 } else { false };

    if ((typeOf _veh) in para_C130J) then {
        _dist_out = 7; _dist_out_para = 15;
    } else {
        _dist_out = 7; _dist_out_para = 5;
    };

    _pos = (_veh worldToModel (getPosATL _player));
    _x_offset = _pos select 0;
    _player allowDamage false;
    sleep 0.2;
    unassignVehicle _player;
    _player action ["EJECT", vehicle _player];
    sleep 0.5;

    if !(_para) then {
        _player setDir ((getDir _veh) + 180);
        _pos = ([_veh, _dist_out, ((getDir _veh) + 180 + _x_offset)] call BIS_fnc_relPos);
        _pos = [_pos select 0, _pos select 1, ((getPosATL _veh) select 2)];
        _player setPosATL _pos;
        _player allowDamage true;
    } else {
        _saveBackpack = false; //pr_saveBackpack = _saveBackpack;
        if ((aceOn) && !(unitBackpack _player isKindOf "ParachuteBase") || !(isNull (unitBackpack player))) then {
            _player setVariable ["PR_saveBackpack", false, false];
            [_player] spawn fnc_saveBackpack;
            _autoDeploy = true;
        };

        _pos = ([_veh, _dist_out_para, ((getDir _veh) + 180 + _x_offset)] call BIS_fnc_relPos);
        _pos = [_pos select 0, _pos select 1, ((getPosATL _veh) select 2)];
        _player setPosATL _pos;
        _dist = _veh distance _player;
        while { (_veh distance _player) - _dist < 20 } do { sleep 0.2; };
        if (pr_paraJumpOpenATL > 0) then { while { (getPosATL _player) select 2 > pr_paraJumpOpenATL } do { sleep 0.2; }; };

        if ((unitBackpack _player isKindOf "ParachuteBase") && !((vehicle _player) isKindOf "ParachuteBase")) then {
            _player action ["OpenParachute", _player];
        } else {
            if (!(unitBackpack _player isKindOf "ParachuteBase") && !((vehicle _player) isKindOf "ParachuteBase")) then {
                _chute = createVehicle ["Steerable_Parachute_F", position _player, [], 0, "CAN_COLLIDE"];
                _chute AttachTo [_player, [0,0,0]];
                detach _chute;
                _velocity = velocity _player;
                _player moveInDriver _chute;
                if (pr_paraJumpVelocity) then { _chute setVelocity _velocity; };
            };
        };
        _player allowDamage true;

        while { (!(isTouchingGround _player) && !(surfaceIsWater position _player)) } do { sleep 0.2; };
        _player setVariable ["PR_saveBackpack", true, false];
    };
};

// if (isServer) then { [[c130_1],"fnc_vehEjectAction"] call BIS_fnc_MP; };
// if (isServer) then { [[c130_1, [true, 30]],"fnc_vehEjectAction"] call BIS_fnc_MP; };
fnc_vehEjectAction = {
    _veh = _this select 0;
    _veh setVariable ["PR_paraCaptive", [false, 0], true];
    if (count _this > 1) then {
        _captiveArray = _this select 1;
        _captiveOn = _captiveArray select 0;
        _captiveOffHeight = _captiveArray select 1;
        _veh setVariable ["PR_paraCaptive", [_captiveOn, _captiveOffHeight], true];
    };

    _veh_type = (typeOf _veh);
    //if ((_veh_type in para_C130J) || (_veh_type in para_MOHAWK)) then {
    if (_veh_type in para_C130J) then {
        _vsupported = true;
        _veh addAction [ "<t color=""#ff0000"">Get out on the side of ramp</t>", { [_this select 0, _this select 1, false] call fnc_getOut; }, [], pr_actionLUPriority, false, true, "", "('cargo' in (assignedVehicleRole _this)) && (vehicle _this == _target) && (abs (speed _target) <= pr_LUSpeed) && (((getPos _target) select 2) <= pr_LUAlt) && (_target animationPhase 'ramp_bottom' > 0.43) && (_target getVariable 'usable_ramp')" ];

        _veh addAction [ "<img image='scripts\IgiLoad\images\unload_para.paa' /><t color=""#b200ff""> Eject</t>", { [_this select 0, _this select 1, true] call fnc_getOut; }, [], pr_actionLUPriority, false, true, "", "('cargo' in (assignedVehicleRole _this)) && (vehicle _this == _target) && (((getPosATL _target) select 2) >= pr_paraDropATL) && (_target animationPhase 'ramp_bottom' > 0.9) && (_target getVariable 'usable_ramp')" ];
    } else {
        if (_veh_type in para_UH1Y) then {
            //_veh addAction [ "<t color=""#ff0000"">Get out on the side of ramp</t>", { [_this select 0, _this select 1, false] call fnc_getOut; }, [], pr_actionLUPriority, false, true, "", "('cargo' in (assignedVehicleRole _this)) && (vehicle _this == _target) && (abs (speed _target) <= pr_LUSpeed) && (((getPos _target) select 2) <= pr_LUAlt) && (_target animationPhase 'ramp_bottom' > 0.43) && (_target getVariable 'usable_ramp')" ];

            _veh addAction [ "<img image='scripts\IgiLoad\images\unload_para.paa' /><t color=""#b200ff""> Eject</t>", { [_this select 0, _this select 1, true] call fnc_getOut; }, [], pr_actionLUPriority, false, true, "", "('cargo' in (assignedVehicleRole _this)) && (vehicle _this == _target) && (((getPosATL _target) select 2) >= pr_paraDropATL)" ];
        } else {
            if (_veh_type in para_BlackFish) then {
                _vsupported = true;
                _veh addAction [ "<img image='scripts\IgiLoad\images\unload_para.paa' /><t color=""#b200ff""> Eject</t>", { [_this select 0, _this select 1, true] call fnc_getOut; }, [], pr_actionLUPriority, false, true, "", "('cargo' in (assignedVehicleRole _this)) && (vehicle _this == _target) && (((getPosATL _target) select 2) >= pr_paraDropATL) && (_target doorPhase 'Door_1_source' > 0.9)" ];
            };
        };
    };
};

//bf_1 animateDoor ["Door_1_source", 1]
//bf_1 doorPhase "Door_1_source"

airParaDropInit = true;

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_airParaDrop complete"]; };
