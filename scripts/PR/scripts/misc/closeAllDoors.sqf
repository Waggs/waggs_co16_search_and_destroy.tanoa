//mr_book.PXS.Pvt Door Closing script.
//Closes all doors within a certain radius
//Place a Game Logic in the centre of the desired area.
//On Activation: nul = [this,1000] execVM "closeAllDoors.sqf";
//1st parameter is a Game Logic at the location where you want to close the doors 
//2nd parameter is the radius. Don't recommend using radius over 1000.

private ["_y","_houses","_zeroes","_ones"]; 
if (!isServer) exitWith {}; 

_houses = position (_this select 0) nearObjects ["house", (_this select 1)]; 
_zeroes = ["dvere","dvere1l","dvere1r","dvere2l","dvere2r","dvere_spodni_r","dvere_spodni_l","dvere_vrchni","vrata1","vrata2","vratal1","vratar1","vratal2","vratar2","vratal3","vratar3"]; 
_ones = ["door","door_1_1","door_1_2","door_2_1","door_2_2","dvere1","dvere2","dvere3","dvere4","dvere5","dvere6","dvere7","dvere8","dvere9","dvere10","dvere11","dvere12","dvere13","dvere14","doorl","doorr","door_01","door01_a","door_02","door02_a","door_03","door_04","door_05","door_06","door_1a","door_1","door_2"]; 
{ 
    _y = _x; 
    {_y animate [format ["%1", _x], 0]} forEach _zeroes; 
    if ((worldName=="Utes") or (worldName=="Chernarus")) then { 
        {_y animate [format ["%1", _x], 0]} forEach _ones; 
    } else { 
        {_y animate [format ["%1", _x], 1]} forEach _ones; 
    }; 
    sleep 0.0001; 
} forEach _houses; 

/*
0 = [getpos this,300] spawn {
	sleep 1;
	{
		private "_b"; 
		_b = _x;
		for "_i" from 0 to 7 do {
			_b animate ["dvere" + str _i,1]
		};
	} foreach ((_this select 0) nearobjects (_this select 1))
}
*/