//--- _nil = [thisTrigger, 1500, "east", true] execVM "scripts\PR\scripts\misc\killNCleanup.sqf"; 
//--- _nil = [thisTrigger, 1500, "civilian", true] execVM "scripts\PR\scripts\misc\killNCleanup.sqf"; 
//--- _nil = [thisTrigger, 1500, "all", true] execVM "scripts\PR\scripts\misc\killNCleanup.sqf"; 

if !(isServer) exitWith {}; 

_center = _this select 0; 
_range = [_this, 1, 1000, [1000]] call BIS_fnc_param; 
_side = [_this, 2, "", ["east"]] call BIS_fnc_param; 
_remove = [_this, 3, true, [true]] call BIS_fnc_param; 

_side = toUpper _side; 

if (_side == "ALL") then { 
    { 
        if ((_x distance _center < _range) && (_x != player)) then { 
            _x setDamage 1; 
        }; 
    } count allUnits; 
} else { 

    switch (_side) do { 
        case "EAST":       {_side = EAST}; 
        case "WEST":       {_side = WEST}; 
        case "RESISTANCE": {_side = RESISTANCE}; 
        case "CIVILIAN":   {_side = CIVILIAN}; 
        default            {_side = EAST}; 
    }; 

    { 
        if (((side _x) == _side) && (_x distance _center < _range) && (_x != player)) then { 
            _x setDamage 1; 
        }; 
    } count allUnits; 
}; 

if (_remove) then { 
    { deleteVehicle _x; } forEach allDead; 

    { 
        if ((count (units _x)) == 0) then { 
            deleteGroup _x; 
            _x = grpNull; 
            _x = nil; 
        }; 
    } foreach allGroups; 
}; 
