/* fn_dragActionKnockOut.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragActionKnockOut

*  Knockout or kill unit action
*/

_unit = _this select 0;
_player = _this select 1; pr_koDragger = _player;
_koID = (_this select 2);

_koAction = _unit getVariable "pr_koAction";
_player switchMove "AwopPercMstpSgthWnonDnon_end";
sleep 0.5;

if !(_koAction) then {
    _unit setDamage 1;
    _unit removeAction _koID;
} else {
    [_unit, 30 + random 120, _player] remoteExec ["pr_fnc_dragKnockOut", 0, true];
};
