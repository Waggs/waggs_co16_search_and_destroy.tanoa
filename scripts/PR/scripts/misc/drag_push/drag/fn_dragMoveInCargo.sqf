/* fn_dragMoveInCargo.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragMoveInCargo

*  Load dragged unit in vehicle cargo
*/

_unit = _this select 0;
_cargoIndex = _this select 1;
_vehicle = _this select 2;
_unit assignAsCargoIndex [_vehicle, _cargoIndex];
_unit moveInCargo _vehicle;
_unit spawn {
    _unit = _this;
    sleep 0.1;
    _unit switchMove "KIA_Driver_low01";
};
