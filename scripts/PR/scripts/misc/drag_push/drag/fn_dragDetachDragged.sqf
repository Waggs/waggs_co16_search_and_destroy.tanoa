/* fn_dragDetachDragged.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragDetachDragged

*  Detach dragged unit
*/

_unit = (_this select 0);
_player = (_this select 1);
_unitID = (_this select 2);

_id = format ["h8EF%1", _unitID];
0 = [_id, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
 
sleep 0.05;
_dirTo = [_unit, _player] call BIS_fnc_dirTo;


_unit switchMove "AinjPpneMstpSnonWrflDb_release";
_unit setDir (_dirTo - 180);
