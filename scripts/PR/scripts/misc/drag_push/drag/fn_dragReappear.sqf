/* fn_dragReappear.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragReappear

*  Temporary fix to show dead after loading in vehicle
*/

_unit = _this select 0;
if (isServer) then { _unit hideObjectGlobal false; };
