if !(isServer) exitWith {}; 

if (isNil "trig1") then { trig1 = "false"; }; 
if (isNil "trig2") then { trig2 = "false"; }; 
if (isNil "trig3") then { trig3 = "false"; }; 
if (isNil "trig4") then { trig4 = "false"; }; 
if (isNil "trig5") then { trig5 = "false"; }; 
if (isNil "trig6") then { trig6 = "false"; }; 
if (isNil "trig7") then { trig7 = "false"; }; 
if (isNil "trig8") then { trig8 = "false"; }; 
if (isNil "trig9") then { trig9 = "false"; }; 
if (isNil "trig10") then { trig10 = "false"; }; 
if (isNil "trig11") then { trig11 = "false"; }; 
if (isNil "trig12") then { trig12 = "false"; }; 
if (isNil "trig13") then { trig13 = "false"; }; 
if (isNil "trig14") then { trig14 = "false"; }; 
if (isNil "trig15") then { trig15 = "false"; }; 
if (isNil "trig16") then { trig16 = "false"; }; 
if (isNil "trig17") then { trig17 = "false"; }; 
if (isNil "trig18") then { trig18 = "false"; }; 
if (isNil "trig19") then { trig19 = "false"; }; 
if (isNil "trig20") then { trig20 = "false"; }; 
if (isNil "trig21") then { trig21 = "false"; }; 
if (isNil "trig22") then { trig22 = "false"; }; 
if (isNil "trig23") then { trig23 = "false"; }; 
if (isNil "trig24") then { trig24 = "false"; }; 
if (isNil "trig25") then { trig25 = "false"; }; 
if (isNil "trig26") then { trig26 = "false"; }; 
if (isNil "trig27") then { trig27 = "false"; }; 
if (isNil "trig28") then { trig28 = "false"; }; 
if (isNil "trig29") then { trig29 = "false"; }; 
if (isNil "trig30") then { trig30 = "false"; }; 
if (isNil "trig31") then { trig31 = "false"; };
if (isNil "trig32") then { trig32 = "false"; };
if (isNil "trig33") then { trig33 = "false"; };
if (isNil "trig34") then { trig34 = "false"; };
if (isNil "trig35") then { trig35 = "false"; };
if (isNil "trig36") then { trig36 = "false"; };
if (isNil "trig37") then { trig37 = "false"; };
if (isNil "trig38") then { trig38 = "false"; };
if (isNil "trig39") then { trig39 = "false"; };
if (isNil "trig40") then { trig40 = "false"; };

if ((isDedicated) && (aliveOn) && (PersistTasks)) then { 
    waitUntil { (!isnil "alive_sys_playertags") }; 

    if (!isNil {["trig1_x"] call ALiVE_fnc_getData}) then { trig1 = ["trig1_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig2_x"] call ALiVE_fnc_getData}) then { trig2 = ["trig2_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig3_x"] call ALiVE_fnc_getData}) then { trig3 = ["trig3_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig4_x"] call ALiVE_fnc_getData}) then { trig4 = ["trig4_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig5_x"] call ALiVE_fnc_getData}) then { trig5 = ["trig5_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig6_x"] call ALiVE_fnc_getData}) then { trig6 = ["trig6_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig7_x"] call ALiVE_fnc_getData}) then { trig7 = ["trig7_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig8_x"] call ALiVE_fnc_getData}) then { trig8 = ["trig8_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig9_x"] call ALiVE_fnc_getData}) then { trig9 = ["trig9_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig10_x"] call ALiVE_fnc_getData}) then { trig10 = ["trig10_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig11_x"] call ALiVE_fnc_getData}) then { trig11 = ["trig11_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig12_x"] call ALiVE_fnc_getData}) then { trig12 = ["trig12_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig13_x"] call ALiVE_fnc_getData}) then { trig13 = ["trig13_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig14_x"] call ALiVE_fnc_getData}) then { trig14 = ["trig14_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig15_x"] call ALiVE_fnc_getData}) then { trig15 = ["trig15_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig16_x"] call ALiVE_fnc_getData}) then { trig16 = ["trig16_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig17_x"] call ALiVE_fnc_getData}) then { trig17 = ["trig17_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig18_x"] call ALiVE_fnc_getData}) then { trig18 = ["trig18_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig19_x"] call ALiVE_fnc_getData}) then { trig19 = ["trig19_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig20_x"] call ALiVE_fnc_getData}) then { trig20 = ["trig20_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig21_x"] call ALiVE_fnc_getData}) then { trig21 = ["trig21_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig22_x"] call ALiVE_fnc_getData}) then { trig22 = ["trig22_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig23_x"] call ALiVE_fnc_getData}) then { trig23 = ["trig23_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig24_x"] call ALiVE_fnc_getData}) then { trig24 = ["trig24_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig25_x"] call ALiVE_fnc_getData}) then { trig25 = ["trig25_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig26_x"] call ALiVE_fnc_getData}) then { trig26 = ["trig26_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig27_x"] call ALiVE_fnc_getData}) then { trig27 = ["trig27_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig28_x"] call ALiVE_fnc_getData}) then { trig28 = ["trig28_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig29_x"] call ALiVE_fnc_getData}) then { trig29 = ["trig29_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig30_x"] call ALiVE_fnc_getData}) then { trig30 = ["trig30_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig31_x"] call ALiVE_fnc_getData}) then { trig31 = ["trig31_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig32_x"] call ALiVE_fnc_getData}) then { trig32 = ["trig32_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig33_x"] call ALiVE_fnc_getData}) then { trig33 = ["trig33_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig34_x"] call ALiVE_fnc_getData}) then { trig34 = ["trig34_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig35_x"] call ALiVE_fnc_getData}) then { trig35 = ["trig35_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig36_x"] call ALiVE_fnc_getData}) then { trig36 = ["trig36_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig37_x"] call ALiVE_fnc_getData}) then { trig37 = ["trig37_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig38_x"] call ALiVE_fnc_getData}) then { trig38 = ["trig38_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig39_x"] call ALiVE_fnc_getData}) then { trig39 = ["trig39_x"] call ALiVE_fnc_getData; }; 
    if (!isNil {["trig40_x"] call ALiVE_fnc_getData}) then { trig40 = ["trig40_x"] call ALiVE_fnc_getData; }; 

    publicVariable "trig1"; diag_log format ["*PR* trig1, %1", trig1]; 
    publicVariable "trig2"; diag_log format ["*PR* trig2, %1", trig2]; 
    publicVariable "trig3"; diag_log format ["*PR* trig3, %1", trig3]; 
    publicVariable "trig4"; diag_log format ["*PR* trig4, %1", trig4]; 
    publicVariable "trig5"; diag_log format ["*PR* trig5, %1", trig5]; 
    publicVariable "trig6"; diag_log format ["*PR* trig6, %1", trig6]; 
    publicVariable "trig7"; diag_log format ["*PR* trig7, %1", trig7]; 
    publicVariable "trig8"; diag_log format ["*PR* trig8, %1", trig8]; 
    publicVariable "trig9"; diag_log format ["*PR* trig9, %1", trig9]; 
    publicVariable "trig10"; diag_log format ["*PR* trig10, %1", trig10]; 
    publicVariable "trig11"; diag_log format ["*PR* trig11, %1", trig11]; 
    publicVariable "trig12"; diag_log format ["*PR* trig12, %1", trig12]; 
    publicVariable "trig13"; diag_log format ["*PR* trig13, %1", trig13]; 
    publicVariable "trig14"; diag_log format ["*PR* trig14, %1", trig14]; 
    publicVariable "trig15"; diag_log format ["*PR* trig15, %1", trig15]; 
    publicVariable "trig16"; diag_log format ["*PR* trig16, %1", trig16]; 
    publicVariable "trig17"; diag_log format ["*PR* trig17, %1", trig17]; 
    publicVariable "trig18"; diag_log format ["*PR* trig18, %1", trig18]; 
    publicVariable "trig19"; diag_log format ["*PR* trig19, %1", trig19]; 
    publicVariable "trig20"; diag_log format ["*PR* trig20, %1", trig20]; 
    publicVariable "trig21"; diag_log format ["*PR* trig21, %1", trig21]; 
    publicVariable "trig22"; diag_log format ["*PR* trig22, %1", trig22]; 
    publicVariable "trig23"; diag_log format ["*PR* trig23, %1", trig23]; 
    publicVariable "trig24"; diag_log format ["*PR* trig24, %1", trig24]; 
    publicVariable "trig25"; diag_log format ["*PR* trig25, %1", trig25]; 
    publicVariable "trig26"; diag_log format ["*PR* trig26, %1", trig26]; 
    publicVariable "trig27"; diag_log format ["*PR* trig27, %1", trig27]; 
    publicVariable "trig28"; diag_log format ["*PR* trig28, %1", trig28]; 
    publicVariable "trig29"; diag_log format ["*PR* trig29, %1", trig29]; 
    publicVariable "trig30"; diag_log format ["*PR* trig30, %1", trig30]; 
    publicVariable "trig31"; diag_log format ["*PR* trig31, %1", trig31]; 
    publicVariable "trig32"; diag_log format ["*PR* trig32, %1", trig32]; 
    publicVariable "trig33"; diag_log format ["*PR* trig33, %1", trig33]; 
    publicVariable "trig34"; diag_log format ["*PR* trig34, %1", trig34]; 
    publicVariable "trig35"; diag_log format ["*PR* trig35, %1", trig35]; 
    publicVariable "trig36"; diag_log format ["*PR* trig36, %1", trig36]; 
    publicVariable "trig37"; diag_log format ["*PR* trig37, %1", trig37]; 
    publicVariable "trig38"; diag_log format ["*PR* trig38, %1", trig38]; 
    publicVariable "trig39"; diag_log format ["*PR* trig39, %1", trig39]; 
    publicVariable "trig40"; diag_log format ["*PR* trig40, %1", trig40]; 

} else { 
    publicVariable "trig1"; diag_log format ["*PR* trig1, %1", trig1]; 
    publicVariable "trig2"; diag_log format ["*PR* trig2, %1", trig2]; 
    publicVariable "trig3"; diag_log format ["*PR* trig3, %1", trig3]; 
    publicVariable "trig4"; diag_log format ["*PR* trig4, %1", trig4]; 
    publicVariable "trig5"; diag_log format ["*PR* trig5, %1", trig5]; 
    publicVariable "trig6"; diag_log format ["*PR* trig6, %1", trig6]; 
    publicVariable "trig7"; diag_log format ["*PR* trig7, %1", trig7]; 
    publicVariable "trig8"; diag_log format ["*PR* trig8, %1", trig8]; 
    publicVariable "trig9"; diag_log format ["*PR* trig9, %1", trig9]; 
    publicVariable "trig10"; diag_log format ["*PR* trig10, %1", trig10]; 
    publicVariable "trig11"; diag_log format ["*PR* trig11, %1", trig11]; 
    publicVariable "trig12"; diag_log format ["*PR* trig12, %1", trig12]; 
    publicVariable "trig13"; diag_log format ["*PR* trig13, %1", trig13]; 
    publicVariable "trig14"; diag_log format ["*PR* trig14, %1", trig14]; 
    publicVariable "trig15"; diag_log format ["*PR* trig15, %1", trig15]; 
    publicVariable "trig16"; diag_log format ["*PR* trig16, %1", trig16]; 
    publicVariable "trig17"; diag_log format ["*PR* trig17, %1", trig17]; 
    publicVariable "trig18"; diag_log format ["*PR* trig18, %1", trig18]; 
    publicVariable "trig19"; diag_log format ["*PR* trig19, %1", trig19]; 
    publicVariable "trig20"; diag_log format ["*PR* trig20, %1", trig20]; 
    publicVariable "trig21"; diag_log format ["*PR* trig21, %1", trig21]; 
    publicVariable "trig22"; diag_log format ["*PR* trig22, %1", trig22]; 
    publicVariable "trig23"; diag_log format ["*PR* trig23, %1", trig23]; 
    publicVariable "trig24"; diag_log format ["*PR* trig24, %1", trig24]; 
    publicVariable "trig25"; diag_log format ["*PR* trig25, %1", trig25]; 
    publicVariable "trig26"; diag_log format ["*PR* trig26, %1", trig26]; 
    publicVariable "trig27"; diag_log format ["*PR* trig27, %1", trig27]; 
    publicVariable "trig28"; diag_log format ["*PR* trig28, %1", trig28]; 
    publicVariable "trig29"; diag_log format ["*PR* trig29, %1", trig29]; 
    publicVariable "trig30"; diag_log format ["*PR* trig30, %1", trig30]; 
    publicVariable "trig31"; diag_log format ["*PR* trig31, %1", trig31]; 
    publicVariable "trig32"; diag_log format ["*PR* trig32, %1", trig32]; 
    publicVariable "trig33"; diag_log format ["*PR* trig33, %1", trig33]; 
    publicVariable "trig34"; diag_log format ["*PR* trig34, %1", trig34]; 
    publicVariable "trig35"; diag_log format ["*PR* trig35, %1", trig35]; 
    publicVariable "trig36"; diag_log format ["*PR* trig36, %1", trig36]; 
    publicVariable "trig37"; diag_log format ["*PR* trig37, %1", trig37]; 
    publicVariable "trig38"; diag_log format ["*PR* trig38, %1", trig38]; 
    publicVariable "trig39"; diag_log format ["*PR* trig39, %1", trig39]; 
    publicVariable "trig40"; diag_log format ["*PR* trig40, %1", trig40]; 
}; 

if (isNil "trigHold") then { trigHold = false; publicVariable "trigHold"; }; 
