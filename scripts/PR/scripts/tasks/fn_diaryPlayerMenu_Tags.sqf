
if ((isDedicated) && !( player == player )) exitWith {}; 

_subject = player createDiarySubject ["PM", "Player Menu System"]; 
_diary = player createDiaryRecord ["PM", ["Credits", "
<font face='PuristaMedium' size=14 color='#ffffff'>Original Mod Created by: </font><font face='PuristaMedium' size=18 color='#8E8E8E'>  baermitumlaut</font><br/>
<font face='PuristaMedium' size=14 color='#ffffff'>Modified Script Version by: </font><font face='PuristaMedium' size=18 color='#8E8E8E'>PapaReap</font><br/><br/>

<font face='PuristaMedium' size=18 color='#8E8E8E'>Credits directly from Mod Author</font><br/>
Credit where credit's due:<br/>
This addon was heavily inspired by ShackTac's Fireteam HUD, my original intention was to create a more subtle and limited replacement for it - I don't think somebody has any perception of what is going on 60m behind himself. I did not use any code from ShackTac's mod, it's an entirely new creation.<br/><br/>

Also thanks to Gruppe W for accepting my mod early in their modpack and for their constant feedback. It has helped and motivated me a lot.<br/><br/>

<font face='PuristaMedium' size=18 color='#8E8E8E'>Credits from PapaReap</font><br/>
While I did convert and modify several scripts to our units liking, I give full credit to baermitumlaut for the work he put into creating the main foundation of this hud system.
"]];

_diary = player createDiaryRecord ["PM", ["Key Binds", "
<br/><br/>
<font face='PuristaMedium' size=18 color='#bcb5b5'>Open Hud Settings:</font><font face='PuristaMedium' size=16 color='#ffffff'>  Hold -   'Shift' + 'Ctrl' + 'b'</font><br/>
  Options:<br/>
    Select role<br/>
    Show Compass<br/>
    Only Show Squad members<br/>
    Show Player Position<br/>
    Automatically Fade Out<br/>
    HUD mode - 180, Midway or 360 degrees<br/>
    HUD Size Slider<br/><br/>

<font face='PuristaMedium' size=18 color='#bcb5b5'>Hide Hud:</font><font face='PuristaMedium' size=16 color='#ffffff'>  Hold -   'Shift' + 'Alt' + 'b'</font><br/><br/>

<font face='PuristaMedium' size=18 color='#bcb5b5'>Unfade Hud:</font><font face='PuristaMedium' size=16 color='#ffffff'>  Hold -   'Shift' + 'Space'</font>
"]];

_diary = player createDiaryRecord ["PM", ["Description", "
<font face='PuristaMedium' size=30 color='#014EE3'>Modified BlueHud System</font><br/><br/>
BlueHud is a new HUD enhancement for Arma 3. Its aim is to provide a better perception of teammates in your vicinity without giving you an infantry radar that provides a super human awareness of your surroundings.<br/><br/>
FEATURES<br/>
    Provides realistic periphal vision of teammates.<br/>
    Completly customizable.<br/>
    Subtle, immersive and well integrated into Arma's UI.<br/>
    11 different infantry roles can be displayed.<br/><br/><br/>
<font face='PuristaMedium' size=13 color='#bcb5b5'>Hud will show units on players side if within 20 meters of player.</font>
"]];

true