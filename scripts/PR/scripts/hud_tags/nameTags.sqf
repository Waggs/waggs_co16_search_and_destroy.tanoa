
//--- credit to MosesUK & ShackTac stgi for inspration 
player setVariable ["myTags", "true", false]; 

//--- Alt + Z = Toggle Tags  
pr_tagKey = 44; // Z 
pr_tagShift = 0; 
pr_tagCtrl = 0; 
pr_tagAlt = 1; 

_tagKey = pr_tagKey; 
if (_tagKey == 0) then { _tagKey = 44 }; 
_tagShift = pr_tagShift == 1; 
_tagCtrl  = pr_tagCtrl == 1; 
_tagAlt   = pr_tagAlt == 1; 

prTagKeys = [_tagKey, [_tagShift, _tagCtrl, _tagAlt]]; 
prTagKeyHandler = { 
    prTagHandled = false; 
    if ((prTagKeys select 0) == (_this select 1)) then { 
        if ((prTagKeys select 1) isEqualTo [(_this select 2), (_this select 3), (_this select 4)]) then { 
            if (player getVariable "myTags" == "true") then { 
                player setVariable ["myTags", "false", false]; 
            } else { 
                player setVariable ["myTags", "true", false]; 
            }; 
            prTagHandled = true; 
        }; 
    }; 
    prTagHandled 
}; 

waitUntil { !isNull(findDisplay 46) }; 
(findDisplay 46) displayAddEventHandler ["KeyDown", "_this call prTagKeyHandler;"]; 


pr_red = [1, 0, 0, 1]; 
pr_green = [0, 1, 0, 1]; 
pr_blue = [0, 0, 1, 1]; 
pr_white = [1, 1, 1, 1]; 
pr_yellow = [0.85, 0.85, 0, 1]; 
pr_orange = [0.85, 0.4, 0, 1]; 

pr_ltGreen = [0.4, 0.8, 0.2, 1]; 
pr_ltBlue = [0, 0.6, 1, 1]; 
pr_ltGray = [0.7, 0.7, 0.7, 1]; 

pr_clear = [0,0,0,0]; 

pr_tooClose = 3; 
pr_maxDist = 50; 
pr_edgeStep = 50; 
pr_edgeFade = pr_maxDist + pr_edgeStep; 

pr_alphaNight = 0.15; 
pr_alphaDay = 0.15; // Extra alpha on from night 
pr_alphaClamp = 0; // Updated every frame 

pr_nameDist = 30; 

pr_tagLeader = ["B_Soldier_SL_F","B_Soldier_TL_F"]; 
pr_tagMedic = ["B_medic_F"]; 
pr_tagRepair = ["B_soldier_repair_F","B_engineer_F"]; 

pr_getColor = { //_color = [_unit, _distance, _sameGroup && _isUsingTeams] call pr_getColor; 
    private ["_unit", "_distance", "_hasTeams"]; 
    _unit = _this select 0; 
    _distance = _this select 1; 
    _hasTeams = _this select 2; 

    if ((vehicle(_unit) != vehicle(player)) && (_distance < pr_tooClose)) exitWith { 
        pr_orange; 
    }; 

    private "_color"; 
    if (_hasTeams) then { 
        //if (_unit in (units player)) then { 
            _color = (switch (_unit getVariable ["BlueHud_TeamColor", "MAIN"]) do { 
                //case "MAIN": { pr_ltGray }; 
                case "MAIN": { pr_white }; 
                case "RED": { pr_red }; 
                case "BLUE": { pr_ltBlue }; 
                case "GREEN": { pr_green }; 
                case "YELLOW": { pr_yellow }; 
                default { pr_green } 
            }); 
        //}; 
    } else { 
        _color = pr_clear; 
    }; 

    if (_distance < pr_maxDist) then { 
        _color set [3, pr_alphaClamp]; 
    } else { 
        private "_alpha"; 
        _alpha = (0 max (pr_edgeFade - _distance)) / pr_edgeStep; 
        _alpha = 1 min _alpha; 
        _color set [3, _alpha * pr_alphaClamp]; 
    }; 
    _color; 
}; 

TAGS = addMissionEventHandler ["Draw3D", { 
    private "_units"; 
    _units = playableUnits; 
    if (count(_units) == 1) exitWith {}; 

    // Adjust the maximum alpha based on day/night 
    pr_alphaClamp = pr_alphaNight + sunOrMoon * pr_alphaDay; 
    pr_orange set [3, pr_alphaClamp + 0.2]; 

    private "_isUsingTeams"; 
    _isUsingTeams = BlueHUD_allowTeamColors; 

    { 
        private "_unit"; 
        _unit = _x; 
        if (_unit != player) then { 
            private ["_icon_pos", "_screen_pos"]; 
            _icon_pos = ASLtoATL(visiblePositionASL(vehicle(_unit))); 
            private "_height_adjust"; 
            _height_adjust = 0.2; 
            if (vehicle(_unit) == _unit) then { 
                _height_adjust = _height_adjust + (_unit selectionPosition "pelvis" select 2); 
            } else { 
                _height_adjust = _height_adjust + 0.7; 
            }; 
            _icon_pos set [2, (_icon_pos select 2) + _height_adjust]; 

            _screen_pos = worldToScreen(_icon_pos); 
            if (count(_screen_pos) == 2) then { 
                _distance = _icon_pos distance vehicle(player); 
                _color = [_unit, _distance, _isUsingTeams] call pr_getColor; 

                private ["_texture", "_angle"]; 
                _texture = ""; 
                _angle = 0; 

                private "_name"; 
                _name = ""; 
                // if within half the min distance, show their name 
                if (_distance < pr_nameDist) then { 
                    if (((_screen_pos select 0) > (SafeZoneX + SafeZoneW/5)) && 
                        ((_screen_pos select 0) < (SafeZoneX + SafeZoneW*4/5)) && 
                        ((_screen_pos select 1) > (SafeZoneY + SafeZoneH/5)) && 
                        ((_screen_pos select 1) < (SafeZoneY + SafeZoneH*4/5))) then { 
                        _name = name _unit; 
                    }; 
                }; 

                _width = 1.3; 
                _height = 1.3; 

                if ((leader (group _unit) == _unit) && !((typeof _unit in pr_tagMedic) || (typeof _unit in pr_tagRepair))) then { 
                    _texture = '\A3\ui_f\data\igui\cfg\cursors\leader_ca.paa'; 
                } else {  
                    if ((leader (group _unit) == _unit) && (typeof _unit in pr_tagLeader)) then { 
                        _texture = '\A3\ui_f\data\igui\cfg\cursors\leader_ca.paa'; 
                    } else {  
                        if (typeof _unit in pr_tagMedic) then { 
                            _texture = '\A3\ui_f\data\igui\cfg\cursors\unitHealer_ca.paa'; 
                        } else {  
                            if (typeof _unit in pr_tagRepair) then { 
                                _texture = '\A3\ui_f\data\igui\cfg\cursors\iconRepairAt_ca.paa'; 
                            } else { 
                                _texture = '\A3\ui_f\data\igui\cfg\cursors\select_ca.paa'; 
                                _width = 0; _height = 0; 
                            }; 
                        }; 
                    }; 
                }; 

                _cursorBoost = 1 - ((player distance _x) / 15); 
                if (player distance _x < 15) then { 
                    if (cursorTarget == _x) then { 
                        _color set [3, (pr_alphaClamp + _cursorBoost)]; 
                    }; 
                }; 
                _text = 'right'; 
                _pos = [0,0,0]; 
                _dist = (player distance _x) / 15; 
                if !(vehicle(_unit) == _unit) then { 
                    if (vehicle(player) == vehicle(_unit)) then { 
                        _pos = [ visiblePosition _x select 0, visiblePosition _x select 1, ((visiblePosition _x select 2) + .8) ]; _text = 'center'; 
                    } else { 
                        _pos = [ visiblePosition _x select 0, visiblePosition _x select 1, ((visiblePosition _x select 2) + 2) ]; 
                    }; 
                } else { 
                    _pos = [ visiblePosition _x select 0, visiblePosition _x select 1, ((visiblePosition _x select 2) + ((_x modelToWorld ( _x selectionPosition 'pelvis' )) select 2) + 0.12 + _dist / 1.5) ]; 
                }; 

                if (player getVariable "myTags" == "false") then { _color set [3, 0]; }; 

                //if ((_name != "") || _sameGroup) then { 
                if !(lineIntersects [ eyePos player, aimPos _unit, player, _unit]) then { 
                    if (_name != "") then { 
                        drawIcon3D [ 
                            _texture,  // texture 
                            _color,    // color 
                            _pos,      // position 
                            _width,    // width 
                            _height,   // height 
                            _angle,    // angle 
                            _name,     // text 
                            false,     // shadow 
                            0.035,     // textSize 
                            'TahomaB', // font i.e. PuristaMedium, TahomaB 
                            _text      // textAlign - String - (optional) - left, center, right... 
                                       // drawSideArrows -  Boolean - (optional) - Draw arrows at edge of screen when icon moves off screen. 
                        ]; 
                    }; 
                }; 
            }; 
        }; 
    } foreach (_units); 
}]; 

TAGS 