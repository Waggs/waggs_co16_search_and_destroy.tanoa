/* natoBox_ammo.sqf
*  Add the following to the init field of an ammo box in the editor:  _nil = this execVM "scripts\PR\loadouts\box\natoBox_ammo.sqf";
*  When adding items, make sure they are in the correct sections (weapons/ammo/items..), and make sure the last item in each group doesn't have a comma at the end of the line
*  If spawned box use - [[namebox], "fnc_natoBoxAmmo"] call BIS_fnc_MP;
*/

waitUntil { !(isNil "objectVarsCompiled") };

//--- <0> Box or item to add items
[_this,

//*** WEAPONS ***//
//--- <1> Add Nato Common weapons
[],

//--- <2> Add Nato ACE Weapons
[],

//--- <3> Add Nato Arma 3 Weapons
[],

//--- <4> Add Nato Massi Weapons
[],

//--- <5> Add Nato RHS Weapons
[
["rhs_weap_M136", 4 ]
],

//*** AMMO ***//
//--- <6> Add Nato Common Ammo
[
["1Rnd_HE_Grenade_shell", 20 ],
["1Rnd_Smoke_Grenade_shell", 5 ],
["UGL_FlareWhite_F", 5 ],
[grenade, 5 ]
],

//--- <7> Add Nato ACE Ammo
[
[aceFlashBang, 5 ]
],

//--- <8> Add Nato Arma 3 Ammo
[
[TiAT, 2 ],
[TiAA, 2 ],
["RPG32_F", 2 ],
["NLAW_F", 2 ],
[mx65_100trace, 10 ],
[snip338_10r, 5 ],
[lmg_556_150r, 3 ],
[lmg338_130r, 3 ],
[mark762_20r, 5 ],
[mx65_30trace, 20 ],
[spar556_30t_r, 20 ]
],                                         //Need 3.38 Rounds LMG
                                           //Need 7.62
                                           //Need 9mm
                                           //Need 5.56 Drum Mag
//--- <9> Add Nato Massi Ammo
[
["mas_M136", 3 ],
["mas_SMAW", 3 ],
["mas_Stinger", 3 ],
["100Rnd_mas_762x51_T_Stanag", 4 ],
["200Rnd_mas_556x45_T_Stanag", 4 ],
["30Rnd_mas_556x45_T_Stanag", 20 ],
["30Rnd_556x45_Stanag_Tracer_Yellow", 8 ],
["5Rnd_127x108_Mag", 5 ],
["5Rnd_mas_127x99_Stanag", 5 ],
["5Rnd_mas_127x99_dem_Stanag", 3 ],
["5Rnd_mas_127x99_T_Stanag", 3 ]
],

//--- <10> Add Nato RHS Ammo
[
["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 12 ],
["rhs_mag_30Rnd_556x45_M855A1_Stanag", 12 ],
["rhsusf_100Rnd_556x45_soft_pouch", 8 ],
["rhsusf_50Rnd_762x51_m62_tracer", 8 ],
["rhsusf_mag_6Rnd_M441_HE", 8 ],
["rhsusf_mag_6Rnd_M576_Buckshot", 8 ],
["rhsusf_mag_6Rnd_M433_HEDP", 8 ],
["rhs_fim92_mag", 3 ],
["rhs_fgm148_magazine_AT", 4 ]
],

//*** ITEMS ***//
//--- <11> Add Nato Common Items
[],

//--- <12> Add Nato ACE Items
[],

//--- <13> Add Nato Arma 3 Items
[],

//--- <14> Add Nato Massi Items
[],

//--- <15> Add Nato RHS Items
[],

//***BACKPACKS***//
//--- <16> Add Nato Common Backpacks
[],

//--- <17> Add Nato ACE Backpacks
[],

//--- <18> Add Nato Arma 3 Backpacks
[],

//--- <19> Add Nato Massi Backpacks
[],

//--- <20> Add Nato RHS Backpacks
[],

//*** RADIOS ***//
//--- <21> How many backup short range radios?
0
] execVM "scripts\PR\loadouts\box\box_fill.sqf";
