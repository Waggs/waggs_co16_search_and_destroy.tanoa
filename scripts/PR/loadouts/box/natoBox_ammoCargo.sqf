/* natoBox_ammoCargo.sqf
*  Add the following to the init field of an ammo box in the editor:  _nil = this execVM "scripts\PR\loadouts\box\natoBox_ammoCargo.sqf";
*  When adding items, make sure they are in the correct sections (weapons/ammo/items..), and make sure the last item in each group doesn't have a comma at the end of the line
*  If spawned box use - [[namebox], "fnc_natoBoxAmmoCargo"] call BIS_fnc_MP;
*/

waitUntil { !(isNil "objectVarsCompiled") };

//--- <0> Box or item to add items
[_this,


//*** WEAPONS ***//
//--- <1> Add Nato Common weapons
[],

//--- <2> Add Nato ACE Weapons
[],

//--- <3> Add Nato Arma 3 Weapons
[],

//--- <4> Add Nato Massi Weapons
[],

//--- <5> Add Nato RHS Weapons
[
["rhs_weap_M136", 15 ]
],

//*** AMMO ***//
//--- <6> Add Nato Common Ammo
[
[gl1HE, 50 ],
[gl1Smoke, 20 ],
[glFlare, 20 ],
[grenade, 20 ]
],

//--- <7> Add Nato ACE Ammo
[
[aceFlashBang, 20 ]
],

//--- <8> Add Nato Arma 3 Ammo
[
[TiAT, 10 ],
[TiAA, 10 ],
[rpg, 10 ],
[nlaw, 10 ],
[mx65_100trace, 50 ],
["5Rnd_127x108_Mag", 25 ],
["7Rnd_408_Mag", 25 ],
[mx65_30trace, 50 ]
],

//--- <9> Add Nato Massi Ammo
[
["mas_M136", 10 ],
["mas_SMAW", 10 ],
["mas_Stinger", 10 ],
["100Rnd_mas_762x51_T_Stanag", 15 ],
["200Rnd_mas_556x45_T_Stanag", 15 ],
["30Rnd_mas_556x45_T_Stanag", 50 ],
["30Rnd_556x45_Stanag_Tracer_Yellow", 15 ],
["5Rnd_127x108_Mag", 15 ],
["5Rnd_mas_127x99_Stanag", 15 ],
["5Rnd_mas_127x99_dem_Stanag", 10 ],
["5Rnd_mas_127x99_T_Stanag", 10 ]
],

//--- <10> Add Nato RHS Ammo
[
["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 25 ],
["rhs_mag_30Rnd_556x45_M855A1_Stanag", 25 ],
["rhsusf_100Rnd_556x45_soft_pouch", 20 ],
["rhsusf_50Rnd_762x51_m62_tracer", 20 ],
["rhsusf_mag_6Rnd_M441_HE", 20 ],
["rhsusf_mag_6Rnd_M576_Buckshot", 20 ],
["rhsusf_mag_6Rnd_M433_HEDP", 20 ],
["rhs_fim92_mag", 15 ],
["rhs_fgm148_magazine_AT", 15 ]
],

//*** ITEMS ***//
//--- <11> Add Nato Common Items
[],

//--- <12> Add Nato ACE Items
[],

//--- <13> Add Nato Arma 3 Items
[],

//--- <14> Add Nato Massi Items
[],

//--- <15> Add Nato RHS Items
[],

//***BACKPACKS***//
//--- <16> Add Nato Common Backpacks
[],

//--- <17> Add Nato ACE Backpacks
[],

//--- <18> Add Nato Arma 3 Backpacks
[],

//--- <19> Add Nato Massi Backpacks
[],

//--- <20> Add Nato RHS Backpacks
[],

//*** RADIOS ***//
//--- <21> How many backup short range radios?
0
] execVM "scripts\PR\loadouts\box\box_fill.sqf";
