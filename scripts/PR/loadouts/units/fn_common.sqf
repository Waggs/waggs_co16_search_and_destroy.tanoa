/*
*  Author: PapaReap
*  File used by fn_gear.sqf
*  Change the loadout as required by your mission
*  Ver 1.0 2014-02-13 initial release
*  Ver 1.1 2015-01-15 allow for ace
*  Ver 1.2 2015-10-18 total loadout rework
*  Ver 1.3 2015-12-25 continued code optimization
*  Ver 1.4 2016-02-18 complete rework
*  Ver 1.5 2016-05-25 adding opfor units
*/
waitUntil { !(isNil "unitArrayCompiled") };

fn_remove = {
    _unit = _this select 0;
    removeAllAssignedItems _unit;
    removeAllContainers _unit;
    removeAllWeapons _unit;
    removeBackpack _unit;
    removeHeadgear _unit;
    removeUniform _unit;
    removeGoggles _unit;
    removeVest _unit;
};

fn_addItems = {
    _unit = _this select 0;
    _unit addWeapon "ItemGPS";
    _unit addWeapon "ItemWatch";
    _unit addWeapon "ItemMap";
    _unit addWeapon "ItemRadio";
    _unit addWeapon "ItemCompass";
    if (aceOn) then { _unit addWeapon "ACE_Vector" } else { _unit addWeapon "Binocular" };
};

fn_uniformItems = {
    _unit = _this select 0;
    _uniform = uniformContainer _unit;
    _uniform addMagazineCargoGlobal [smoke, 2];
    _uniform addMagazineCargoGlobal [chemY, 3];
    if (aceOn) then {
        _uniform addItemCargoGlobal ["ACE_MapTools", 1];
        _uniform addItemCargoGlobal ["ACE_CableTie", 1];
        _uniform addItemCargoGlobal ["ACE_microDAGR", 1];
        _uniform addItemCargoGlobal ["ACE_Flashlight_XL50", 1];
        _uniform addMagazineCargoGlobal ["ACE_HandFlare_White", 2];
        _uniform addMagazineCargoGlobal ["ACE_M84", 3];
    };
    
    if (playWest) then {
        //if (prWeapons == 8) then {
        //    _uniform addItemCargoGlobal ["H_mas_mar_Woolhat_w", 1]; //Massi - US Marines Winter
        //};
        if (masWeapon) then {
            _uniform addItemCargoGlobal ["H_mas_mar_Woolhat_w", 1]; //Massi - US Marines Winter
        };
    };
};

fn_vestItems = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    if !(rhsWeapon) then {
        if !((typeOf player) in pr_sniperArray) then {
            _vest addItemCargoGlobal [irPoint, 1];
        };
    };
    _vest addMagazineCargoGlobal [grenade, 2];
};

fn_backpackItems = {
    _unit = _this select 0;
    _backpack = unitBackPack _unit;
    if (aceOn) then {
        _backpack addItemCargoGlobal ["ACE_morphine", 3];
        _backpack addItemCargoGlobal ["ACE_fieldDressing", 6];
        _backpack addItemCargoGlobal ["ACE_entrenchingtool", 1];
    } else {
        _backpack addItemCargoGlobal ["FirstAidKit", 3];
    };
};

fn_45Handgun = {
    _unit = _this select 0;
    if (playWest) then {
        _unit addMagazines ["11Rnd_45ACP_Mag",1];
        _unit addWeapon "hgun_Pistol_heavy_01_snds_F";
        _unit addHandgunItem "muzzle_snds_acp";
        _unit addHandgunItem "optic_MRD";
        _vest = vestContainer _unit;
        _vest addMagazineCargoGlobal ["11Rnd_45ACP_Mag", 2];
    } else {
        if (playEast) then {
        _unit addMagazines ["6Rnd_45ACP_Cylinder",1];
        _unit addWeapon "hgun_Pistol_heavy_02_F";
        _unit addHandgunItem "optic_Yorris";
        _vest = vestContainer _unit;
        _vest addMagazineCargoGlobal ["6Rnd_45ACP_Cylinder", 2];
        };
    };
};

fn_rifle = {  // Primary weapons and attachments
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            if (closeQuarters) then {
                _unit addMagazine "30Rnd_9x21_Mag_SMG_02";
                _unit addWeapon "SMG_05_F";
                _unit addPrimaryWeaponItem "muzzle_snds_L";
                _unit addPrimaryWeaponItem "optic_Holosight_smg_blk_F";
                _vest addMagazineCargoGlobal ["30Rnd_9x21_Mag_SMG_02", 4];
                _backpack addMagazineCargoGlobal ["30Rnd_9x21_Mag_SMG_02", 2];
            } else {
                if (mediumAssault) then {
                    _unit addMagazine m556_30t_r;
                    _unit addWeapon "arifle_SPAR_01_blk_F";
                    _unit addPrimaryWeaponItem "muzzle_snds_M";
                    _unit addPrimaryWeaponItem "optic_ERCO_blk_F";
                    _vest addMagazineCargoGlobal [m556_30t_r, 4];
                    _backpack addMagazineCargoGlobal [m556_30t_r, 2];
                } else {
                    _unit addMagazine mx65_30trace;
                    if (defaultWeapon) then {
                        _unit addWeapon "arifle_MX_F";
                        _unit addPrimaryWeaponItem "muzzle_snds_H";
                        _unit addPrimaryWeaponItem "optic_Hamr";
                    } else {
                        _unit addWeapon "arifle_MX_khk_F";
                        _unit addPrimaryWeaponItem "muzzle_snds_H_khk_F";
                        _unit addPrimaryWeaponItem "optic_Hamr_khk_F";
                    };
                    _vest addMagazineCargoGlobal [mx65_30trace, 6];
                    _backpack addMagazineCargoGlobal ["30Rnd_65x39_caseless_mag", 2];
                };
            }
        } else {
            if (playEast) then {
                _unit addMagazine "30Rnd_65x39_caseless_green_mag_Tracer";
                _unit addWeapon "arifle_Katiba_F";
                _unit addPrimaryWeaponItem "muzzle_snds_H";
                _unit addPrimaryWeaponItem "optic_Arco";
                _vest addMagazineCargoGlobal ["30Rnd_65x39_caseless_green_mag_Tracer", 6];
                _backpack addMagazineCargoGlobal ["30Rnd_65x39_caseless_green", 2];
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "30Rnd_mas_556x45_T_Stanag";
            _unit addWeapon "arifle_mas_m4";
            _unit addPrimaryWeaponItem "muzzle_mas_snds_M";
            _unit addPrimaryWeaponItem "optic_mas_Arco_blk";
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 6];
            _backpack addMagazineCargoGlobal ["30Rnd_mas_556x45_Stanag", 2];
        } else {
            if (rhsWeapon) then {  // RHS
                if (playWest) then {  // US Marines Desert
                    _unit addMagazine "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red";
                    _unit addWeapon "rhs_weap_m16a4";
                    _unit addPrimaryWeaponItem "muzzle_snds_H_MG";
                    _unit addPrimaryWeaponItem "optic_Hamr";
                    _unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";
                    _vest addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 6];
                    _backpack addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag", 2];
                } else {
                    if (playEast) then {  // Russian Airborne Troops [VDV] - Desert
                        _unit addMagazine "rhs_45Rnd_545X39_AK_Green";
                        _unit addWeapon "rhs_weap_ak74m_desert_npz";
                        _unit addPrimaryWeaponItem "rhs_acc_tgpa";
                        _unit addPrimaryWeaponItem "optic_Arco";
                        if (_unit isKindOf "O_engineer_F") then {
                            _vest addMagazineCargoGlobal ["rhs_45Rnd_545X39_AK_Green", 2];
                            _backpack addMagazineCargoGlobal ["rhs_45Rnd_545X39_AK_Green", 1];
                            _backpack addMagazineCargoGlobal ["rhs_30Rnd_545x39_7N10_AK", 1];
                        } else {
                            if (_unit isKindOf "O_Soldier_AT_F") then {
                                _vest addMagazineCargoGlobal ["rhs_45Rnd_545X39_AK_Green", 4];
                                _backpack addMagazineCargoGlobal ["rhs_45Rnd_545X39_AK_Green", 2];
                                _backpack addMagazineCargoGlobal ["rhs_30Rnd_545x39_7N10_AK", 2];
                            } else {
                                _vest addMagazineCargoGlobal ["rhs_45Rnd_545X39_AK_Green", 6];
                                _backpack addMagazineCargoGlobal ["rhs_30Rnd_545x39_7N10_AK", 2];
                            };
                        };
                    };
                };
            };
        };
    };
    if !(rhsWeapon) then {
        _unit addPrimaryWeaponItem "acc_flashlight";
    };
};

fn_rifleMGlt = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            if (mediumAssault) then {
                _unit addMagazine "150Rnd_556x45_Drum_Mag_F";
                _unit addWeapon "arifle_SPAR_02_blk_F";
                _unit addPrimaryWeaponItem "optic_Holosight_blk_F";
                _vest addMagazineCargoGlobal ["150Rnd_556x45_Drum_Mag_F", 2];
                _backpack addMagazineCargoGlobal ["150Rnd_556x45_Drum_Mag_F", 4];
            } else {
                _unit addMagazine "100Rnd_65x39_caseless_mag_Tracer";
                if (defaultWeapon) then {
                    _unit addWeapon "arifle_MX_SW_F";
                    _unit addPrimaryWeaponItem "optic_Hamr";
                    _unit addPrimaryWeaponItem "bipod_01_F_snd";
                } else {
                    _unit addWeapon "arifle_MX_SW_khk_F";
                    _unit addPrimaryWeaponItem "optic_Hamr_khk_F";
                    _unit addPrimaryWeaponItem "bipod_01_F_blk";
                };
                _vest addMagazineCargoGlobal ["100Rnd_65x39_caseless_mag_Tracer", 3];
                _backpack addMagazineCargoGlobal ["100Rnd_65x39_caseless_mag_Tracer", 6];
                _backpack addMagazineCargoGlobal ["100Rnd_65x39_caseless_mag", 2];
            };
        } else {
            if (playEast) then {
                _unit addMagazine "150Rnd_762x54_Box_Tracer";
                _unit addWeapon "LMG_Zafir_pointer_F";
                _unit addPrimaryWeaponItem "optic_Arco";
                _vest addMagazineCargoGlobal ["150Rnd_762x54_Box_Tracer", 1];
                _backpack addMagazineCargoGlobal ["150Rnd_762x54_Box_Tracer", 2];
                _backpack addMagazineCargoGlobal ["150Rnd_762x54_Box", 1];
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "200Rnd_mas_556x45_T_Stanag";
            _unit addWeapon "LMG_mas_M249a_F";
            _unit addPrimaryWeaponItem "optic_mas_Arco_blk";
            _vest addMagazineCargoGlobal ["200Rnd_mas_556x45_T_Stanag", 6];
            _backpack addMagazineCargoGlobal ["200Rnd_mas_556x45_T_Stanag", 6];
        } else {
            if (rhsWeapon) then {  // RHS - US Marines Desert
                if (playWest) then {  // RHS - US Marines Desert
                    _unit addMagazine "rhsusf_200Rnd_556x45_soft_pouch";
                    _unit addWeapon "rhs_weap_m249_pip_S";
                    _unit addPrimaryWeaponItem "optic_Hamr";
                    _vest addMagazineCargoGlobal ["rhsusf_200Rnd_556x45_soft_pouch", 1];
                    _vest addItemCargoGlobal ["acc_flashlight", 1];
                    _backpack addMagazineCargoGlobal ["rhsusf_200Rnd_556x45_soft_pouch", 2];
                } else {
                    if (playEast) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                        _unit addMagazine "rhs_100Rnd_762x54mmR_green";
                        _unit addWeapon "rhs_weap_pkp";
                        _unit addPrimaryWeaponItem "rhs_acc_pkas";
                        _vest addMagazineCargoGlobal ["rhs_100Rnd_762x54mmR_green", 1];
                        _backpack addMagazineCargoGlobal ["rhs_100Rnd_762x54mmR_green", 2];
                        _backpack addMagazineCargoGlobal ["rhs_100Rnd_762x54mmR", 1];
                    };
                };
            };
        };
    };
    if !(rhsWeapon) then {
        if (playWest) then {
            _unit addPrimaryWeaponItem "bipod_01_F_blk";
            _unit addPrimaryWeaponItem "acc_flashlight";
        } else {
            if (playEast) then {
                _unit addPrimaryWeaponItem "bipod_01_F_blk";
                _unit addPrimaryWeaponItem "acc_flashlight";
            };
        };
    };
    _backpack addMagazineCargoGlobal [grenade, 2];
    _backpack addMagazineCargoGlobal [smoke, 2];
};

fn_rifleMGhvy = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            _unit addMagazine "130Rnd_338_Mag";
            if (defaultWeapon) then {
                _unit addWeapon "MMG_02_sand_RCO_LP_F";
                _unit addPrimaryWeaponItem "optic_Hamr";
                _unit addPrimaryWeaponItem "bipod_01_F_snd";
            } else {
                _unit addWeapon "MMG_02_black_F";
                _unit addPrimaryWeaponItem "optic_Hamr";
                _unit addPrimaryWeaponItem "bipod_01_F_blk";
            };
            _vest addMagazineCargoGlobal ["130Rnd_338_Mag", 1];
            _backpack addMagazineCargoGlobal ["130Rnd_338_Mag", 2];
        } else {
            if (playEast) then {
                //need
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "100Rnd_mas_762x51_T_Stanag";
            _unit addWeapon "LMG_mas_mk48_F";
            _unit addPrimaryWeaponItem "optic_mas_Arco_blk";
            _vest addMagazineCargoGlobal ["100Rnd_mas_762x51_T_Stanag", 4];
            _backpack addMagazineCargoGlobal ["100Rnd_mas_762x51_T_Stanag", 8];
        } else {
            if (rhsWeapon) then {  // RHS - US Marines Desert
                _unit addMagazine "rhsusf_100Rnd_762x51_m62_tracer";
                _unit addWeapon "rhs_weap_m240b_usmc";
                _unit addPrimaryWeaponItem "optic_Hamr";
                _vest addMagazineCargoGlobal ["rhsusf_100Rnd_762x51_m62_tracer", 1];
                _vest addMagazineCargoGlobal ["rhsusf_50Rnd_762x51_m62_tracer", 1];
                _backpack addMagazineCargoGlobal ["rhsusf_100Rnd_762x51_m62_tracer", 1];
                _backpack addMagazineCargoGlobal ["rhsusf_50Rnd_762x51_m62_tracer", 1];
            };
        };
    };
    _backpack addMagazineCargoGlobal [grenade, 2];
    _backpack addMagazineCargoGlobal [smoke, 2];
    if (rhsWeapon) exitWith {};
    _unit addPrimaryWeaponItem "bipod_01_F_blk";
    _unit addPrimaryWeaponItem "acc_flashlight";
};

fn_rifleGL = {
    _unit = _this select 0;
    if ((_unit getVariable "rhsGLhvy") == "true") exitWith {};

    if !((rhsWeapon) && (playEast)) then {
        _unit addMagazine gl1HE;
    };
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            if (mediumAssault) then {
                _unit addMagazine m556_30t_r;
                _unit addWeapon "arifle_SPAR_01_GL_blk_F";
                _unit addPrimaryWeaponItem "muzzle_snds_M";
                _unit addPrimaryWeaponItem "optic_ERCO_blk_F";
                _vest addMagazineCargoGlobal [m556_30t_r, 4];
                _backpack addMagazineCargoGlobal [m556_30t_r, 2];
            } else {
                _unit addMagazine mx65_30trace;
                if (defaultWeapon) then {
                    _unit addWeapon "arifle_MX_GL_F";
                    _unit addPrimaryWeaponItem "muzzle_snds_H";
                    _unit addPrimaryWeaponItem "optic_Hamr";
                } else {
                    _unit addWeapon "arifle_MX_GL_khk_F";
                    _unit addPrimaryWeaponItem "muzzle_snds_H_khk_F";
                    _unit addPrimaryWeaponItem "optic_Hamr_khk_F";
                };
                _vest addMagazineCargoGlobal [mx65_30trace, 5];
                _backpack addMagazineCargoGlobal ["30Rnd_65x39_caseless_mag", 2];
            };
        } else {
            if (playEast) then {
                _unit addMagazine "30Rnd_65x39_caseless_green_mag_Tracer";
                _unit addWeapon "arifle_Katiba_GL_ACO_F";
                _unit addPrimaryWeaponItem "muzzle_snds_H";
                _unit addPrimaryWeaponItem "optic_Arco";
                _vest addMagazineCargoGlobal ["30Rnd_65x39_caseless_green_mag_Tracer", 5];
                _backpack addMagazineCargoGlobal ["30Rnd_65x39_caseless_green", 2];
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "30Rnd_mas_556x45_T_Stanag";
            _unit addWeapon "arifle_mas_m4_m203";
            _unit addPrimaryWeaponItem "muzzle_mas_snds_M";
            _unit addPrimaryWeaponItem "optic_mas_Arco_blk";
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 6];
            _backpack addMagazineCargoGlobal ["30Rnd_mas_556x45_Stanag", 2];
        } else {
            if (rhsWeapon) then {
                if (playWest) then {  // RHS - US Marines Desert
                    _unit addMagazine "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red";
                    _unit addWeapon "rhs_weap_m16a4_carryhandle_M203";
                    _unit addPrimaryWeaponItem "muzzle_snds_H_MG";
                    _unit addPrimaryWeaponItem "optic_Hamr";
                    _unit addPrimaryWeaponItem "rhsusf_acc_nt4_black";
                    _vest addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 6];
                    _backpack addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag", 2];
                } else {
                    if (playEast) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                        _unit addMagazine "rhs_30Rnd_545x39_AK_green";
                        _unit addMagazine "rhs_VOG25";
                        _unit addWeapon "rhs_weap_ak74m_gp25_npz";
                        //_unit addPrimaryWeaponItem "rhs_acc_dtk1";
                        _unit addPrimaryWeaponItem "rhs_acc_tgpa";
                        _unit addPrimaryWeaponItem "optic_Arco";
                        _vest addMagazineCargoGlobal ["rhs_30Rnd_545x39_AK_green", 6];
                        _backpack addMagazineCargoGlobal ["rhs_30Rnd_545x39_7N10_AK", 2];
                    };
                };
            };
        };
    };
    if !((rhsWeapon) && (playEast)) then {
        _unit addPrimaryWeaponItem "acc_flashlight";
        _vest addMagazineCargoGlobal [gl1HE, 2];
        _backpack addMagazineCargoGlobal [gl1HE, 10];
        _backpack addMagazineCargoGlobal [glFlare, 6];
        _backpack addMagazineCargoGlobal [gl1Smoke, 2];
    } else {
        if (playEast) then {
            if (rhsWeapon) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                if (_unit isKindOf "O_Soldier_GL_F") then {
                    _backpack addMagazineCargoGlobal ["rhs_VOG25", 12];
                } else {
                    _vest addMagazineCargoGlobal ["rhs_VOG25", 2];
                    _backpack addMagazineCargoGlobal ["rhs_VOG25", 10];
                };
                _backpack addMagazineCargoGlobal ["rhs_VG40OP_white", 6];
                _backpack addMagazineCargoGlobal ["rhs_GRD40_White", 2];
            };
        };
    };
};

fn_GLlt = {
    _unit = _this select 0;
    if ((_unit getVariable "rhsGLhvy") == "true") exitWith {};

    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 2];
        } else {
            if (rhsWeapon) then {  // RHS
                if (playWest) then {  // RHS - US Marines Desert
                    _vest addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 2];
                } else {
                    if (playEast) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                        _backpack addMagazineCargoGlobal ["rhs_30Rnd_545x39_AK_green", 2];
                    };
                };
            };
        };
    };

    if (playWest) then {
        _vest addMagazineCargoGlobal [gl1HE, 6];
        _vest addMagazineCargoGlobal [gl1Smoke, 2];
        _vest addMagazineCargoGlobal [gl1SmokeG, 2];
        _backpack addMagazineCargoGlobal [glFlare, 6];
    } else {
        if (playEast) then {
            if (rhsWeapon) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                _backpack addMagazineCargoGlobal ["rhs_VOG25", 6];
                _backpack addMagazineCargoGlobal ["rhs_GRD40_White", 2];
                _backpack addMagazineCargoGlobal ["rhs_VG40OP_green", 2];
                _backpack addMagazineCargoGlobal ["rhs_VG40OP_white", 6];
            };
        };
    };
    _backpack addMagazineCargoGlobal [grenade, 2];
    _backpack addMagazineCargoGlobal [smoke, 2];
};

fn_GLhvy = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;

    if (rhsWeapon) then {  // RHS - US Marines Desert
        _unit addMagazine "rhsusf_mag_6Rnd_M441_HE";
        _unit addWeapon "rhs_weap_m32";
        _unit addPrimaryWeaponItem "acc_flashlight";
        _vest addMagazineCargoGlobal ["rhsusf_mag_6Rnd_M441_HE", 3];
        _vest addMagazineCargoGlobal ["rhsusf_mag_6Rnd_M433_HEDP", 2];
        _backpack addMagazineCargoGlobal ["rhsusf_mag_6Rnd_M441_HE", 6];
        _backpack addMagazineCargoGlobal ["rhsusf_mag_6Rnd_M576_Buckshot", 2];
        _backpack addMagazineCargoGlobal ["rhsusf_mag_6Rnd_M433_HEDP", 2];
        _backpack addMagazineCargoGlobal [smoke, 2];
    };

    if ((_unit getVariable "rhsGLhvy") == "true") exitWith {};
    _backpack addMagazineCargoGlobal [gl1HE, 14];
    _backpack addMagazineCargoGlobal [glFlare, 4];
    _backpack addMagazineCargoGlobal [grenade, 2];
};

fn_AT = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        _unit addMagazine "NLAW_F";
        _unit addWeapon "launch_NLAW_F";
        _backpack addMagazineCargoGlobal ["NLAW_F", 1];
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "mas_M136";
            _unit addWeapon "mas_launch_M136_F";
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 2];
            _backpack addMagazineCargoGlobal ["mas_M136", 3];
            _backpack addMagazineCargoGlobal ["mas_M136_HE", 1];
        } else {
            if (rhsWeapon) then {  // RHS
                if (playWest) then {  // US Marines Desert
                    _unit addWeapon "rhs_weap_M136";
                    _backpack addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 4];
                    _backpack addMagazineCargoGlobal ["SLAMDirectionalMine_Wire_Mag", 6];
                } else {
                    if (playEast) then {  // Russian Airborne Troops [VDV] - Desert
                        _unit addMagazine "rhs_rpg7_PG7VL_mag";
                        _unit addWeapon "rhs_weap_rpg7_pgo";
                        _backpack addMagazineCargoGlobal ["rhs_45Rnd_545X39_AK_Green", 6];
                        _backpack addMagazineCargoGlobal ["rhs_rpg7_PG7VL_mag", 1];
                        _backpack addMagazineCargoGlobal ["rhs_rpg7_PG7VR_mag", 2];
                    };
                };
            };
        };
    };
};

fn_ATS = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        _unit addMagazine TiAT;
        if (defaultWeapon) then {
            _unit addWeapon "launch_Titan_short_F";
        } else {
            _unit addWeapon "launch_B_Titan_short_tna_F";
        };
        _backpack addMagazineCargoGlobal [TiAT, 1];
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "mas_SMAW";
            _unit addWeapon "mas_launch_smaw_F";
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 2];
            _backpack addMagazineCargoGlobal ["mas_SMAW", 2];
            _backpack addMagazineCargoGlobal ["mas_SMAW_HE", 1];
            _backpack addMagazineCargoGlobal ["mas_SMAW_NE", 1];
        } else {
            if (rhsWeapon) then {  // RHS - US Marines Desert
                _unit addMagazine "rhs_fgm148_magazine_AT";
                _unit addWeapon "rhs_weap_fgm148";
                _backpack addMagazineCargoGlobal ["rhs_fgm148_magazine_AT", 1];
            };
        };
    };
};

fn_AAlt = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        _unit addMagazine TiAA;
        if (defaultWeapon) then {
            _unit addWeapon "launch_Titan_F";
        } else {
            _unit addWeapon "launch_B_Titan_tna_F";
        };
        _backpack addMagazineCargoGlobal [TiAA, 1];
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "mas_Stinger";
            _unit addWeapon "mas_launch_Stinger_F";
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 2];
            _backpack addMagazineCargoGlobal ["mas_Stinger", 2];
        } else {
            if (rhsWeapon) then {  // RHS
                _unit addMagazine "rhs_fim92_mag";
                _unit addWeapon "rhs_weap_fim92";
                _backpack addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 6];
            };
        };
    };
};

fn_AAhvy = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        _unit addMagazine TiAA;
        if (defaultWeapon) then {
            _unit addWeapon "launch_Titan_F";
        } else {
            _unit addWeapon "launch_B_Titan_tna_F";
        };
        _backpack addMagazineCargoGlobal [TiAA, 1];
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "mas_Stinger";
            _unit addWeapon "mas_launch_Stinger_F";
            _backpack addMagazineCargoGlobal ["mas_Stinger", 3];
        } else {
            if (rhsWeapon) then {  // RHS
                _unit addMagazine "rhs_fim92_mag";
                _unit addWeapon "rhs_weap_fim92";
                _backpack addMagazineCargoGlobal ["rhs_fim92_mag", 1];
            };
        };
    };
};

fn_ammoMag = {        //Allow for MediumAssault
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            _vest addMagazineCargoGlobal [mx65_30trace, 2];
            _backpack addMagazineCargoGlobal [mx65_30trace, 10];
            _backpack addMagazineCargoGlobal [mx65_100trace, 1];
            _backpack addMagazineCargoGlobal ["130Rnd_338_Mag", 1];
            _backpack addMagazineCargoGlobal [gl1HE, 22];
        } else {
            if (playEast) then {
                _vest addMagazineCargoGlobal ["30Rnd_65x39_caseless_green_mag_Tracer", 2];
                _backpack addMagazineCargoGlobal ["30Rnd_65x39_caseless_green_mag_Tracer", 10];
                _backpack addMagazineCargoGlobal ["150Rnd_762x54_Box_Tracer", 1];
                //_backpack addMagazineCargoGlobal ["150Rnd_762x54_Box_Tracer", 1];
                _backpack addMagazineCargoGlobal [gl1HE, 22];
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 2];
            _backpack addMagazineCargoGlobal ["30Rnd_mas_556x45_T_Stanag", 10];
            _backpack addMagazineCargoGlobal ["200Rnd_mas_556x45_T_Stanag", 1];
            _backpack addMagazineCargoGlobal ["100Rnd_mas_762x51_T_Stanag", 2];
            _backpack addMagazineCargoGlobal [gl1HE, 22];
        } else {
            if (rhsWeapon) then {  // RHS - US Marines Desert
                _backpack addMagazineCargoGlobal ["rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10];
                _backpack addMagazineCargoGlobal ["rhsusf_200Rnd_556x45_soft_pouch", 1];
                _backpack addMagazineCargoGlobal ["rhsusf_100Rnd_762x51_m62_tracer", 2];
                _backpack addMagazineCargoGlobal [gl1HE, 10];
                _backpack addMagazineCargoGlobal ["rhsusf_mag_6Rnd_M441_HE", 2];
            };
        };
    };
};

fn_rifleMark = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            if (mediumAssault) then {
                _unit addMagazine "20Rnd_762x51_Mag";
                _unit addWeapon "arifle_SPAR_03_blk_F";
                _unit addPrimaryWeaponItem "muzzle_snds_B";
                _unit addPrimaryWeaponItem "optic_SOS";
                _unit addPrimaryWeaponItem "bipod_01_F_blk";
                _vest addMagazineCargoGlobal ["20Rnd_762x51_Mag", 4];
                _backpack addMagazineCargoGlobal ["20Rnd_762x51_Mag", 2];
            } else {
                _unit addMagazine "20Rnd_762x51_Mag";
                if (defaultWeapon) then {
                    _unit addWeapon "srifle_DMR_03_F";
                    _unit addPrimaryWeaponItem "optic_AMS";
                    _unit addPrimaryWeaponItem "muzzle_snds_B";
                } else {
                    _unit addWeapon "srifle_DMR_03_khaki_F";
                    _unit addPrimaryWeaponItem "optic_AMS_khk";
                    _unit addPrimaryWeaponItem "muzzle_snds_B_khk_F";
                };
                _unit addPrimaryWeaponItem "bipod_01_F_blk";
                _unit addPrimaryWeaponItem "acc_flashlight";
                _vest addMagazineCargoGlobal ["20Rnd_762x51_Mag", 8];
                _backpack addMagazineCargoGlobal ["20Rnd_762x51_Mag", 2];
                _backpack addMagazineCargoGlobal ["APERSTripMine_Wire_Mag", 4];
                _backpack addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 2];
            };
        } else {
            if (playEast) then {
                //need
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "20Rnd_762x51_Mag";
            _unit addWeapon "arifle_mas_fal";
            _unit addPrimaryWeaponItem "optic_AMS";
            _unit addPrimaryWeaponItem "bipod_01_F_blk";
            _unit addPrimaryWeaponItem "muzzle_mas_snds_M";
            _unit addPrimaryWeaponItem "acc_flashlight";
            _vest addMagazineCargoGlobal ["20Rnd_762x51_Mag", 8];
            _backpack addMagazineCargoGlobal ["20Rnd_762x51_Mag", 2];
            _backpack addMagazineCargoGlobal ["APERSTripMine_Wire_Mag", 4];
            _backpack addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 2];
        } else {
            if (rhsWeapon) then {  // RHS
                if (playWest) then {  // US Marines Desert
                    _unit addMagazine "rhsusf_20Rnd_762x51_m118_special_Mag";
                    _unit addWeapon "rhs_weap_m14ebrri";
                    _unit addPrimaryWeaponItem "optic_AMS";
                    _unit addPrimaryWeaponItem "bipod_01_F_blk";
                    _unit addPrimaryWeaponItem "muzzle_mas_snds_M";
                    _unit addPrimaryWeaponItem "acc_flashlight";
                    _vest addMagazineCargoGlobal ["rhsusf_20Rnd_762x51_m118_special_Mag", 7];
                    _backpack addMagazineCargoGlobal ["rhsusf_20Rnd_762x51_m118_special_Mag", 2];
                    _backpack addMagazineCargoGlobal ["APERSTripMine_Wire_Mag", 4];
                    _backpack addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 2];
                } else {
                    if (playEast) then {  // Russian Airborne Troops [VDV] - Desert
                        _unit addMagazine "";
                        _unit addWeapon "";
                        _unit addPrimaryWeaponItem "";
                        _unit addPrimaryWeaponItem "";
                        if (_unit isKindOf "O_engineer_F") then {
                            _vest addMagazineCargoGlobal ["", 2];
                            _backpack addMagazineCargoGlobal ["", 1];
                            _backpack addMagazineCargoGlobal ["", 1];
                        } else {
                            if (_unit isKindOf "O_Soldier_AT_F") then {
                                _vest addMagazineCargoGlobal ["", 4];
                                _backpack addMagazineCargoGlobal ["", 2];
                                _backpack addMagazineCargoGlobal ["", 2];
                            } else {
                                _vest addMagazineCargoGlobal ["", 6];
                                _backpack addMagazineCargoGlobal ["", 2];
                            };
                        };
                    };
                };
            };
        };
    };
    if (!(rhsWeapon) && !(playEast)) then {
        _unit addPrimaryWeaponItem "acc_flashlight";
    };
};

fn_rifleSnip = {
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if ((defaultWeapon) || (tropicWeapon)) then {  //Standard Arma 3 Soldiers
        if (playWest) then {
            if (mediumAssault) then {
                _unit addMagazine "10Rnd_338_Mag";
                _unit addWeapon "srifle_DMR_02_F";
                _unit addPrimaryWeaponItem "muzzle_snds_338_black";
                _unit addPrimaryWeaponItem "optic_KHS_old";
                _unit addPrimaryWeaponItem "bipod_01_F_blk";
                _vest addMagazineCargoGlobal ["10Rnd_338_Mag", 4];
                _backpack addMagazineCargoGlobal ["10Rnd_338_Mag", 2];
            } else {
                _unit addMagazine "7Rnd_408_Mag";
                if (defaultWeapon) then {
                    _unit addWeapon "srifle_LRR_F";
                    _unit addPrimaryWeaponItem "optic_SOS";
                } else {
                    _unit addWeapon "srifle_LRR_tna_F";
                    _unit addPrimaryWeaponItem "optic_SOS_khk_F";
                };
                _unit addPrimaryWeaponItem "";
                _unit addPrimaryWeaponItem "acc_flashlight";
                _unit addPrimaryWeaponItem "bipod_01_F_blk";
                _vest addMagazineCargoGlobal ["7Rnd_408_Mag", 6];
                _backpack addMagazineCargoGlobal ["7Rnd_408_Mag", 2];
                _backpack addMagazineCargoGlobal ["APERSTripMine_Wire_Mag", 2];
                _backpack addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 1];
            };
        } else {
            if (playEast) then {
                //need
            };
        };
    } else {
        if (masWeapon) then { // Massi - UK, UK Winter, US Marines, US Marines Winter
            _unit addMagazine "5Rnd_mas_127x99_T_Stanag";
            _unit addWeapon "srifle_mas_m107_h";
            _unit addPrimaryWeaponItem "acc_flashlight";
            _unit addPrimaryWeaponItem "optic_mas_DMS";
            _unit addPrimaryWeaponItem "bipod_01_F_blk";
            _vest addMagazineCargoGlobal ["5Rnd_mas_127x99_Stanag", 4];
            _vest addMagazineCargoGlobal ["5Rnd_mas_127x99_T_Stanag", 4];
            _vest addMagazineCargoGlobal ["5Rnd_mas_127x99_dem_Stanag", 4];
            _vest addItemCargoGlobal ["acc_mas_pointer_IR_b", 1];
            _backpack addMagazineCargoGlobal ["5Rnd_mas_127x99_Stanag", 2];
            _backpack addMagazineCargoGlobal ["5Rnd_mas_127x99_T_Stanag", 2];
            _backpack addMagazineCargoGlobal ["5Rnd_mas_127x99_dem_Stanag", 2];
            _backpack addMagazineCargoGlobal ["APERSTripMine_Wire_Mag", 4];
            _backpack addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 2];
        } else {
            if (rhsWeapon) then {  // RHS
                if (playWest) then {  // US Marines Desert
                    _unit addMagazine "rhsusf_5Rnd_300winmag_xm2010";
                    _unit addWeapon "rhs_weap_XM2010";
                    _unit addPrimaryWeaponItem "rhsusf_acc_M2010S";
                    _unit addPrimaryWeaponItem "acc_flashlight";
                    _unit addPrimaryWeaponItem "rhsusf_acc_LEUPOLDMK4_2";
                    _unit addPrimaryWeaponItem "bipod_01_F_blk";
                    _vest addMagazineCargoGlobal ["rhsusf_5Rnd_300winmag_xm2010", 6];
                    _vest addItemCargoGlobal [irPoint, 1];
                    _backpack addMagazineCargoGlobal ["rhsusf_5Rnd_300winmag_xm2010", 2];
                    _backpack addMagazineCargoGlobal ["APERSTripMine_Wire_Mag", 4];
                    _backpack addMagazineCargoGlobal ["ClaymoreDirectionalMine_Remote_Mag", 2];
                } else {
                    if (playEast) then {  // Russian Airborne Troops [VDV] - Desert
                        _unit addMagazine "";
                        _unit addWeapon "";
                        _unit addPrimaryWeaponItem "";
                        _unit addPrimaryWeaponItem "";
                        if (_unit isKindOf "O_engineer_F") then {
                            _vest addMagazineCargoGlobal ["", 2];
                            _backpack addMagazineCargoGlobal ["", 1];
                            _backpack addMagazineCargoGlobal ["", 1];
                        } else {
                            if (_unit isKindOf "O_Soldier_AT_F") then {
                                _vest addMagazineCargoGlobal ["", 4];
                                _backpack addMagazineCargoGlobal ["", 2];
                                _backpack addMagazineCargoGlobal ["", 2];
                            } else {
                                _vest addMagazineCargoGlobal ["", 6];
                                _backpack addMagazineCargoGlobal ["", 2];
                            };
                        };
                    };
                };
            };
        };
    };
    if (!(rhsWeapon) && !(playEast)) then {
        _unit addPrimaryWeaponItem "acc_flashlight";
    };
};

fn_Pilot = {
    _unit = _this select 0;
    if (playWest) then {
        _unit addMagazines ["30Rnd_45ACP_Mag_SMG_01",1];
        _unit addWeapon "SMG_01_F";
        _unit addPrimaryWeaponItem "muzzle_snds_acp";
        _unit addPrimaryWeaponItem "optic_Holosight_smg_blk_F";
        _vest = vestContainer _unit;
        _vest addMagazineCargoGlobal ["30Rnd_45ACP_Mag_SMG_01", 7];
    } else {
        if (playEast) then {
        _unit addMagazines ["30Rnd_45ACP_Mag_SMG_01",1];
        _unit addWeapon "SMG_01_F";
        _unit addPrimaryWeaponItem "muzzle_snds_acp";
        _unit addPrimaryWeaponItem "optic_Holosight_smg_blk_F";
        _vest = vestContainer _unit;
        _vest addMagazineCargoGlobal ["30Rnd_45ACP_Mag_SMG_01", 7];
        };
    };
};

fn_repairDemo = {  // Medic Backpack/rucksack
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if (aceOn) then {
        _vest addItemCargoGlobal ["ACE_M26_Clacker", 1];
        _vest addItemCargoGlobal ["ACE_DefusalKit", 1];
    };
    _backpack addItemCargoGlobal ["MineDetector", 1];
    _backpack addMagazineCargoGlobal ["DemoCharge_Remote_Mag", 4];
    _backpack addMagazineCargoGlobal ["SatchelCharge_Remote_Mag", 1];
    _backpack addItemCargoGlobal ["ToolKit", 1];
};

fn_med = {  // Medic Backpack/rucksack
    _unit = _this select 0;
    _vest = vestContainer _unit;
    _backpack = unitBackPack _unit;
    if (aceOn) then {
        _backpack addItemCargoGlobal ["ACE_bloodIV", 10];
        _backpack addItemCargoGlobal ["ACE_epinephrine", 6];
        _backpack addItemCargoGlobal ["ACE_fieldDressing", 24];
        _backpack addItemCargoGlobal ["ACE_morphine", 16];
        _backpack addItemCargoGlobal ["ACE_bodyBag", 1];
    } else {
        _backpack addItemCargoGlobal ["FirstAidKit", 9];
        _backpack addItemCargoGlobal ["MediKit", 1];
    };
    _backpack addMagazineCargoGlobal ["SmokeShellGreen", 14];
};

fn_vest2Pack = {
    _unit = _this select 0;
    _vest = vest _unit;
    _vestContainer = vestContainer _unit;
    _itemsVest = getItemCargo _vestContainer;
    _magazinesVest = getMagazineCargo _vestContainer;
    _backpack = unitBackPack _unit;

    _count = count (_itemsVest select 0) - 1;
    for "_i" from 0 to _count do {
        _backpack addItemCargoGlobal [(_itemsVest select 0) select _i, (_itemsVest select 1) select _i];
    };
    _count = count (_magazinesVest select 0) - 1;
    for "_i" from 0 to _count do {
        _backpack addMagazineCargoGlobal [(_magazinesVest select 0) select _i, (_magazinesVest select 1) select _i];
    };

    removeVest _unit;
    _unit addVest _vest;
};

gearCompiled = true;
