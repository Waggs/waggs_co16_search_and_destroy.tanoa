// script to add objects to the list of objects Zeus can influence
//
// Usage:  ObjectArray call z_AddObjects
//    or in unit's init:  [this] execVM "scripts\SKULL\DebugMenu\SKL_ZeusObjects.sqf";

if (isNil "z_AddObjects") then {
    z_AddObjects = {
        if (isServer) then
        {
            _objs = [];
            if (typeName _this == "ARRAY") then {
                {
                    if ((side _x) in [EAST,WEST,RESISTANCE,CIVILIAN]) then {_objs = _objs + [_x]};
                } forEach _this;
            } else { _objs = [_this]};
            
            {
                _x addCuratorEditableObjects [_objs,true]; 
            } forEach allCurators;
        };
    };
};

if (typeName _this == "ARRAY") then { _this call z_AddObjects; };

