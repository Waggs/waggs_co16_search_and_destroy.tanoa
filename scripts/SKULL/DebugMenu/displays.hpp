class RscTitles
{
	class FHQ_Debug_Watch
	{
		onload = "uinamespace setvariable ['FHQ_Debug_Display',_this select 0];";
		moving = false;
		idd = 140869;
		movingEnable = true;
		enableSimulation = true;
		controlsBackground[] = { };
		objects[] = { };
		controls[] = { Line1, Line2, Line3, Line4};
		duration = 0x7fffffff;
		class Line1 : FHQRscText
		{
			idc = 181;
			style = ST_LEFT;
			x = 0;
			y = 0.76;
			w = 1;
			h = 0.5;
			shadow = SHADOW;
			text = "";
		};
		class Line2 : FHQRscText
		{
			idc = 182;
			style = ST_STATIC;
			x = 0;
			y = 0.82;
			w = 0.5;
			h = 0.5;
			shadow = SHADOW;
			text = "";
		};
		class Line3 : FHQRscText
		{
			idc = 183;
			style = ST_STATIC;
			x = 0;
			y = 0.88;
			w = 0.5;
			h = 0.5;
			shadow = SHADOW;
			text = "";
		};
		class Line4 : FHQRscText
		{
			idc = 184;
			style = ST_STATIC;
			x = 0;
			y = 0.94;
			w = 0.5;
			h = 0.5;
			shadow = SHADOW;
			text = "";
		};
	};


	class FHQ_Debug_OutputWindow
	{
		onload = "uinamespace setvariable ['FHQ_Debug_Display',_this select 0];";
		moving = false;
		idd = 140862;
		movingEnable = true;
		enableSimulation = true;
		controlsBackground[] = { };
		objects[] = { };
		controls[] = { FHQ_DebugConsole_OutputWindow};
		duration = 0x7fffffff;

		class FHQ_DebugConsole_OutputWindow: FHQRscStructuredText
		{
			style = ST_LEFT;
			idc = 1100;
			x = 0.00368684 * safezoneW + safezoneX;
			y = 0.00485283 * safezoneH + safezoneY;
			w = 0.689682 * safezoneW;
			h = 0.288836 * safezoneH;
			colorBackground[] = {1,1,1,0.2};
			 class Attributes
			 {
				font = "TahomaB";
				color = "#ffffff";
				align = "left";
				valign = "left";
				shadow = true;
				shadowColor = "#ff0000";
				size = "1";
			 };
		};
	};
};
