#include "ids.h"

FHQ_DebugConsole_HouseposSelect = {
    if (!isNull FHQ_DebugConsole_CurrentHouse) then {
    	FHQ_DebugConsole_SelectedHouse = FHQ_DebugConsole_CurrentHouse;
        
        disableSerialization;
        createDialog "FHQ_DebugDialog_HousePos";
        
        _display = findDisplay HOUSE_DIALOG;
        /* Fill the dialog */
        _i = 0;
        FHQ_DebugConsole_CurrentHouse_Position = [];
        
        while {true} do {
        	_pos = FHQ_DebugConsole_SelectedHouse buildingPos _i;
            
            // Assume zero x/y means no position #_i present
            if (((_pos select 0) == 0) && ((_pos select 1) == 0)) exitWith {}; 

			// Add this to the listview
            lbAdd [HOUSE_DIALOG_SELECT, format ["#%1", _i]];
            FHQ_DebugConsole_CurrentHouse_Position = FHQ_DebugConsole_CurrentHouse_Position + [_pos]; 
			_i = _i + 1;
        };
        
        if (count FHQ_DebugConsole_CurrentHouse_Position == 0) exitWith {closeDialog 0; hintSilent "Building has no positions"};
        
        _string = toArray (str FHQ_DebugConsole_SelectedHouse);
        _houseID = [];
        _modelName = [];
        _space = 0;
        _idx = 0;
        // str'd object is "hex number# ID: model"
        while {_idx < count _string} do {
            // Search for spaces and copy
            switch (_space) do {
                case 0: {
                    // searching for first space, just skip
                    if ((_string select _idx) == 32) then {
                		_space = 1;
                    };
					_idx = _idx + 1;
                };
                case 1: {
                    // Found first space
            		if ( ((_string select _idx) != 58) && ((_string select _idx) != 32) ) then {
                        _houseID = _houseID + [_string select _idx];
                    };
                    if ((_string select _idx) == 32) then {
                        _space = 2;
                    };
                    _idx = _idx +  1;
                };
                case 2: {
                    // Second space
                    _modelName = _modelName + [_string select _idx];
              		_idx = _idx +  1;
                };
            };
        };
        
        FHQ_DebugConsole_CurrentHouse_Model = toString _modelName;
        FHQ_DebugConsole_CurrentHouse_ID = toString _houseID;
        
        lbSetCurSel [HOUSE_DIALOG_SELECT, 0];
        ctrlSetText [HOUSE_DIALOG_MODEL, FHQ_DebugConsole_CurrentHouse_Model];
        ctrlSetText [HOUSE_DIALOG_ID, FHQ_DebugConsole_CurrentHouse_ID];
        ctrlSetText [HOUSE_DIALOG_POSITION, format ["%1", FHQ_DebugConsole_CurrentHouse_Position select 0]];
        ctrlSetText [HOUSE_DIALOG_INIT, format ["this setPos (((position this) nearestObject %1) buildingPos %2)", FHQ_DebugConsole_CurrentHouse_ID, 0]]; 
    };
};

FHQ_DebugConsole_HousePos_SelectPos = {
    FHQ_DebugConsole_CurrentHouse_Selected = _this select 1;
	ctrlSetText [HOUSE_DIALOG_POSITION, format ["%1", FHQ_DebugConsole_CurrentHouse_Position select FHQ_DebugConsole_CurrentHouse_Selected]];
    ctrlSetText [HOUSE_DIALOG_INIT, format ["this setPos (((position this) nearestObject %1) buildingPos %2)", FHQ_DebugConsole_CurrentHouse_ID, FHQ_DebugConsole_CurrentHouse_Selected]]; 
};

FHQ_DebugConsole_HousePos_Jump = {
    player setPos (FHQ_DebugConsole_CurrentHouse_Position select FHQ_DebugConsole_CurrentHouse_Selected);
};


#define DISTANCE 40

private ["_object", "_arrow", "_playerPos", "_playerDir", "_endPos", "_list", "_house", "_pos", "__box"];

if (FHQ_DebugConsole_IsArma3) then {
	_object = createVehicle ["Sign_Sphere100cm_F", [0,0,0], [], 0, "NONE"];
	_arrow = createVehicle ["Sign_Arrow_Large_F", [0,0,-10000], [], 0, "NONE"];
} else {
	_object = createVehicle ["sign_sphere25cm_ep1", [0,0,0], [], 0, "NONE"];
	_arrow = createVehicle ["Sign_arrow_down_large_EP1", [0,0,-10000], [], 0, "NONE"];
};

FHQ_DebugConsole_CurrentHouse = objNull;
FHQ_DebugConsole_SelectedHouse = objNull;

SKL_KeyHandler = FHQ_DebugConsole_HouseposSelect;
call SKL_DebugWatchKeys; // enable trapping the ENTER key

hint "When arrow points at house, press ENTER";
while {isNull FHQ_DebugConsole_SelectedHouse} do
{
    _playerPos = eyePos player;
	_playerDir = player weaponDirection currentWeapon player;

    _oldHouse = objNull;
   
    _endPos = [(_playerPos select 0) + (_playerDir select 0) * DISTANCE,
		   (_playerPos select 1) + (_playerDir select 1) * DISTANCE,
           (_playerPos select 2) + (_playerDir select 2) * DISTANCE];

    _object setPosASL _endPos;
    
    _list = lineIntersectsWith [_playerPos, _endPos, player, _object, true];

	if (count _list > 0) then {
        _house = _list select 0;
        
        if (_house isKindOf "House") then {
  			FHQ_DebugConsole_CurrentHouse = _house;
            _pos = getPos _house;
            _box = boundingBox _house;
            _arrow setPos [_pos select 0, _pos select 1, (_pos select 2) + ((_box select 1) select 2)];
        };
   	} else {
        FHQ_DebugConsole_CurrentHouse = objNull;
        _arrow  setPos [0,0,-10000];
    };

    sleep 0.05;
};

deleteVehicle _object;
deleteVehicle _arrow;
