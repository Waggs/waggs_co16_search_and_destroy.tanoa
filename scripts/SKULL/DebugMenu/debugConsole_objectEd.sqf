#include "ids.h"
#include "keycodes.h"

FHQ_DebugConsole_ClassCatLast = 0;
FHQ_DebugConsole_ClassSelected = -1;
FHQ_DebugConsole_CurrentWorkObject = objNull;
FHQ_DebugConsole_CurrentWorkObjectPos =  [0, 0, 0];
FHQ_DebugConsole_CurrentClass = "";
FHQ_DebugConsole_CurrentWorkObjectAltitude = 0;
FHQ_DebugConsole_ObjectList = [];
FHQ_DebugConsole_EditorCamera = objNull;
FHQ_DebugConsole_EditorCameraDragging = 0;
FHQ_DebugConsole_EditorCameraDraggingP = 0;

FHQ_DebugConsole_MapPos = [
			0.773281 * safezoneW + safezoneX,
			0.016 * safezoneH + safezoneY,
			0.216563 * safezoneW,
			0.374 * safezoneH
];
FHQ_DebugConsole_MapVisible = false;
FHQ_DebugConsole_MapZoom = 0.1;

FHQ_DebugConsole_ObjectFilterCategories = [
    ["Car","Armored","Animals","Air","Ship","Structures","Flag","Static","Autonomous","Fortifications","Tents","Wreck","Ammo","Helpers","Cargo","Dead_bodies","Garbage","Signs","Furniture","Lamps","Market","Container","Small_items","Military","Submerged","Training","Backpacks","Mines","Men","MenArmy","MenSLA","MenRACS","MenDiver","MenSniper","MenStory","MenRecon","MenSupport","MenUrban","Afroamerican","European","Asian","Support","Submarine","Wreck_sub", 
    "Structures", "Lamps", "Training", "Fortifications",
    "dren_mil", "dren_house", "dren_wreck", "dren_item", "dren_wall", "A3_Bush", "A3_Plants", "A3_Stones", "A3_Trees"],
    
    ["Cars", "Armored", "Animals", "Air", "Ships", "Structures", "Flags", 
	"Static", "Autonomous", "Fortifications", "Tents", "Wrecks", "Ammo", "Helpers", "Cargo", "Dead Bodies",
    "Garbage", "Signs", "Furniture","Lamps","Market", "Container", "Small Items", "Military", "Submerged", "Training", 
	"Backpacks","Mines", "Men", "Men (Army)", "Men (SLA)", "Men (RACS)", "Men (Diver)", "Men (Sniper)", "Men (Story)", "Men (Recon)", "Men (Support)", "Men (Urban)",
    "Men (African)", "Men (European)", "Men (Asian)", "Support", "Submarines", "Wrecks (Submerged)", 
    "Structures (Hidden)", "Lamps (Hidden)", "Training (Hidden)", "Fortifications (Hidden)",
    "+Military", "+Houses", "+Wrecks", "+Misc", "+Walls", "-Bushes", "-Plants", "-Rocks", "-Trees"]
];

FHQ_DebugConsole_ObjectFilterScopes = [
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2
];
	
FHQ_DebugConsole_Cat2Class = {
    /* Find a category and return the appropriate class */
    private ["_cat", "_i", "_res", "_cmp"];
    _cat = _this;
    _res = _cat;
    
    for "_i" from 0 to count (FHQ_DebugConsole_ObjectFilterCategories select 1) do
    {
        _cmp = (FHQ_DebugConsole_ObjectFilterCategories select 1) select _i;
        if (_cat == _cmp) exitWith {
            _res = (FHQ_DebugConsole_ObjectFilterCategories select 0) select _i;         
        };
    };
    
    _res;
};

FHQ_DebugConsole_Cat2Scope = {
    /* Find a category and return the appropriate class */
    private ["_cat", "_i", "_res", "_cmp"];
    _cat = _this;
    _res = _cat;
    
    for "_i" from 0 to count (FHQ_DebugConsole_ObjectFilterCategories select 1) do
    {
        _cmp = (FHQ_DebugConsole_ObjectFilterCategories select 1) select _i;
        if (_cat == _cmp) exitWith {
            _res = FHQ_DebugConsole_ObjectFilterScopes select _i;         
        };
    };
    
    _res;
};

FHQ_DebugConsole_ClassFilterListBuild = {
    private ["_type", "_cnt", "_cfgVehicles", "_i", "_current"];
	FHQ_DebugConsole_ClassList = [];

	_type = _this select 0;
    _targetScope = _this select 1;
	_cnt = 0;
	_cfgVehicles = configFile >> "cfgVehicles";
	_display = findDisplay OBJECTED_DIALOG;
	lbClear OBJECTED_DIALOG_CLASSLIST;

	for "_i" from 0 to (count _cfgVehicles) - 1 do {
		_current = _cfgVehicles select _i;
		if (isClass _current) then {
            _isManClass = false;
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgVehicles" >> _configName >> "scope");
			_class =  getText(configFile >> "cfgVehicles" >> _configName >> "vehicleclass");
			_displayName =  getText(configFile >> "cfgVehicles" >> _configName >> "displayName");
            _faction = getText(configFile >> "cfgVehicles" >> _configName >> "faction");
			_picture =  getText(configFile >> "cfgVehicles" >> _configName >> "picture");
            
			_class2 = toArray _class;
            if (count _class2 >= 3) then {
                if ((_class2 select 0) == 77 && (_class2 select 1) == 101 && (_class2 select 2) == 110) then {
                	_isManClass = true;
					_picture = "";
                };
            }; 

			if (_scope == _targetScope && _class == _type) then {
                if (_displayName == "") then {
                    _displayName = _configName;
                };
                        
                _origDisplayName = _displayName;
	            switch (_faction) do {
    	                case "IND_F": 	{_displayName = _displayName + " [AAC]";};
                    	case "BLU_F": 	{_displayName = _displayName + " [NATO]";};
        				case "BLU_G_F": {_displayName = _displayName + " [FIA]";};
        	        	case "CIV_F": 	{_displayName = _displayName + " [Civilian]";};
        				case "OPF_F": 	{_displayName = _displayName + " [CSAT]";};
                };
               
				FHQ_DebugConsole_ClassList = FHQ_DebugConsole_ClassList + [[_configName, _displayName]];
			 
	            lbAdd [OBJECTED_DIALOG_CLASSLIST, _displayName]; 
//                lbSetPicture[OBJECTED_DIALOG_CLASSLIST, _cnt, _picture]; 
				lbSetValue [OBJECTED_DIALOG_CLASSLIST, _cnt, _cnt];
				if (FHQ_DebugConsole_IsArma3) then {
                	lbSetTooltip [OBJECTED_DIALOG_CLASSLIST, _cnt, _origDisplayName];
				};  
            	_cnt = _cnt + 1;
			};
		};
	};
            
	lbSort (_display displayCtrl OBJECTED_DIALOG_CLASSLIST);
};

FHQ_DebugConsole_ClassFilterList =
{
	_filter = lbData [OBJECTED_DIALOG_CLASSFILTER, FHQ_DebugConsole_ClassCatLast];
    [_filter call FHQ_DebugConsole_Cat2Class, _filter call FHQ_DebugConsole_Cat2Scope] call FHQ_DebugConsole_ClassFilterListBuild;
    FHQ_DebugConsole_ClassSelected = -1;
};

FHQ_DebugConsole_OPSSelectClass =
{
    _index = _this select 0;
	_display = findDisplay OBJECTED_DIALOG;
           
    FHQ_DebugConsole_ClassSelected = lbValue [OBJECTED_DIALOG_CLASSLIST, _index];
    
    _class = FHQ_DebugConsole_ClassList select FHQ_DebugConsole_ClassSelected;
    FHQ_DebugConsole_CurrentClass = _class select 0;
};

FHQ_DebugConsole_CreateEditorCamera = {
	_res = [] spawn {
		_camPos = [(getPos player) select 0, (getPos player) select 1, 5];
    
    	_camera = "camera" camCreate _camPos;
        _camera setDir direction player;
        _camera camCommand "inertia off";
    	_camera camPrepareDive -15;
    	_camera camPrepareTarget objNull;
    	_camera camCommitPrepared 0;
    	_camera cameraEffect ["EXTERNAL", "BACK"];
        showCinemaBorder false;
       
		FHQ_DebugConsole_EditorCamera = _camera;
		waitUntil {false};
	};
    
    FHQ_DebugConsole_EditorCameraActive = false;
    FHQ_DebugConsole_EditorCameraLastX = -1000;
    FHQ_DebugConsole_EditorCameraLastY = -1000;
    FHQ_DebugConsole_EditorCameraLastDX = 0;
    FHQ_DebugConsole_EditorCameraLastDY = 0;
    FHQ_DebugConsole_EditorCameraDir = (direction player);
    FHQ_DebugConsole_EditorCameraDive = -15;
    FHQ_DebugConsole_EditorCameraPos = [(getPos player) select 0, (getPos player) select 1, 5]; 
    FHQ_DebugConsole_EditorCameraCtrl = false;
    FHQ_DebugConsole_EditorCameraShift = false;
    FHQ_DebugConsole_EditorCameraAlt = false;
    FHQ_DebugConsole_EditorCameraDragging = 0;
    FHQ_DebugConsole_EditorCameraDraggingP = 0;
    FHQ_DebugConsole_EditorLeftDownLastTime = 0;
    FHQ_DebugConsole_EditorCameraKeys = [];

    
    _codes = true call BIS_fnc_keyCode;
	_last = _codes select (count _codes - 1);
    
	for "_i" from 0 to (_last - 1) do {
		FHQ_DebugConsole_EditorCameraKeys set [_i, false];
	};

    _res;
};

FHQ_DebugConsole_DeleteEditorCamera = { 
    FHQ_DebugConsole_EditorCamera cameraEffect ["Terminate", "BACK"];
    FHQ_DebugConsole_EditorCamera = objNull;
    terminate _this;
};


FHQ_DebugConsole_UpdateMapPosition = {
	_display = findDisplay OBJECTED_DIALOG;
	_ctrlMap = _display displayctrl OBJECTED_DIALOG_MAP;
    
    if (FHQ_DebugConsole_MapVisible) then {
    	_ctrlMap ctrlmapanimadd [0, FHQ_DebugConsole_MapZoom ,position FHQ_DebugConsole_EditorCamera];
		ctrlmapanimcommit _ctrlMap;
    };
};
    

if (FHQ_DebugConsole_IsArma3) then {
	FHQ_DebugConsole_OPSBoundingBox = compile "boundingBoxReal _this";
} else {
    FHQ_DebugConsole_OPSBoundingBox = compile "boundingBox _this";
};

FHQ_DebugConsole_OPSDrawBoundingBox = {
    _obj = _this select 0;
    _color = _this select 1;
        
 	_box = boundingBoxReal _obj; // call FHQ_DebugConsole_OPSBoundingBox;

	_low = _box select 0;
    _high = _box select 1;
    
	_x1 = _low select 0;
    _y1 = _low select 1;
    _z1 = _low select 2;

	_x2 = _high select 0;
    _y2 = _high select 1;
    _z2 = _high select 2;
    
	drawLine3d [_obj modelToWorld [_x1, _y1, _z2], _obj modelToWorld [_x2, _y1, _z2], _color];
    drawLine3d [_obj modelToWorld [_x2, _y1, _z2], _obj modelToWorld [_x2, _y2, _z2], _color];
    drawLine3d [_obj modelToWorld [_x2, _y2, _z2], _obj modelToWorld [_x1, _y2, _z2], _color];
    drawLine3d [_obj modelToWorld [_x1, _y2, _z2], _obj modelToWorld [_x1, _y1, _z2], _color]; 

    drawLine3d [_obj modelToWorld [_x1, _y1, _z1], _obj modelToWorld [_x2, _y1, _z1], _color];
    drawLine3d [_obj modelToWorld [_x2, _y1, _z1], _obj modelToWorld [_x2, _y2, _z1], _color];
    drawLine3d [_obj modelToWorld [_x2, _y2, _z1], _obj modelToWorld [_x1, _y2, _z1], _color];
    drawLine3d [_obj modelToWorld [_x1, _y2, _z1], _obj modelToWorld [_x1, _y1, _z1], _color];     
    
    drawLine3d [_obj modelToWorld [_x1, _y1, _z1], _obj modelToWorld [_x1, _y1, _z2], _color];
    drawLine3d [_obj modelToWorld [_x2, _y1, _z1], _obj modelToWorld [_x2, _y1, _z2], _color];
    drawLine3d [_obj modelToWorld [_x2, _y2, _z1], _obj modelToWorld [_x2, _y2, _z2], _color];
    drawLine3d [_obj modelToWorld [_x1, _y2, _z1], _obj modelToWorld [_x1, _y2, _z2], _color];
};

FHQ_DebugConsole_OPSMouseDownHandler = {
    if ((_this select 1) == 1) then {
        // Right mouse button down
        FHQ_DebugConsole_EditorCameraActive = true;
    };
    
    if ((_this select 1) == 0) then {
        // Left mouse button down.
        FHQ_DebugConsole_EditorCameraDraggingP = FHQ_DebugConsole_EditorCameraDragging;
        FHQ_DebugConsole_EditorCameraDragging = 1;
    };
    
	// Update shift/ctrl/alt
    FHQ_DebugConsole_EditorCameraShift = _this select 4;
    FHQ_DebugConsole_EditorCameraCtrl = _this select 5;
    FHQ_DebugConsole_EditorCameraAlt = _this select 6;
};    

FHQ_DebugConsole_OPSMouseUpHandler = {
    if ((_this select 1) == 1) then {
        // Right mouse button up
        FHQ_DebugConsole_EditorCameraActive = false;
    };
    
    if ((_this select 1) == 0) then {
        // Left mouse button up
        FHQ_DebugConsole_EditorCameraDraggingP = FHQ_DebugConsole_EditorCameraDragging;
        FHQ_DebugConsole_EditorCameraDragging = 0;
    };
    
    // Update shift/ctrl/alt
    FHQ_DebugConsole_EditorCameraShift = _this select 4;
    FHQ_DebugConsole_EditorCameraCtrl = _this select 5;
    FHQ_DebugConsole_EditorCameraAlt = _this select 6;
};

FHQ_DebugConsole_OPSMouseHandler = {
    if (FHQ_DebugConsole_EditorCameraActive) then {
    	if (FHQ_DebugConsole_EditorCameraLastX != -1000) then {
	        _dx = (_this select 1) - FHQ_DebugConsole_EditorCameraLastX;
        	_dy = (_this select 2) - FHQ_DebugConsole_EditorCameraLastY;
    
    		_factor = 100;
            if (FHQ_DebugConsole_EditorCameraCtrl) then {
                _factor = 10;
            };
            
	    	_dx = _dx * _factor;
        	_dy = _dy * -1 * _factor;
   		
			FHQ_DebugConsole_EditorCameraDir = FHQ_DebugConsole_EditorCameraDir + _dx;
        	FHQ_DebugConsole_EditorCameraDive = FHQ_DebugConsole_EditorCameraDive + _dy;
        
        	FHQ_DebugConsole_EditorCamera setDir FHQ_DebugConsole_EditorCameraDir;
		    [
	    		FHQ_DebugConsole_EditorCamera,
	        	FHQ_DebugConsole_EditorCameraDive,
	        	0
    		] call BIS_fnc_setPitchBank;
         };
    };
    
    FHQ_DebugConsole_EditorCameraLastDX = (_this select 1) - FHQ_DebugConsole_EditorCameraLastX;
	FHQ_DebugConsole_EditorCameraLastDY = (_this select 2) - FHQ_DebugConsole_EditorCameraLastY;
    FHQ_DebugConsole_EditorCameraLastX = _this select 1;
    FHQ_DebugConsole_EditorCameraLastY = _this select 2;

	call FHQ_DebugConsole_MaybeHighlightMouseObject;
};

FHQ_DebugConsole_OPSZChangeHandler = {
    _dz = _this select 1;
    FHQ_DebugConsole_MapZoom = FHQ_DebugConsole_MapZoom + (_dz/50);
    if (FHQ_DebugConsole_MapZoom < 0) then {
        FHQ_DebugConsole_MapZoom = 0;
    };
    
    if (FHQ_DebugConsole_MapZoom > 1.5) then {
        FHQ_DebugConsole_MapZoom = 1.5;
    };
};

FHQ_DebugConsole_OPSKeyDownHandler = {
	// Update shift/ctrl/alt
    FHQ_DebugConsole_EditorCameraShift = _this select 2;
    FHQ_DebugConsole_EditorCameraCtrl = _this select 3;
    FHQ_DebugConsole_EditorCameraAlt = _this select 4;
    
    FHQ_DebugConsole_EditorCameraKeys set [_this select 1, true];
};    

FHQ_DebugConsole_OPSKeyUpHandler = {
	// Update shift/ctrl/alt
    FHQ_DebugConsole_EditorCameraShift = _this select 2;
    FHQ_DebugConsole_EditorCameraCtrl = _this select 3;
    FHQ_DebugConsole_EditorCameraAlt = _this select 4;
    
    FHQ_DebugConsole_EditorCameraKeys set [_this select 1, false];
};

FHQ_DebugConsole_OPSIsCommandOn = {
    _res = false;
    
    {
        if (_x > 0 && _x < count FHQ_DebugConsole_EditorCameraKeys) then {
        	if (FHQ_DebugConsole_EditorCameraKeys select _x) exitWith {
	            _res = true;
        	};
        };
    } foreach actionKeys _this;
    
    _res;
};

FHQ_DebugConsole_GetCameraTargetPos = {
    _pos = screenToWorld [0.5, 0.5];
    
    _res = [_pos select 0, _pos select 1, getTerrainHeightASL _pos]; 
    _res;
};

FHQ_DebugConsole_OPSMouseHolding = {
	FHQ_DebugConsole_EditorCameraLastDX = 0;
	FHQ_DebugConsole_EditorCameraLastDY = 0;   
    
    call FHQ_DebugConsole_MaybeHighlightMouseObject;
};

FHQ_DebugConsole_OPSUpdateCamera =
{    
	_move = 0.1;
	if (FHQ_DebugConsole_EditorCameraShift) then {_move = 1;};
	if (FHQ_DebugConsole_EditorCameraAlt) then {_move = 10;};
	if (FHQ_DebugConsole_EditorCameraAlt && FHQ_DebugConsole_EditorCameraShift) then {_move = 100;};
        
	if (("BuldMoveForward" call FHQ_DebugConsole_OPSIsCommandOn) || (("MoveForward" call FHQ_DebugConsole_OPSIsCommandOn))) then {
        FHQ_DebugConsole_EditorCameraPos = [
        	(FHQ_DebugConsole_EditorCameraPos select 0) + _move * sin (FHQ_DebugConsole_EditorCameraDir),
            (FHQ_DebugConsole_EditorCameraPos select 1) + _move * cos (FHQ_DebugConsole_EditorCameraDir),
            (FHQ_DebugConsole_EditorCameraPos select 2)
        ];
        FHQ_DebugConsole_EditorCamera setPos FHQ_DebugConsole_EditorCameraPos;
    };
    
	if (("BuldMoveBack" call FHQ_DebugConsole_OPSIsCommandOn) || (("MoveBack" call FHQ_DebugConsole_OPSIsCommandOn))) then {
        FHQ_DebugConsole_EditorCameraPos = [
        	(FHQ_DebugConsole_EditorCameraPos select 0) - _move * sin (FHQ_DebugConsole_EditorCameraDir),
            (FHQ_DebugConsole_EditorCameraPos select 1) - _move * cos (FHQ_DebugConsole_EditorCameraDir),
            (FHQ_DebugConsole_EditorCameraPos select 2)
        ];
        FHQ_DebugConsole_EditorCamera setPos FHQ_DebugConsole_EditorCameraPos;
    };
    
	if (("BuldMoveLeft" call FHQ_DebugConsole_OPSIsCommandOn) || (("CarLeft" call FHQ_DebugConsole_OPSIsCommandOn))) then {
        _dir = FHQ_DebugConsole_EditorCameraDir + 90;
        if (_dir > 360) then {_dir = _dir - 360;};
        
        FHQ_DebugConsole_EditorCameraPos = [
        	(FHQ_DebugConsole_EditorCameraPos select 0) - _move * sin (_dir),
            (FHQ_DebugConsole_EditorCameraPos select 1) - _move * cos (_dir),
            (FHQ_DebugConsole_EditorCameraPos select 2)
        ];
        FHQ_DebugConsole_EditorCamera setPos FHQ_DebugConsole_EditorCameraPos;
    };
    
	if (("BuldMoveRight" call FHQ_DebugConsole_OPSIsCommandOn) || (("CarRight" call FHQ_DebugConsole_OPSIsCommandOn))) then {
        _dir = FHQ_DebugConsole_EditorCameraDir + 90;
        if (_dir > 360) then {_dir = _dir - 360;};
        
        FHQ_DebugConsole_EditorCameraPos = [
        	(FHQ_DebugConsole_EditorCameraPos select 0) + _move * sin (_dir),
            (FHQ_DebugConsole_EditorCameraPos select 1) + _move * cos (_dir),
            (FHQ_DebugConsole_EditorCameraPos select 2)
        ];
        FHQ_DebugConsole_EditorCamera setPos FHQ_DebugConsole_EditorCameraPos;
    };
    
    if (("MoveUp" call FHQ_DebugConsole_OPSIsCommandOn) || ("SwimUp" call FHQ_DebugConsole_OPSIsCommandOn)) then {
        FHQ_DebugConsole_EditorCameraPos = [
        	(FHQ_DebugConsole_EditorCameraPos select 0),
            (FHQ_DebugConsole_EditorCameraPos select 1),
            (FHQ_DebugConsole_EditorCameraPos select 2) + _move
        ];
        FHQ_DebugConsole_EditorCamera setPos FHQ_DebugConsole_EditorCameraPos;
    };
        
    if (("MoveDown" call FHQ_DebugConsole_OPSIsCommandOn) || ("SwimDown" call FHQ_DebugConsole_OPSIsCommandOn)) then {
        FHQ_DebugConsole_EditorCameraPos = [
        	(FHQ_DebugConsole_EditorCameraPos select 0),
            (FHQ_DebugConsole_EditorCameraPos select 1),
            (FHQ_DebugConsole_EditorCameraPos select 2) - _move
        ];
        FHQ_DebugConsole_EditorCamera setPos FHQ_DebugConsole_EditorCameraPos;
    };
 
#ifdef UNOBTANIUM   
    if ("ShowMap" call FHQ_DebugConsole_OPSIsCommandOn) then {
        _display = findDisplay OBJECTED_DIALOG;
		_ctrlMap = _display displayctrl OBJECTED_DIALOG_MAP;
        if (!FHQ_DebugConsole_MapVisible) then {
            player sideChat "showing map";
            _ctrlMap ctrlSetPosition FHQ_DebugConsole_MapPos;
            call FHQ_DebugConsole_UpdateMapPosition;
            FHQ_DebugConsole_MapVisible = true;
        } else {
            player sideChat "hiding map";
            _ctrlMap ctrlSetPosition [-10, -10, 1, 1];;
        	FHQ_DebugConsole_MapVisible = false;
        };
	};
#endif
    
    if (FHQ_DebugConsole_EditorCameraDragging == 1) then {
    	call FHQ_DebugConsole_OPSMaybeDragOrSelectObject;
	};
    
    if (FHQ_DebugConsole_CurrentWorkObject != objNull) then {
        [FHQ_DebugConsole_CurrentWorkObject, [0, 1, 0, 1]] call FHQ_DebugConsole_OPSDrawBoundingBox;
	};
    
    if (FHQ_DebugConsole_MapVisible) then {
        call FHQ_DebugConsole_UpdateMapPosition;
	};        
};


FHQ_DebugConsole_UpdateObjectList = {
    _display = findDisplay OBJECTED_DIALOG;
	lbClear OBJECTED_DIALOG_OBJECTLIST;

	_cnt = 0;
	{
        
		lbAdd [OBJECTED_DIALOG_OBJECTLIST, _x select 1]; 
		lbSetValue [OBJECTED_DIALOG_OBJECTLIST, _cnt, _cnt];
        _cnt = _cnt + 1;
  	} foreach FHQ_DebugConsole_ObjectList;
    
//    if (_cnt != 0) then {
//    	lbSort (_display displayCtrl OBJECTED_DIALOG_OBJECTLIST);
//	};        
};


FHQ_DebugConsole_OPSUpdatePosRotStep = {
    private ["_display", "_pos", "_rot"];
    
    _display = findDisplay OBJECTED_DIALOG;
    
    _pos = FHQ_DebugConsole_CurrentWorkObjectPos;
    
    (_display displayCtrl OBJECTED_DIALOG_POSX) ctrlSetText str (_pos select 0);    
    (_display displayCtrl OBJECTED_DIALOG_POSY) ctrlSetText str (_pos select 1);
    (_display displayCtrl OBJECTED_DIALOG_POSZ) ctrlSetText str (_pos select 2);
    
    _rot = getDir FHQ_DebugConsole_CurrentWorkObject;
    (_display displayCtrl OBJECTED_DIALOG_ANGLE) ctrlSetText str _rot;
};


FHQ_DebugConsole_OPS_Create = {
    if (FHQ_DebugConsole_CurrentClass == "") exitWith  {};
    
    _pos = call FHQ_DebugConsole_GetCameraTargetPos;

    // Create the object
    _object = createVehicle [FHQ_DebugConsole_CurrentClass,  [0, 0, 0], [], 0, "NONE"];
    _object setPosASL _pos;
   	_object enableSimulation false;
    _object setDir 0;
    
    FHQ_DebugConsole_CurrentWorkObject = _object;
    FHQ_DebugConsole_CurrentWorkObjectPos =  _pos;

    _displayName =  getText(configFile >> "cfgVehicles" >> FHQ_DebugConsole_CurrentClass >> "displayName");
    
    FHQ_DebugConsole_ObjectList = FHQ_DebugConsole_ObjectList + [[_object, _displayName, FHQ_DebugConsole_CurrentClass]];
    FHQ_DebugConsole_CurrentWorkObjectAltitude = 0;
    
    call FHQ_DebugConsole_UpdateObjectList;
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};

FHQ_DebugConsole_OPS_Delete = {
    for "_i" from 0 to count FHQ_DebugConsole_ObjectList - 1 do {
        private ["_object", "_current"];
        
        _current = FHQ_DebugConsole_ObjectList select _i;
        _object = _current select 0;
        
        if (_object == FHQ_DebugConsole_CurrentWorkObject) exitWith {
           
            FHQ_DebugConsole_ObjectList = FHQ_DebugConsole_ObjectList - [_current];
            deleteVehicle _object;
        };
    };
        
    call FHQ_DebugConsole_UpdateObjectList;
};

FHQ_DebugConsole_OPSSetObject = {
   	FHQ_DebugConsole_CurrentWorkObject = _this;
    FHQ_DebugConsole_CurrentWorkObjectPos = getPosASL FHQ_DebugConsole_CurrentWorkObject; 
    FHQ_DebugConsole_CurrentWorkObjectAltitude = FHQ_DebugConsole_CurrentWorkObject getVariable ["altitude", 0];
        
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};


FHQ_DebugConsole_OPSSelectObject = {
    _index = _this select 0;
	_display = findDisplay OBJECTED_DIALOG;
           
    _idx = lbValue [OBJECTED_DIALOG_OBJECTLIST, _index];
    
    _objects = FHQ_DebugConsole_ObjectList select _idx;
    
   (_objects select 0) call FHQ_DebugConsole_OPSSetObject;
};



FHQ_DebugConsole_GetMouseTargetPos = {
    _offset = _this;
    _pos = screenToWorld [FHQ_DebugConsole_EditorCameraLastX, FHQ_DebugConsole_EditorCameraLastY];
    
    _res = [_pos select 0, _pos select 1, getTerrainHeightASL _pos + _offset]; 
    _res;
};


FHQ_DebugConsole_GetMouseObject = {
	_objects = [];
	if (!isNull FHQ_DebugConsole_EditorCamera) then {
  		_pos = screenToWorld [FHQ_DebugConsole_EditorCameraLastX, FHQ_DebugConsole_EditorCameraLastY];
  		_intersectCam = getPosASL FHQ_DebugConsole_EditorCamera;
  
  		_intersectTarget = [_pos select 0, _pos select 1, getTerrainHeightASL _pos];
  
  		_objects = lineIntersectswith [
   			_intersectCam,
   			_intersectTarget,
   			objnull,
   			objnull,
   			true
  		];
    };
    
    _objects;
};

FHQ_DebugConsole_MaybeHighlightMouseObject = {
	_mouseObjects = call FHQ_DebugConsole_GetMouseObject;

	_res = _mouseObjects call FHQ_DebugConsole_OPSCheckClickedObject;

	if (_res != objNull) then {
       [_res, [1, 1, 1, 1]] call FHQ_DebugConsole_OPSDrawBoundingBox;
	};
};

FHQ_DebugConsole_OPSCheckClickedObject = {
    _mouseObjects = _this;
    _res = objNull;
    
    {
        private ["_check"];
        _check = _x select 0;
        
    	if (_check in _mouseObjects) exitWith {
            _res = _check;
        };
    } foreach FHQ_DebugConsole_ObjectList;
    
    _res;
};


FHQ_DebugConsole_OPSMaybeDragOrSelectObject = {
       
    if (!FHQ_DebugConsole_EditorCameraShift && !FHQ_DebugConsole_EditorCameraAlt) then {
		if (FHQ_DebugConsole_EditorCameraLastDX != 0 || FHQ_DebugConsole_EditorCameraLastDY != 0) then {
            /* Click and drag */
        	FHQ_DebugConsole_CurrentWorkObjectPos = FHQ_DebugConsole_CurrentWorkObjectAltitude call FHQ_DebugConsole_GetMouseTargetPos;
    		FHQ_DebugConsole_CurrentWorkObject setPosASL FHQ_DebugConsole_CurrentWorkObjectPos;
			FHQ_DebugConsole_EditorCameraDraggingP = FHQ_DebugConsole_EditorCameraDragging;
		} else {
            if (FHQ_DebugConsole_EditorCameraDraggingP != FHQ_DebugConsole_EditorCameraDragging) then {
            	/* Click only. CLick means mouse button just went down, no dragging */
            	_mouseObjects = call FHQ_DebugConsole_GetMouseObject;

	            _res = _mouseObjects call FHQ_DebugConsole_OPSCheckClickedObject;

	            if (!isNull _res) then {
                	_res call FHQ_DebugConsole_OPSSetObject;
            	};
         	};
       	};
        
    };
    
    if (FHQ_DebugConsole_EditorCameraShift && !FHQ_DebugConsole_EditorCameraAlt) then {
        _dir = getDir FHQ_DebugConsole_CurrentWorkObject;

        _dir = _dir + (FHQ_DebugConsole_EditorCameraLastDX * 50);        
        while {_dir > 360.0} do {_dir = _dir - 360};
        while {_dir < 0} do {_dir = _dir + 360};

        FHQ_DebugConsole_CurrentWorkObject setDir _dir;
    };
    
    if (!FHQ_DebugConsole_EditorCameraShift && FHQ_DebugConsole_EditorCameraAlt) then {
        _dz = -1 * FHQ_DebugConsole_EditorCameraLastDY;
        
        FHQ_DebugConsole_CurrentWorkObjectAltitude = 
        	(FHQ_DebugConsole_CurrentWorkObjectPos select 2) + _dz - getTerrainHeightASL FHQ_DebugConsole_CurrentWorkObjectPos;
        FHQ_DebugConsole_CurrentWorkObject setVariable ["altitude", FHQ_DebugConsole_CurrentWorkObjectAltitude];
        
        _pos = [ 
          FHQ_DebugConsole_CurrentWorkObjectPos select 0,
          FHQ_DebugConsole_CurrentWorkObjectPos select 1,
         (FHQ_DebugConsole_CurrentWorkObjectPos select 2) + _dz
        ];
        
        FHQ_DebugConsole_CurrentWorkObjectPos = _pos;

        FHQ_DebugConsole_CurrentWorkObject setPosASL FHQ_DebugConsole_CurrentWorkObjectPos;
    };
    
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};
    
FHQ_DebugConsole_OPSSetRotation = {
	_newVal = (_this select 0) * 36.0;
    FHQ_DebugConsole_CurrentWorkObject setDir _newVal;
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};

FHQ_DebugConsole_StrToFloat = {
    private ["_array", "_i", "_frac", "_cur"];
    
    _frac = -1;
    _array = toArray _this;
    _res = 0.0;
    
    for "_i"  from 0 to count _array do {
        _cur = _array select _i;
        if (_cur == 46) then {
            _frac = 10;
        } else {
            _cur = _cur - 48;
            if (_cur >= 0 && _cur <= 9) then {
            	if (_frac < 0) then {
                	_res = _res * 10 + _cur;
            	} else {
	                _res = _res + (_cur / _frac);
                	_frac = _frac * 10;
            	};
            };
        };
    };

    _res  
};


FHQ_DebugConsole_OPSSetX = {
    private ["_val"];
    
    _val = ctrlText OBJECTED_DIALOG_POSX;
    FHQ_DebugConsole_CurrentWorkObjectPos set [0, _val call FHQ_DebugConsole_StrToFloat];
    
    FHQ_DebugConsole_CurrentWorkObject setPosASL FHQ_DebugConsole_CurrentWorkObjectPos;
    
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};

FHQ_DebugConsole_OPSSetY = {
    private ["_val"];
    
    _val = ctrlText OBJECTED_DIALOG_POSY;
    FHQ_DebugConsole_CurrentWorkObjectPos set [1, _val call FHQ_DebugConsole_StrToFloat];
    
    FHQ_DebugConsole_CurrentWorkObject setPosASL FHQ_DebugConsole_CurrentWorkObjectPos;
    
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};

FHQ_DebugConsole_OPSSetZ = {
    private ["_val"];
    
    _val = ctrlText OBJECTED_DIALOG_POSZ;
    FHQ_DebugConsole_CurrentWorkObjectPos set [2, _val call FHQ_DebugConsole_StrToFloat];
    
    FHQ_DebugConsole_CurrentWorkObject setPosASL FHQ_DebugConsole_CurrentWorkObjectPos;
    
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};

FHQ_DebugConsole_OPSSetRotationText = {
    private ["_val"];
    
    _val = ctrlText OBJECTED_DIALOG_ANGLE;
    
    FHQ_DebugConsole_CurrentWorkObject setDir (_val call FHQ_DebugConsole_StrToFloat);
    
    call FHQ_DebugConsole_OPSUpdatePosRotStep;
};


FHQ_DebugConsole_OPS_EnableSim = {
    _enable = _this;
    FHQ_DebugConsole_CurrentWorkObject setVariable ["ABOVE_SEA_LEVEL",0];
    FHQ_DebugConsole_CurrentWorkObject enableSimulation _enable;
};


FHQ_DebugConsole_OPS_EnableSimASL = {
    _enable = _this;
    FHQ_DebugConsole_CurrentWorkObject setVariable ["ABOVE_SEA_LEVEL",1];
    FHQ_DebugConsole_CurrentWorkObject enableSimulation _enable;
};


FHQ_DebugConsole_OPS_SaveComp = {
    private ["_output", "_cr", "_object", "_class", "_name", "_array", "_pos", "_rot", "_first"];
    
    _cr = toString [13, 10];
    _first = true;
    
    // [_type, [_dX, _dY, _z], _azimut, _fuel, _damage, _orientation, _varName, _init, _simulation, _ASL];
    // [_object, _name, _class];
    _output = "[" + _cr;
     
	_refPos = getPosASL ((FHQ_DebugConsole_ObjectList select 0) select 0);
	
    {
    	_object = _x select 0;
        _name = _x select 1;
    	_class = _x select 2;
        
        _pos = getPosASL _object;
        _rot = getDir _object;
        
        if (!_first) then {
            _output = _output + "," + _cr;
        };
        _array = [_class, 
        		 [(_pos select 0) - (_refPos select 0), (_pos select 1) - (_refPos select 1), (_pos select 2) - (_refPos select 2)],
                 _rot,
                 1.0,
                 0.0,
                 [0, 0],
                 "",
                 "",
                 simulationEnabled _object,                 
                 false]; 
    	_output = _output + "    " + str _array + "   /* " + _name + " */";
    	_first = false;     
	} forEach FHQ_DebugConsole_ObjectList;
    
    _output = _output + _cr + "];" + _cr
    				  + "/* Generated by FHQ Debug Console " + _cr
                      + " * Reference coordinates: " + str _refPos + _cr
                      + " * On World: " + worldName + _cr
                      + " */" + _cr;
    
    copyToClipboard _output;
    hint "Saved to clipboard";
    
};

FHQ_DebugConsole_OPS_SaveCompAbs = {
    private ["_output", "_cr", "_object", "_class", "_name", "_array", "_pos", "_rot", "_first"];
    
    _cr = toString [13, 10];
    _first = true;
    
    // [_type, [_dX, _dY, _z], _azimut, _fuel, _damage, _orientation, _varName, _init, _simulation, _ASL];
    // [_object, _name, _class];
    _output = "[" + _cr;
     
	_refPos = [0, 0, 0];
	
    {
    	_object = _x select 0;
        _name = _x select 1;
    	_class = _x select 2;
        
        _pos = getPosASL _object;
        _rot = getDir _object;
        
        if (!_first) then {
            _output = _output + "," + _cr;
        };
        _array = [_class, 
        		 [(_pos select 0) - (_refPos select 0), (_pos select 1) - (_refPos select 1), (_pos select 2) - (_refPos select 2)],
                 _rot,
                 1.0,
                 0.0,
                 [0, 0],
                 "",
                 "",
                 simulationEnabled _object,
                 true]; 
    	_output = _output + "    " + str _array + "   /* " + _name + " */";
    	_first = false;     
	} forEach FHQ_DebugConsole_ObjectList;
    
    _output = _output + _cr + "];" + _cr
					  + "/* Generated by FHQ Debug Console " + _cr
                      + " * Reference coordinates: " + str _refPos + _cr
                      + " * On World: " + worldName + _cr
                      + " */" + _cr;
                      
    copyToClipboard _output;
    hint "Saved to clipboard";
    
};

FHQ_DebugConsole_OPS_Header = {
    private ["_output", "_cr"];
    
    _cr = toString [13, 10];
    
    _output = "    addOns[] = {}; " + _cr
			+ "    addOnsAuto[] = {}; " + _cr 
			+ "    randomSeed = " + str (random 100000) + ";" + _cr
			+ "    class Intel {}; " + _cr;
 
	_output;
};

FHQ_DebugConsole_OPS_Save = {
    private ["_output", "_cr", "_object", "_class", "_name", "_array", "_pos", "_rot", "_cnt", "_header"];
    
    _cr = toString [13, 10];
    _header = call FHQ_DebugConsole_OPS_Header;

    _output = "version=12;" + _cr
            + "class Mission {" + _cr
            + _header 
            + "    class Vehicles {" + _cr
            + format ["        items= %1;", count FHQ_DebugConsole_ObjectList] + _cr;

	_cnt = 0;
    {
    	_object = _x select 0;
        _name = _x select 1;
    	_class = _x select 2;
        
        _pos = getPos _object;
        _rot = getDir _object;

		_output = _output 
                 + format ["        class Item%1 {", _cnt] + _cr
                 + format ["            position[]={%1,%3,%2};", _pos select 0, _pos select 1, _pos select 2] + _cr
                 + format ["            azimut=%1;", _rot] + _cr
                 + format ["            id=%1;", _cnt] + _cr
                 + "            side=""EMPTY"";" + _cr
                 + format ["            vehicle=""%1"";", _class] + _cr
                 + "            skill=0.6;" + _cr;
        
		if (!simulationEnabled _object) then {
            if (_object getVariable ["ABOVE_SEA_LEVEL",0] == 1) then {
                    _pos = getPosASL _object;
                    _output = _output
                     + format ["            init=""this setPos [0,0,0]; this enableSimulation false; this setPosASL [%1,%2,%3];"";",
                     _pos select 0, _pos select 1, _pos select 2] + _cr;
            } else {
            	_output = _output
                 + "            init=""this enableSimulation false"";"  + _cr;
            };
        };
        
        _alt = _object getVariable ["altitude", 0];
        if (_alt != 0) then {
            _output = _output
            	+ format["            offsetY=%1;", _alt] + _cr;
        };
        
		_output = _output                 
                 + "        };" + _cr;

        _cnt = _cnt + 1;
      
	} forEach FHQ_DebugConsole_ObjectList;
    
	_output = _output 
    		+ "    };" + _cr
            + "};" + _cr
            + "class Intro {" + _cr
            + _header 
            + "};" + _cr
            + "class OutroWin {" + _cr
            + _header 
            + "};" + _cr
            + "class OutroLoose {" + _cr
            + _header 
            + "};" + _cr;
                      
    copyToClipboard _output;
    hint "Saved to clipboard";
    
};

[] spawn { 
	_cameraScript = call FHQ_DebugConsole_CreateEditorCamera;

	disableSerialization;

	_ok = createDialog "FHQ_DebugDialog_objectEd";
    
	if (_ok) then {
          
		{
			private "_index";
        
			_index = lbAdd [OBJECTED_DIALOG_CLASSFILTER, _x];
			lbSetData [OBJECTED_DIALOG_CLASSFILTER, _index, _x];
		} forEach (FHQ_DebugConsole_ObjectFilterCategories select 1);

		lbSetCurSel [OBJECTED_DIALOG_CLASSFILTER, 0];
 
		["Car", 2] call FHQ_DebugConsole_ClassFilterListBuild;

        _display = findDisplay OBJECTED_DIALOG;
		(_display displayctrl OBJECTED_DIALOG_MAP) ctrlEnable false;
        (_display displayctrl OBJECTED_DIALOG_MOUSEAREA) ctrlEnable true;
        ctrlSetFocus (_display displayctrl OBJECTED_DIALOG_MOUSEAREA);
        
        call FHQ_DebugConsole_UpdateMapPosition;
        FHQ_DebugConsole_MapVisible = true;

		waitUntil {dialog};
		waitUntil {!dialog};	
	};

	_cameraScript call FHQ_DebugConsole_DeleteEditorCamera;
};


FHQ_DebugConsole_DumpCfgVehicle = {
    private ["_type", "_cnt", "_cfgVehicles", "_i", "_current"];
	_classes = [];

	_cfgVehicles = configFile >> "cfgVehicles";


	for "_i" from 0 to (count _cfgVehicles) - 1 do {
		_current = _cfgVehicles select _i;
		if (isClass _current) then {
            _isManClass = false;
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgVehicles" >> _configName >> "scope");
			_class =  getText(configFile >> "cfgVehicles" >> _configName >> "vehicleclass");
			_displayName =  getText(configFile >> "cfgVehicles" >> _configName >> "displayName");
            _faction = getText(configFile >> "cfgVehicles" >> _configName >> "faction");
			_picture =  getText(configFile >> "cfgVehicles" >> _configName >> "picture");
            
			if (_scope == 1) then {        
				diag_log format["Scope 1: %1 of class %2", _displayName, _class];
			};
            if (_scope == 2) then {        
				diag_log format["Scope 2: %1 of class %2", _displayName, _class];
			};
            
            if (!(_class in _classes)) then { _classes = _classes + [_class]; };
		};
	};
            
	diag_log str _classes;
};