#define MISSION
#ifdef MISSION
#else
#define FHQRscMapControl RscMapControl
#define FHQRscText RscText
#define FHQRscStructuredText RscStructuredText
#define FHQRscPicture RscPicture
#define FHQRscEdit RscEdit
#define FHQRscCombo RscCombo
#define FHQRscListBox RscListBox
#define FHQRscListbox RscListBox
#define FHQRscButton RscButton
#define FHQRscShortcutButton RscShortcutButton
#define FHQRscShortcutButtonMain RscShortcutButtonMain
#define FHQRscFrame RscFrame
#define FHQRscSlider RscSlider
#define FHQRscCheckbox RscCheckbox
#define FHQRscButtonMenu RscButtonMenu
#define FHQRscButtonMenuOK RscButtonMenuOK
#define FHQRscButtonMenuCancel RscButtonMenuCancel
#define FHQRscControlsGroup RscControlsGroup
class RscMapControl;
class RscText;
class RscStructuredText;
class RscPicture;
class RscEdit;
class RscCombo;
class RscListBox;
class RscButton;
class RscShortcutButton;
class RscShortcutButtonMain;
class RscFrame;
class RscSlider;
class RscCheckbox;
class RscButtonMenu;
class RscButtonMenuOK;
class RscButtonMenuCancel;
class RscControlsGroup;
#endif

#include "constants_a3.hpp"
