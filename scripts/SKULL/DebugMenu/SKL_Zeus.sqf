// SKL_Zeus.sqf
//
// add to init.sqf:   execVM "scripts\SKULL\SKL_Zeus.sqf";

if (isNil "z_AddObjects") then {
    0 execVM "scripts\SKULL\DebugMenu\SKL_ZeusObjects.sqf";
};

if (isNil "z_ZeusCostumes") then {
    0 execVM "scripts\SKULL\DebugMenu\SKL_ZeusCostumes.sqf";
};

if (isServer) then
{    
    while {isNil "z_AddObjects"} do {sleep 1;};
    [] spawn { playableUnits call z_AddObjects; };
    { _x addEventHandler ["respawn",{ [_this select 0] call z_AddObjects }] } foreach playableUnits;
};    

if (!isDedicated) then
{
    [] spawn {
        ZMENU_KEYPRESSED = false;
        while {true} do {
            waitUntil {sleep 5; !isNull (findDisplay 312)};
            _keyDown = (findDisplay 312) displayAddEventHandler ["KeyDown", {if (((_this select 1) == 41) && !ZMENU_KEYPRESSED) then {ZMENU_KEYPRESSED = true; curatorSelected spawn zmenu}; false}];
            _keyUp = (findDisplay 312) displayAddEventHandler ["KeyUp", {if ((_this select 1) == 41) then {ZMENU_KEYPRESSED = false}; false}];
            waitUntil {sleep 1; isNull (findDisplay 312)};
        };
    };
};

SKL_ZEUS_THIS = objNull;
SKL_ZEUS_THIS_ONE = objNull;
SKL_ZEUS_ALLOW_POSSESSION = false;

zerror = {
    ((findDisplay 2806) displayCtrl 2700) ctrlSetText _this;
    _handle = CreateDialog "SKLZD_ERROR";
};

zmenu = { // bring up the zeus debug menu
    if (!isNull (getAssignedCuratorLogic player)) then
    {
        SKL_ZEUS_THIS = if (typeName _this == "ARRAY") then {(_this select 0) + (_this select 3)} else {[_this]};
        if (count SKL_ZEUS_THIS == 0) then {SKL_ZEUS_THIS = [player]};
        SKL_ZEUS_THIS_ONE = SKL_ZEUS_THIS select 0;
        _handle = CreateDialog "SKL_ZEUS_DIALOG";
    };
};

z_surrender = // this call z_surrender
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    //if (typename _this != "MAN") exitWith {"Only works on men" call zerror;};
    removeAllWeapons _this;
    removeBackpack _this;
    _this action ["Surrender", _this];
    _this Setcaptive true;
};

z_UnSurrender = // this call z_surrender
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    _this action ["SitDown", _this];
    _this setCaptive false;
    _this switchMove "";
};

z_unupsmon = // this call z_unupsmon;   disables upsmon for the units group
{
    if (!isServer) exitWith { [_this,"z_unupsmon",false] spawn BIS_fnc_MP};
    //if (typeName _this != "MAN") exitWith {"Only works on men" call zerror;};
    UPSMON_NPCs = UPSMON_NPCs  - [_this];
};

z_nearby_position = // utility function returns random position nearby [pos, radius]
{
    if (typeName _this != "ARRAY") exitWith {[0,0,0]};
    
    _pos = _this select 0;
    _radius = _this select 1;
    
    if (typeName _pos != "ARRAY") exitWith {[0,0,0]};
    _x = _pos select 0;
    _y = _pos select 1;
    _z = _pos select 2;
    if ((typeName _x != "SCALAR") || (typeName _y != "SCALAR") || (typeName _z != "SCALAR") || (typeName _radius != "SCALAR")) exitWith {[0,0,0]};
    
    [_x - _radius + (random (_radius*2)), _y - _radius + (random (_radius*2)), _z]
};

z_upsmon = // [this, radius, option] call z_upsmon;  option: "F","R","" = fortify, road, default
{
    if (!isServer) exitWith { [_this,"z_upsmon",false] spawn BIS_fnc_MP};
    private ["_npc","_size","_option","_options","_mkr","_cmd"];
    
    if (isNil "SKL_FH_ZEUS_CNTR") then {SKL_FH_ZEUS_CNTR = 0;};
    if (isNil "FHMZ_UPSMON") then { FHMZ_UPSMON = compile preprocessFileLineNumbers 'scripts\UPSMON\UPSMON.sqf';};
    _npc = _this select 0;
    _size = _this select 1;
    _option = if (count _this > 1) then {_this select 2;} else {"U"};

    _mkr = format ["SKL_ZEUS_%1",SKL_FH_ZEUS_CNTR];
    SKL_FH_ZEUS_CNTR = SKL_FH_ZEUS_CNTR + 1;
    createMarker [_mkr, getPos _npc];
    _mkr setMarkerShape "ELLIPSE";
    _mkr setMarkerAlpha 0;
    _mkr setMarkerSize [_size,_size];
     
    _options = "'SPAWNED', 'NOWAIT'";
    switch (_option) do
    {
        case "F": {_options ="'SPAWNED', 'FORTIFY', 'NOFOLLOW', 'RANDOMA', 'NOWP2', 'NOSMOKE'";};
        case "R": {_options ="'SPAWNED', 'NOWAIT', 'SAFE', 'ONROAD'";};
        default   {_options ="'SPAWNED', 'NOWAIT'";};
    };
    _cmd = format ["[ _this, '%1', %2] spawn FHMZ_UPSMON;",_mkr, _options];
    _npc call compile _cmd;
};

z_endChase = 
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    z_ALLOW_CHASE = false; 
};

z_chase = // this call z_chase;   will chase nearest player unit
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    z_ALLOW_CHASE = true;
    _nearest=objNull;
    _nearestdist=10000;
    {
        _dist=vehicle _x distance _this;
        if (isPlayer _x and _dist<_nearestdist) then {
            _nearest=_x;
            _nearestdist=_dist;
        };
    } forEach playableUnits;

    if (!isNull _nearest) then {
        [_this,_nearest] spawn {
            _chaser = _this select 0;
            _chasee = _this select 1;
            while {alive _chaser && alive _chasee && z_ALLOW_CHASE } do {
               (leader _chaser) doMove (getPos _chasee);
               sleep 5;
            };
        };
    };
};

z_teleport = // this call z_teleport
{
    ZEUS_TELEPORT_UNIT = _this;
    onMapSingleClick {
        _xmax = -1e10; _xmin = 1e10; _ymax = -1e10; _ymin = 1e10;
        {
            _oldPos = getPos _x;
            if (_xmax < _oldPos select 0) then {_xmax = _oldPos select 0};
            if (_xmin > _oldPos select 0) then {_xmin = _oldPos select 0};
            if (_ymax < _oldPos select 1) then {_ymax = _oldPos select 1};
            if (_ymin > _oldPos select 1) then {_ymin = _oldPos select 1};
        } forEach ZEUS_TELEPORT_UNIT;
        _oldZeroPos = [_xmin,_ymin];
        _newZeroPos = [(_pos select 0) - (_xmax - _xmin)/2, (_pos select 1) - (_ymax - _ymin)/2];
        diag_log format ["1: %1 to %2",_oldZeroPos,_newZeroPos];
        {
            if (vehicle _x == _x) then
            {
                _oldPos = getPos _x;
                _deltaX = (_oldPos select 0) - (_oldZeroPos select 0);
                _deltaY = (_oldPos select 1) - (_oldZeroPos select 1);
                _x setPos [_deltaX + (_newZeroPos select 0), _deltaY + (_newZeroPos select 1), (_oldPos select 2)];
        diag_log format ["2: %1 to %2",_oldPos,[_deltaX + (_newZeroPos select 0), _deltaY + (_newZeroPos select 1), (_oldPos select 2)]];
            };
        } forEach ZEUS_TELEPORT_UNIT;
        openMap false; 
    }; 
    _this spawn {
        sleep 0.5;
        findDisplay 312 closeDisplay 2; // close curator interface
        sleep 0.5;
        hint 'Click on the map to teleport'; 
        showMap true; 
        openMap true;
        while {!visibleMap} do {sleep 1;};
        while {visibleMap} do {sleep 1;};
        hintSilent '';
        onMapSingleClick { }; 
        openCuratorInterface;
        sleep 2;
        ZEUS_TELEPORT_UNIT = objNull;
    };
};


z_teleportGroup = // this call z_teleport
{
    ZEUS_TELEPORT_UNIT = _this;
    onMapSingleClick {
        if (isNull ZEUS_TELEPORT_UNIT) exitWith {};
        {_x setPos ([_pos,8] call z_nearby_position)} count units group ZEUS_TELEPORT_UNIT;
        openMap false; 
    }; 
    _this spawn {
        sleep 0.5;
        findDisplay 312 closeDisplay 2; // close curator interface
        sleep 0.5;
        hint 'Click on the map to teleport'; 
        showMap true; 
        openMap true;
        while {!visibleMap} do {sleep 1;};
        while {visibleMap} do {sleep 1;};
        hintSilent '';
        onMapSingleClick { }; 
        openCuratorInterface;
        sleep 2;
        ZEUS_TELEPORT_UNIT = objNull;
    };
};

z_teleportPlayers = // this call z_teleport
{
    ZEUS_TELEPORT_UNIT = GM;
    onMapSingleClick {
        ZEUS_TELEPORT_UNIT = objNull;
        {if (_x != GM) then {_x setPos ([_pos,8] call z_nearby_position)}} count playableUnits;
        openMap false; 
    }; 
    _this spawn {
        sleep 0.5;
        findDisplay 312 closeDisplay 2; // close curator interface
        sleep 0.5;
        hint 'Click map to teleport there. Close map to teleport to object.';
        showMap true; 
        openMap true;
        while {!visibleMap} do {sleep 1;};
        while {visibleMap} do {sleep 1;};
        hintSilent '';
        onMapSingleClick { }; 
        openCuratorInterface;
        if (!isNull ZEUS_TELEPORT_UNIT) then
        {
            {if (_x != GM) then {_x setPos ([getPos _this,8] call z_nearby_position)}} count playableUnits;
        };
        ZEUS_TELEPORT_UNIT = objNull;
    };
};

z_teleportZeus = // this call z_teleport
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    ZEUS_TELEPORT_UNIT = player;
    onMapSingleClick {
        ZEUS_TELEPORT_UNIT = objNull;
        player setPos _pos;
        openMap false; 
    }; 
    _this spawn {
        sleep 0.5;
        findDisplay 312 closeDisplay 2; // close curator interface
        sleep 0.5;
        hint 'Click map to teleport there. Close map to teleport to object.'; 
        showMap true; 
        openMap true;
        while {!visibleMap} do {sleep 1;};
        while {visibleMap} do {sleep 1;};
        hintSilent '';
        onMapSingleClick { }; 
        openCuratorInterface;
        if (!isNull ZEUS_TELEPORT_UNIT) then
        {
            _pos = if (typeName _this == "STRING") then {getMarkerPos _this} else {getPos _this};
            player setPos ([_pos,8] call z_nearby_position);
        };
        ZEUS_TELEPORT_UNIT = objNull;
    };
};

z_fhmz = // calls SKL_FhMz
{
    _pos = [0,0,0];
    if (typeName _this == "STRING") then {_pos = getMarkerPos _this}
    else {_pos = getPos _this};
    
    disableSerialization;
    
    _handle = CreateDialog "SKL_ZEUS_FHMZ_DIALOG";
    
    _display = (findDisplay 2900);
    
    // Center
    (_display displayCtrl 2901) ctrlSetText format ["%1",(_pos select 0)];
    (_display displayCtrl 2902) ctrlSetText format ["%1",(_pos select 1)];
    (_display displayCtrl 2903) ctrlSetText format ["%1",(_pos select 2)];
    
    {
        _index = lbAdd [ 2905, _x];
    } forEach ["East","West","Resistance","Civilian"];
    lbSetCurSel [2905,0];
};

z_movableMarker = // attach to item to allow moving markers  [object,markerName,hidden]
{
    private ["_object","_marker", "_hide"];
    _object = _this select 0;
    _marker = _this select 1;
    _hide = false;
    if (count _this > 2) then { _hide = _this select 2 };
    
    [_object] call z_AddObjects;
    if (_hide) then { hideObject _object; };
    [_object,_marker] spawn {
        private ["_object","_marker"];
        _object = _this select 0;
        _marker = _this select 1;
        while {(alive _object) && (getMarkerColor _marker != "")} do
        {
            _marker setMarkerPos (getPos _object);
            sleep 10;
        };
    };
};
    
z_changeAppearance = // put up dialog to change zeus's appearance
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    disableSerialization;
    
    _handle = CreateDialog "SKL_ZEUS_APPEARANCE_DIALOG";
    
    _display = (findDisplay 2950);
    
    {
        _index = lbAdd [ 2951, _x select 0];
    } forEach z_ZeusCostumes;
};	

z_setAppearance = // change zeus's appearance [type]
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
	_stuff = z_ZeusCostumes select _this;
	_zeus = player;
	
	removeAllWeapons _zeus;
	removeBackpack _zeus;
	removeHeadgear _zeus;
	removeUniform _zeus;
	removeGoggles _zeus;
	removeVest _zeus;
	
	//[ "name", [uniforms], [vests], [backpacks], [hats], [glasses], weapon, [ammo], items]
	_uniform = (_stuff select 1) call BIS_fnc_selectRandom;
	_vest = (_stuff select 2) call BIS_fnc_selectRandom;
	_backpack = (_stuff select 3) call BIS_fnc_selectRandom;
	_hat = (_stuff select 4) call BIS_fnc_selectRandom;
	_glasses = (_stuff select 5) call BIS_fnc_selectRandom;
	_weapons = (_stuff select 6);
	_ammo = (_stuff select 7);
	_items = (_stuff select 8);
	
	_zeus addUniform _uniform;
	if (!isNil "_vest") then {_zeus addVest _vest;};
	if (!isNil "_backpack") then {_zeus addBackpack _backpack};
	if (!isNil "_hat") then {_zeus addHeadgear _hat};
	if (!isNil "_glasses") then {_zeus addGoggles _glasses};
	{_zeus addWeapon _x} forEach _weapons;
	
	_container = uniformContainer _zeus;
	if (!isNil "_backpack") then { _container = unitBackPack _zeus }
	else { if (!isNil "_vest") then { _container = vestContainer _zeus }; };
	
	{ _container addMagazineCargoGlobal [_x, 2] } forEach _ammo;	
	{ _container addItemCargoGlobal [_x, 1] } forEach _items;
    _container addItemCargoGlobal ["FirstAidKit", 1];
	
	_zeus selectWeapon "this";
    reload _zeus;
};

// z_killableZeusDamageHandler = // damage handler for z_killableZeus
// {
     // private ["_zeus","_damagedPart","_damage","_dummy"];
     
    // _zeus = _this select 0;
    // _damagedPart = _this select 1;
    // _damage = _this select 2;
    
// hint format ["%1 - %2",_zeus,_damage];     
    // if (!SKL_KILLABLE_ZEUS) exitWith {0};
    
    // if (_damage < 0.9) exitWith {_damage};
    // _zeus allowDamage false;
    // SKL_KILLABLE_ZEUS = false;
    
    // _damage = 0;
    // _dummy = (group _zeus) createUnit [typeOf player, getMarkerPos "Boot_Hill", [], 0, "NONE"];
     
    // removeAllWeapons _dummy;
    // removeBackpack _dummy;
    // removeHeadgear _dummy;
    // removeUniform _dummy;
    // removeGoggles _dummy;
    // removeVest _dummy;
    // removeAllAssignedItems _dummy;
    
    // _uniform = uniform _zeus;
    // _vest = vest _zeus;
    // _backpack = backpack _zeus;
    // if (_backpack in ["tf_rt1523g","tf_anprc155","tf_mr3000"]) then {_backpack = "B_Kitbag_mcamo"};
    // _hat = headgear _zeus;
    // _glasses = goggles _zeus;
    // _weapons = weapons _zeus;
    // [_dummy] spawn z_AddObjects; 
    
    // _dummy addUniform _uniform;
    // if (!isNil "_vest") then {_dummy addVest _vest;};
    // if (!isNil "_backpack") then {_dummy addBackpack _backpack};
    // if (!isNil "_hat") then {_dummy addHeadgear _hat};
    // if (!isNil "_glasses") then {_dummy addGoggles _glasses};
    // {_dummy addWeapon _x} forEach _weapons;
    
    // switch (stance _zeus) do
    // {
        // case "STAND":  {_dummy setUnitPos "UP"};
        // case "CROUCH": {_dummy setUnitPos "Middle"};
        // case "PRONE":  {_dummy setUnitPos "DOWN"};
        // default        {_dummy setUnitPos "UP"};
    // };
    // _dummy switchMove (animationState _zeus);
    // _dummy setDir (direction _zeus);
    // _dummy setPos (getPos _zeus);
    // _zeus setPos (getMarkerPos "Boot_Hill");
    // _dummy setDamage 1;
    // _zeus setDamage 0;
    // 0 spawn { sleep 1; true call z_killableZeus; };
    // 0
// };

// z_killableZeus = // allow zeus to fake his death (true/false)
// {
    // private ["_enable"];
    // _enable = _this;
    // if (isNull (getAssignedCuratorLogic player)) exitWith {};
    // if (isNil "SKL_KILLABLE_ZEUS") then {SKL_KILLABLE_ZEUS = false};
    // if (!_enable) then 
    // {
        // SKL_KILLABLE_ZEUS = false;
        // player removeAllEventHandlers "HandleDamage";
        // player setDamage 0;
        // player allowDamage false;
    // }
    // else
    // {
        // SKL_KILLABLE_ZEUS = true;
        // player addEventHandler ["HandleDamage", {_this call z_killableZeusDamageHandler}];
        // player allowDamage true;
    // };
// };

z_hideObject = { (_this select 0) hideObject (_this select 1) };

z_possess = // allow zeus to follow the remote controlled player (must be spawned)
{
    if (isNull (getAssignedCuratorLogic player)) exitWith {};
    while {SKL_ZEUS_ALLOW_POSSESSION} do
    {
        waitUntil {sleep 2; !isNil "bis_fnc_moduleRemoteControl_unit"};
        // don't allow possession if zeus is in vehicle
        if (vehicle player == player) then
        {
            _npc = bis_fnc_moduleRemoteControl_unit;
            _pos = getPos player;      
            [[player,true],"z_hideObject",true,false] spawn BIS_fnc_MP;
            player attachTo [_npc,[0,0,0]]; 
        
            waitUntil {sleep 1; isNil "bis_fnc_moduleRemoteControl_unit"};
            detach player;
            player setPos _pos;
            [[player,false],"z_hideObject",true,false] spawn BIS_fnc_MP;  
        }
        else
        {
            waitUntil {sleep 1; isNil "bis_fnc_moduleRemoteControl_unit"};
        };
    };        
};

z_ammoBox = // fill ammo box (1st parameter), side will be 2nd param, side of 2nd param, or side dialog will appear
{
    _this spawn {
        private ["_box","_side"];
        _box = _this;
        _side = sideLogic;
        if (typeName _this == "ARRAY") then 
        {
            _box = _this select 0;
            if (count _this > 1) then
            {
                _side = if (typeName (_this select 1) == "SIDE") then {_this select 1} else {side (_this select 1)};
            };
        }; 
        if (_side == sideLogic) then
        {
            _handle = CreateDialog "SKL_ZEUS_SIDE_DIALOG";
            {
                _index = lbAdd [ 2910, _x];
            } forEach ["East","West","Resistance"];
            lbSetCurSel [2910,1];
            waitUntil {sleep 1; isNull (findDisplay 2960)};
            _side = SKL_ZEUS_SIDE;
        };
        if (_side == sideLogic) exitWith{};
        
        SKL_ZEUS_AMMOBOX = _box;
        publicVariable "SKL_ZEUS_AMMOBOX";
        switch (_side) do
        {
            case west:  {["SKL_ZEUS_AMMOBOX execVM 'scripts\CUSTOM\custom_loadouts\genericNatoBox.sqf'", "SKL_DebugCmd",false,false] spawn BIS_fnc_MP};
            case east:  {["SKL_ZEUS_AMMOBOX execVM 'scripts\CUSTOM\custom_loadouts\genericCsatBox.sqf'", "SKL_DebugCmd",false,false] spawn BIS_fnc_MP};
            default     {["SKL_ZEUS_AMMOBOX execVM 'scripts\CUSTOM\custom_loadouts\genericIndBox.sqf'", "SKL_DebugCmd",false,false] spawn BIS_fnc_MP};
        };
    };
};

z_holdFire = 
{
    private ["_units","_enable"];
    _units = _this select 0;
    _enabled = _this select 1;
    {
        if (_enabled) then 
        {
            (group _x) setCombatMode "BLUE";
            _id = _x addEventHandler ["FiredNear",{[[_this select 0],false] call z_holdFire;}];
            _x setVariable ["z_holdFireId",_id];
        } else {
            (group _x) setCombatMode "YELLOW";
            _x removeEventHandler ["FiredNear",(_x getVariable ["z_holdFireId",0])];
        };
    } forEach _units;    
};

z_saveScene = 
{
    private ["_objects","_groups","_waypoints","_markers","_vehicleList","_objectList","_sideList","_sideCount","_sides","_output", "_cr", "_cnt","_veh","_side"];

    _objects = curatorSelected select 0;
    _groups = curatorSelected select 0;
    _waypoints = curatorSelected select 0;
    _markers = curatorSelected select 0;
    
    _objectList = [];
    _sideList = [[],[],[],[]]; // [[east],[west],[resistance],[civilian]]
    _vehicleList = [[],[],[],[]];
    _vehCount = 0;
    _sides = ["EAST","WEST","GUER","CIV"];
    
	{
        if (vehicle _x == _x) then
        {
            if (isNull (group _x)) then
            {
                _objectList = _objectList + [_x];
            } else {
                if (_x isKindOf "Man") then 
                {
                    switch (side _x) do
                    {
                        case east:       {_sideList set [0, (_sideList select 0) + [_x]]};
                        case west:       {_sideList set [1, (_sideList select 1) + [_x]]};
                        case resistance: {_sideList set [2, (_sideList select 2) + [_x]]};
                        default          {_sideList set [3, (_sideList select 3) + [_x]]};
                    };
                } else {
                    _vehCount = _vehCount + 1;
                    switch (side _x) do
                    {
                        case east:       {_vehicleList set [0, (_vehicleList select 0) + [_x]]};
                        case west:       {_vehicleList set [1, (_vehicleList select 1) + [_x]]};
                        case resistance: {_vehicleList set [2, (_vehicleList select 2) + [_x]]};
                        default          {_vehicleList set [3, (_vehicleList select 3) + [_x]]};
                    };

                };
            };
        };
		
	} forEach _objects;
    
    _cr = toString [13, 10];
    _sideCount = {count _x > 0} count _sideList;
    
    _header = "    addOns[] = {}; " + _cr
			+ "    addOnsAuto[] = {}; " + _cr 
			+ "    randomSeed = " + str (round (random 100000)) + ";" + _cr
			+ "    class Intel {}; " + _cr;
            
    _output = "version=12;" + _cr
            + "class Mission {" + _cr
            + _header;
            
    ////// GROUPS //////
    _cnt = 0;
    if (_sideCount > 0) then
    {
        _output = _output
            +         "    class Groups {" + _cr
            + format ["        items= %1;", _vehCount + ({count _x > 0} count _sideList) ] + _cr;
        for "_i" from 0 to 3 do
        {
            _side = _sides select _i;
            if (count (_sideList select _i) > 0) then
            {
                _veh = [(_sideList select _i),false] call z_sceneVehicles;
                _output = _output
                    + format ["        class Item%1 {", _cnt] + _cr
                    + format ["            side=""%1"";", _side] + _cr
                    + _veh
                    + "        };" + _cr;
                _cnt = _cnt + 1;
            };          
            {                 
                _veh = [[_x],false] call z_sceneVehicles;
                _output = _output
                    + format ["        class Item%1 {", _cnt] + _cr
                    + format ["            side=""%1"";", _side] + _cr
                    + _veh
                    + "        };" + _cr;
                _cnt = _cnt + 1;
            } forEach (_vehicleList select _i);            
        };
        _output = _output 
            + _cr + "    };" + _cr;
     };
     
    ////// OBJECTS ////// 
    if (count _objectList > 0) then 
    {
        _veh = [_objectList,true] call z_sceneVehicles;
        _output = _output + _veh;
    };
    
	_output = _output + "};" + _cr
            + "class Intro {" + _cr
            + _header 
            + "};" + _cr
            + "class OutroWin {" + _cr
            + _header 
            + "};" + _cr
            + "class OutroLoose {" + _cr
            + _header 
            + "};" + _cr;
                      
    copyToClipboard _output;
    hint "Saved to clipboard";
};

z_sceneVehicles = // used by z_sceneSave
{    
    private ["_output","_cr", "_object", "_class", "_name", "_array", "_pos", "_rot", "_cnt","_items","_empty"];
    
    _items = _this select 0;
    _empty = _this select 1;
    _tabs = "    ";
    if (!_empty) then {_tabs = _tabs + "        "};
	_cnt = 0;
    _cr = toString [13, 10];
    _output = format ["%1class Vehicles {",_tabs] + _cr
            + format ["%1    items= %2;",_tabs, count _items] + _cr;
    {
    	_object = _x;     
        _pos = getPos _object;
        _rot = getDir _object;

		_output = _output 
                 + format ["%1    class Item%2 {",_tabs, _cnt] + _cr
                 + format ["%1        position[]={%2,%4,%3};",_tabs, _pos select 0, _pos select 1, _pos select 2] + _cr
                 + format ["%1        azimut=%2;",_tabs, _rot] + _cr
                 + format ["%1        id=%2;",_tabs, _cnt] + _cr
                 + format ["%1        side=""%2"";",_tabs, if (_empty) then {"EMPTY"} else {side _object}] + _cr
                 + format ["%1        vehicle=""%2"";",_tabs, typeOf _object] + _cr
                 + format ["%1        skill=%2;",_tabs, skill _object] + _cr;
        
        _alt = (getPosATL _object) select 2;
        _alt = (round (_alt * 100)) / 100;
        if (_alt != 0) then {
            _output = _output
            	+ format["%1        offsetY=%2;",_tabs, _alt] + _cr;
        };
        if (!_empty) then {
            _output = _output
            	+ format["%1        init=""doStop this; this setPosATL %2; this setDir %3"";",_tabs,getPosATL _object, _rot] + _cr;
        };
		_output = _output                 
                 + format ["%1    };",_tabs] + _cr;

        _cnt = _cnt + 1;
      
	} forEach _items;
	_output = _output 
    		+ format ["%1};",_tabs] + _cr;
    _output
};






