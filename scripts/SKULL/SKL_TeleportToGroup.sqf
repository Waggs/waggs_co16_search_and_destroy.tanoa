// Teleport To Group: use in teleport board to allow JIP'ers to catch up with their group
//
// usage - in init field: _nil = this execVM "scripts\SKULL\SKL_TeleportToGroup.sqf";
//
// Version 1.0  3-11-14
// 1.0: initial release
//["<t color=""#00aeff"">" +"Move to our Forward Base", "GoToBase.sqf"];
_this addAction ["<t color=""#00aeff"">" +"Teleport to team", 
    { 
        private ["_done","_player"];
        _done = false;
        _player = _this select 1;
        {
            if (_player distance _x > 400) exitWith
            {
                _player setPos getPos _x;
                _done = true;
            };
        } forEach units group _player;
        if (!_done) then
        {
            {
                if (_player distance _x > 400) exitWith
                {
                    _player setPos getPos _x;
                    _done = true;
                };
            } forEach playableUnits;
        };
     } ];
     