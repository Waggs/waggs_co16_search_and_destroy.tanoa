// SKL_Title.sqf
//
// Shows a mission title on the screen
//
// Usage: in init.sqf add : ["title line 1","title line 2"] execVM "scripts\SKULL\SKL_Title.sqf"
//
// You can add as many title lines as you like.
//
// Example:
// ["<t size='.8'>Operation Hummingbird</t>",
//  "<t size='.6'>"+str(date select 1) + "." + str(date select 2) + "." + str(date select 0)+"</t>",
//  "<t size='.6'>Northern Altis</t>",
//  "<t size='.55'>by SkullTT</t>"] execVM "scripts\SKULL\SKL_Title.sqf";
//
// The 1st item of the array can be a number - time is sec to show title


if (hasInterface) then
{
    waitUntil{!(isNil "BIS_fnc_init")};
    _y = 0.3; // location on screen
    _dy = 0.08;// distance between lines
    _dt = 1.5;   // time between showing each line
    _rsc = 3010; // resource to use
    _time = 5; // total time to show
    _textArray = _this;
    
    // see if 1st item is a time
    if (typeName (_this select 0) !=  typeName "") then
    {
        _time = _this select 0;
        _textArray = _textArray - [_time];
    };
     
    // now adjust for size of array
    _arraySize = count _textArray;
    _time = _time + (_dt * _arraySize);   
    _y = _y - _dy * _arraySize / 2;
    {
        [_x,0.02,_y,_time,1,0,_rsc] spawn bis_fnc_dynamicText;
        sleep _dt;
        _y = _y + _dy;
        _rsc = _rsc + 1;
        _time = _time - _dt * 1.2;
    } forEach _textArray;
};