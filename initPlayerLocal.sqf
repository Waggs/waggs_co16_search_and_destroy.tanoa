
waitUntil { (postInitComplete) };
if !(pr_deBug == 0) then { diag_log format ["*PR* initPlayerLocal pr_debug = %1",pr_debug]; };

waitUntil { local player; player == player };
_unit = player;

fnc_setStanima = {
    _unit = _this select 0; _unit SetStamina 90; _unit setCustomAimCoef 0.4;
    if !(pr_deBug == 0) then { diag_log format ["*PR* fnc_setStanima - complete"]; };
};

[_unit] spawn fnc_setStanima;

if (aceOn) then {
    [_unit] spawn pr_fnc_medHolder;
};

if !(aceOn) then {
    [] execVM "scripts\SKULL\skl_pm\skl_init.sqf";
    [] execVM "scripts\PR\scripts\hud_tags\nameTags.sqf";

    _unit spawn {
        if !(pr_deBug == 0) then { diag_log format ["*PR* backup check - start"]; };
        _unit = _this;
        waitUntil { time > 1 };

        sleep 5;
        if (isNil { ais_aisInit }) then {
            [] call compile preprocessFileLineNumbers (TCB_AIS_PATH+"init_ais.sqf");
            titleText ["AIS complete","PLAIN DOWN", 1];
            if !(pr_deBug == 0) then { diag_log format ["*PR* AIS backup - complete"]; };
        };
        sleep 5;
        if (isNil { BlueHud_initialized }) then {
            [] call blueHud_fnc_initHud;
            titleText ["BluHud complete","PLAIN DOWN", 1];
            if !(pr_deBug == 0) then { diag_log format ["*PR* BluHud backup - complete"]; };
        };
        if !(pr_deBug == 0) then { diag_log format ["*PR* backup check - complete"]; };
    };
};

_unit spawn {
    if !(pr_deBug == 0) then { diag_log format ["*PR* backup check - start"]; };
    _unit = _this;
    waitUntil { time > 1 };
    sleep 5;
    if (isNil { myCustomLoadout }) then {
        [] call compile preprocessFileLineNumbers "scripts\PR\loadouts\fn_loadoutInit.sqf";
        [] execVM "special\fn_Client.sqf";
        titleText ["Loadout complete","PLAIN DOWN", 1];
        if !(pr_deBug == 0) then { diag_log format ["*PR* Loadout backup - complete"]; };
    };
};

_unit addMPEventHandler ["MPRespawn", {
    [_this select 0] spawn fnc_setStanima;
    if !(aceOn) then {
        [_unit] call compile preprocessFileLineNumbers (TCB_AIS_PATH+"init_ais.sqf");
        disableUserInput false;
    };
    if ((p1) == (player)) then {
        [_this select 0] call pr_commRespawn;
    };
}];

//--- jip checks to make sure the following scripts are ran
if (_this select 1) then {
    if !(pr_deBug == 0) then { diag_log format ["*PR* JIP check - start"]; };
    _unit = _this select 1;
    _unit spawn {
        _unit = _this;
        waitUntil { local player; player == player };

        sleep 10;
        if (isNil { myCustomLoadout }) then {
            [] call compile preprocessFileLineNumbers "scripts\PR\loadouts\fn_loadoutInit.sqf";
            [] execVM "special\fn_Client.sqf";
            if !(pr_deBug == 0) then { diag_log format ["*PR* Loadout backup - complete"]; };
        };
        if !(aceOn) then {
            if (isNil { ais_aisInit }) then {
                [] call compile preprocessFileLineNumbers (TCB_AIS_PATH+"init_ais.sqf");
                if !(pr_deBug == 0) then { diag_log format ["*PR* AIS backup - complete"]; };
            };
            if (isNil { BlueHud_initialized }) then {
                [] call blueHud_fnc_initHud;
            };
        };
    };
    if !(pr_deBug == 0) then { diag_log format ["*PR* JIP check - complete"]; };
};
